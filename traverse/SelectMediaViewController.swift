//
//  SelectMediaViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 13.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

private let SelectMediaViewControllerNib: String = "SelectMediaViewController"
private let MediaCellNib: String = "MediaCell"
private let SectionInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 16.0, bottom: 0, right: 16.0)
private let MediaCellSize: CGSize = CGSize(width: 208, height: 117)

class SelectMediaViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {

    private let fiches: [Fiche]
    private var selectedMediaCell: MediaCell!
    private var selectedCover: Cover?

    @IBOutlet weak var mediasView: UICollectionView!

    init(fiches: [Fiche]) {
        self.fiches = fiches
        self.selectedCover = nil
        super.init(nibName: SelectMediaViewControllerNib, bundle: nil)
    }

    convenience init(fiches: [Fiche], selected: Cover) {
        self.init(fiches: fiches)
        self.selectedCover = selected
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Register cell classes
        self.mediasView!.register(UINib(nibName: MediaCellNib, bundle: nil), forCellWithReuseIdentifier: MediaCellNib)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getSelectedCover() -> Cover? {
        return selectedCover
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */

    // MARK: UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.fiches.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()

        if let mediaCell: MediaCell = collectionView.dequeueReusableCell(withReuseIdentifier: MediaCellNib, for: indexPath) as? MediaCell {
            mediaCell.set(item: self.fiches[indexPath.row])
            if self.fiches.count == 1 && self.selectedCover == nil {
                self.selectedCover = mediaCell.getItem().cover
                mediaCell.select()
                self.selectedMediaCell = mediaCell
            }
            if mediaCell.getItem().cover != nil
                && self.selectedCover != nil
                && mediaCell.getItem().cover?.path == self.selectedCover?.path
                && self.selectedMediaCell == nil {
                mediaCell.select()
                self.selectedMediaCell = mediaCell
            }
            cell = mediaCell
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        if self.selectedMediaCell != nil {
            self.selectedMediaCell.deselect()
        }

        if let selectedCell: MediaCell = collectionView.cellForItem(at: indexPath) as? MediaCell {
            selectedCell.select()
            self.selectedMediaCell = selectedCell
            self.selectedCover = selectedCell.getItem().cover
        }
    }

    // MARK: UICollectionViewDelegateFlowLayout

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return SectionInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return MediaCellSize
    }

    // MARK: UICollectionViewDelegate

    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */

    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */

    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }

     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }

     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
     
     }
     */
    
}

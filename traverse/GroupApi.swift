//
//  GroupAPI.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Hydra

class GroupAPI {
    static func getGroups() -> Promise<[Group]> {
        return ApiRequest.request(array: GroupRouter.getGroups(), type: Group.self)
    }
}

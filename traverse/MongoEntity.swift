//
//  MongoEntity.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class MongoEntity: Gloss.Decodable {
    static let ID: String = "_id"

    let id: String

    required init?(json: JSON) {
        guard let id: String = MongoEntity.ID <~~ json else {
            return nil
        }

        self.id = id
    }

    static func getDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"
        return dateFormatter
    }
}

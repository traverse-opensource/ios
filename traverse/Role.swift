//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class Role: MongoEntity {
    static let TITLE: String = "title"
    static let DESCRIPTION: String = "description"

    var title: String?
    var description: String?

    required init?(json: JSON) {
//        guard let title: String = Role.TITLE <~~ json,
//        let description: String = Role.DESCRIPTION <~~ json else {
//            return nil
//        }

        self.title = Role.TITLE <~~ json
        self.description = Role.DESCRIPTION <~~ json

        super.init(json: json)
    }

    func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "title" ~~> self.title,
            "description" ~~> self.description
        ])
    }
}

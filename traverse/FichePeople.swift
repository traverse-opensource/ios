//
//  FicheEvent.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class FichePeople: Fiche {
    static let SURNAME: String = "surname"
    static let DATE: String = "date"
    static let BIRTHDAY: String = "birthday"
    static let DELTA_START: String = "delta_start"
    static let DEATHDAY: String = "deathday"
    static let PLACE_OF_BIRTH: String = "place_of_birth"
    static let DELTA_END: String = "delta_end"

    var surname: String?
    var date: Date?
    var birthday: Date?
    var deltaStart: Int?
    var deathday: Date?
    var placeOfBirth: String?
    var deltaEnd: Int?

    required init?(json: JSON) {

        self.surname = FichePeople.SURNAME <~~ json

        let dateFormatter: DateFormatter = MongoEntity.getDateFormatter()

        if let _: String = FichePeople.DATE <~~ json {
            self.date =
                Decoder.decode(dateForKey: FichePeople.DATE, dateFormatter: dateFormatter)(json)!
        } else {
            self.date = nil
        }

        if let _: String = FichePeople.BIRTHDAY <~~ json {
            self.birthday =
                Decoder.decode(dateForKey: FichePeople.BIRTHDAY, dateFormatter: dateFormatter)(json)!
        } else {
            self.birthday = nil
        }

        if let _: String = FichePeople.DEATHDAY <~~ json {
            self.deathday =
                Decoder.decode(dateForKey: FichePeople.DEATHDAY, dateFormatter: dateFormatter)(json)!
        } else {
            self.deathday = nil
        }

        self.deltaStart = FichePeople.DELTA_START <~~ json
        self.placeOfBirth = FichePeople.PLACE_OF_BIRTH <~~ json
        self.deltaEnd = FichePeople.DELTA_END <~~ json

        super.init(json: json)
    }


    override func toJSON() -> JSON? {

        var jsonThemes: [JSON] = []
        themes.forEach { (theme) in
            jsonThemes.append(theme.toJSON()!)
        }

        var jsonCategories: [JSON] = []
        categories?.forEach { (cat) in
            jsonCategories.append(cat.toJSON()!)
        }

        var jsonTags: [JSON] = []
        tags?.forEach { (tag) in
            jsonTags.append(tag.toJSON()!)
        }

        return jsonify([
            MongoEntity.ID ~~> self.id,
            ItemEntity.TYPE ~~> self.type,
            ItemEntity.NAME ~~> self.name,
            Gloss.Encoder.encode(dateForKey: ItemEntity.CREATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.createdAt),
            Gloss.Encoder.encode(dateForKey: ItemEntity.LAST_UPDATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.lastUpdatedAt),
            ItemEntity.STATUS ~~> self.status,
            ItemEntity.COVER ~~> self.cover?.toJSON(),
            ItemEntity.CREATED_BY ~~> self.createdBy?.toJSON(),
            ItemEntity.LAST_UPDATED_BY ~~> self.lastUpdatedBy?.toJSON(),
            ItemEntity.THEME ~~> self.mainTheme.toJSON(),
            ItemEntity.SUB_CATEGORIES ~~> self.subCategories,
            ItemEntity.LIKES ~~> self.likes,
            Fiche.REFERENCES ~~> self.references,
            Fiche.LIKES ~~> self.city,
            Fiche.LATITUDE ~~> self.latitude,
            Fiche.LONGITUDE ~~> self.longitude,
            Fiche.MONGO_LOCATION ~~> self.mongoLocation?.toJSON(),
            Fiche.GOOGLE_MAP_LOCATION ~~> self.googleMapLocation?.toJSON(),
            Fiche.SHORT_DESCRIPTION ~~> self.shortDescription,
            Fiche.LONG_DESCRIPTION ~~> self.longDescription,
            FichePeople.SURNAME ~~> self.surname,
            Gloss.Encoder.encode(dateForKey: FichePeople.DATE, dateFormatter: MongoEntity.getDateFormatter())(self.date),
            Gloss.Encoder.encode(dateForKey: FichePeople.BIRTHDAY, dateFormatter: MongoEntity.getDateFormatter())(self.birthday),
            Gloss.Encoder.encode(dateForKey: FichePeople.DEATHDAY, dateFormatter: MongoEntity.getDateFormatter())(self.deathday),
            FichePeople.DELTA_START ~~> self.deltaStart,
            FichePeople.PLACE_OF_BIRTH ~~> self.placeOfBirth,
            FichePeople.DELTA_END ~~> self.deltaEnd,
            [ItemEntity.CATEGORIES: jsonCategories],
            [ItemEntity.TAGS: jsonTags],
            [Fiche.THEMES: jsonThemes]
            ])
    }
}

//
//  DiscoveryViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Hydra
import CoreLocation

class DiscoveryViewController: UITableViewController,
    CLLocationManagerDelegate,
    ItemQuickActionBarDelegate {

    private static let EmptyCellIdentifier: String = "EmptyCell"
    private static let EmptyCellHeight: CGFloat = 140
    private static let BigCard: String = "BigCard"
    private static let MixinHorizontalTableViewCell: String = "MixinHorizontalTableViewCell"
    private static let SectionFont: UIFont = UIFont(name: "Roboto-Black", size: 12)!
    private static let SectionHeight: CGFloat = 48
    private static let MixinHorizontalCellHeight: CGFloat = 266
    private static let SegueToPlaylist: String = "segueToPlaylist"
    private static let SegueToFiche: String = "segueToFiche"
//    private static let HeartstrokesCell: String = "HeartstrokesHorizontalCollectionViewCell"

    fileprivate var heartstrokes: [ItemEntity] = []
    private var suggestions: [ItemEntity] = []
    private var storedOffsetOfHeartstrokes: CGFloat = 0
    private var selectedItem: ItemEntity!

    private let locationManager = CLLocationManager()
    private var currentLocation: CLLocationCoordinate2D?

    private var currentPage: Int = 0
    private var isLoadingNextPage: Bool = false

    var isSuggestionsLoading: Bool = true
    var isHeartstrokesLoading: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.

        // Set up navigation bar for all controllers under this ones
        self.navigationController?.hidesNavigationBarHairline = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.textColor]
        self.navigationController?.navigationBar.tintColor = UIColor.textColor
        self.navigationController?.navigationBar.isTranslucent = false

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic-map"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(DiscoveryViewController.displayMap))

        self.tableView.register(
            UINib(nibName: DiscoveryViewController.EmptyCellIdentifier, bundle: nil),
            forCellReuseIdentifier: DiscoveryViewController.EmptyCellIdentifier)

        self.tableView.register(
            UINib(nibName: DiscoveryViewController.MixinHorizontalTableViewCell, bundle: nil),
                                forCellReuseIdentifier: DiscoveryViewController.MixinHorizontalTableViewCell)
        self.tableView.register(
            UINib(nibName: DiscoveryViewController.BigCard, bundle: nil),
            forCellReuseIdentifier: DiscoveryViewController.BigCard)
        // set dynamic table cell dimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 444

        //Get user location
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.distanceFilter = 50
            locationManager.startUpdatingLocation()
        }
        else {
            UserAPI.getSuggestions(with: Const.DEFAULT_RADIUS, latitude: Const.DEFAULT_LATITUDE, longitude: Const.DEFAULT_LONGITUDE, page: self.currentPage + 1, limit: 10, includePlaylist: true).then(in: .main, { (suggestions: [ItemEntity]) in
                self.isSuggestionsLoading = false
                self.suggestions.append(contentsOf: suggestions)
                self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
            }).catch(in: .main) { (_: Error) in
                self.showError(NSLocalizedString("Error! Impossible to load data!", comment: "Error! Impossible to load data!"))
                self.isSuggestionsLoading = false
            }
        }

        // load heart strokes data
        MixAPI.getHeartstrokes().then(in: .main) { (heartstrokes: [ItemEntity]) in
            self.isHeartstrokesLoading = false
            self.heartstrokes = heartstrokes
            self.tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        }
        .catch(in: .main) { (_: Error) in
            self.showError(NSLocalizedString("Error! Impossible to load data!", comment: "Error! Impossible to load data!"))
            self.isHeartstrokesLoading = false
        }
    }

    func onAddClick(fiche: Fiche) {
        ItemQuickActionBarViewController.addFicheToPlaylistHelper(controller: self, ficheToAdd: fiche)
    }

    func displayMap() {
        self.navigationController?.popToRootViewController(animated: false)
        self.navigationController?.pushViewController(MapViewController(), animated: false)
    }

    func updateSuggestions(_ location: CLLocationCoordinate2D?) {
        if !self.isLoadingNextPage {
            self.isLoadingNextPage = true
            self.isSuggestionsLoading = true
            UserAPI.getSuggestions(with: Const.DEFAULT_RADIUS, latitude: (location?.latitude)!, longitude: (location?.longitude)!, page: self.currentPage + 1, limit: 10, includePlaylist: true).then(in: .main, { (suggestions: [ItemEntity]) in
                self.isSuggestionsLoading = false
                self.currentPage += 1
                self.suggestions.append(contentsOf: suggestions)
                self.tableView.reloadData()
                self.isLoadingNextPage = false
            }).catch(in: .main) { (_: Error) in
                self.showError(NSLocalizedString("Error! Impossible to load data!", comment: "Error! Impossible to load data!"))
                self.isSuggestionsLoading = false
                self.isLoadingNextPage = false
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue: CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")

        manager.stopUpdatingLocation()

        if self.currentLocation == nil {
            self.currentLocation = locValue
            self.updateSuggestions(locValue)
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var nRow: Int = 0
        if section == 0 {
            //if !self.heartstrokes.isEmpty {
                nRow = 1
            //}
        } else {
            if !self.suggestions.isEmpty {
                nRow = self.suggestions.count
            }
            else {
                nRow = 1
            }
        }
        return nRow
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return NSLocalizedString("Heartstrokes, Traverse", comment: "Heartstrokes, Traverse")
        } else {
            return NSLocalizedString("Personalized Suggestions", comment: "Personalized Suggestions")
        }
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.textLabel?.textColor = UIColor.textColor
            headerTitle.textLabel?.font = DiscoveryViewController.SectionFont
            //headerTitle.tintColor = UIColor.backgroundDefaultColor

            headerTitle.backgroundView = UIView(frame: headerTitle.bounds)
            headerTitle.backgroundView?.backgroundColor = UIColor.backgroundDefaultColor
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DiscoveryViewController.SectionHeight
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = UITableViewAutomaticDimension
        if indexPath.section == 0 {
            if self.heartstrokes.isEmpty {
                height = DiscoveryViewController.EmptyCellHeight
            }
            else {
                height = DiscoveryViewController.MixinHorizontalCellHeight
            }
        }
        else if indexPath.section == 1 && self.suggestions.isEmpty {
            height = DiscoveryViewController.EmptyCellHeight
        }
        return height
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell()
        if indexPath.section == 0 {
            if self.heartstrokes.isEmpty {
                if let emptyCell: EmptyCell = tableView.dequeueReusableCell(withIdentifier: DiscoveryViewController.EmptyCellIdentifier, for: indexPath) as? EmptyCell {
                    if self.isHeartstrokesLoading {
                        emptyCell.setEmptyMessage("")
                    }
                    else {
                        emptyCell.setEmptyMessage(NSLocalizedString("Error! Impossible to load data!", comment: "Error! Impossible to load data!"))
                    }

                    cell = emptyCell
                }
            }
            else {
                if let horizontalCell: MixinHorizontalTableViewCell =
                    tableView.dequeueReusableCell(
                        withIdentifier: DiscoveryViewController.MixinHorizontalTableViewCell,
                        for: indexPath) as? MixinHorizontalTableViewCell {
                    cell = horizontalCell
                }
            }

        } else {
            if self.suggestions.isEmpty {
                if let emptyCell: EmptyCell = tableView.dequeueReusableCell(withIdentifier: DiscoveryViewController.EmptyCellIdentifier, for: indexPath) as? EmptyCell {
                    if self.isSuggestionsLoading {
                        emptyCell.setEmptyMessage("")
                    }
                    else {
                        emptyCell.setEmptyMessage(NSLocalizedString("Nothing to display near you", comment: "Nothing to display near you"))
                    }
                    cell = emptyCell
                }
            }
            else {
                if let bigCardCell: BigCard =
                    tableView.dequeueReusableCell(withIdentifier: DiscoveryViewController.BigCard,
                                                  for: indexPath) as? BigCard {
                    bigCardCell.set(item: self.suggestions[indexPath.row],
                                    parentController: self,
                                    actionBarDelegate: self)
                    cell = bigCardCell
                }
            }
        }

        return cell
    }

    override func tableView(_ tableView: UITableView,
                            willDisplay cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            // set the delegates for the horizontal UICollectionView
            guard let tableViewCell = cell as? MixinHorizontalTableViewCell else {
                return
            }
            tableViewCell.setCollectionViewDelegate(delegate: self, forRow: indexPath.row)
            // set the offset to go back to the horizontal cell position we left
            tableViewCell.collectionViewOffset = self.storedOffsetOfHeartstrokes
        }

        if indexPath.section == 1 && indexPath.row > self.suggestions.count - 4 && self.suggestions.count > 9 {
            updateSuggestions(self.currentLocation)
        }
    }

    override func tableView(_ tableView: UITableView,
                            didEndDisplaying cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            // set the delegates for the horizontal UICollectionView
            guard let tableViewCell = cell as? MixinHorizontalTableViewCell else {
                return
            }
            // save the offset for when we go back to the horizontal cell
            self.storedOffsetOfHeartstrokes = tableViewCell.collectionViewOffset
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        if indexPath.section != 0 && !self.suggestions.isEmpty {
            if let fiche = self.suggestions[indexPath.row] as? Fiche {
                self.performSegueTo(item: fiche)
            }
            else if let playlist = self.suggestions[indexPath.row] as? Playlist {
                self.performSegueTo(item: playlist)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == DiscoveryViewController.SegueToPlaylist {
            if let destination: DiscoveryPlaylistTableViewController =
                segue.destination as? DiscoveryPlaylistTableViewController {
                if let playlist: Playlist = self.selectedItem as? Playlist {
                    destination.set(playlist: playlist)
                }
            }
        } else if segue.identifier == DiscoveryViewController.SegueToFiche {
            if let destination: DiscoveryFicheViewController =
                segue.destination as? DiscoveryFicheViewController {
                if let fiche: Fiche = self.selectedItem as? Fiche {
                    destination.set(fiche: fiche)
                }
            }
        }
    }

    fileprivate func performSegueTo(item: ItemEntity) {
        self.selectedItem = item
        switch item.type {
        case .playlist:
            performSegue(withIdentifier: DiscoveryViewController.SegueToPlaylist, sender: self)
        default:
            performSegue(withIdentifier: DiscoveryViewController.SegueToFiche, sender: self)
        }
        UserDefaults.standard.set(true, forKey: item.id)
    }
}

extension DiscoveryViewController: UICollectionViewDelegate,
                                    UICollectionViewDataSource,
                                    UICollectionViewDelegateFlowLayout {
    private static let HorizontalSectionInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 8.0, bottom: 0, right: 0)

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return heartstrokes.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell = UICollectionViewCell()
        if let smallCard: SmallCard = collectionView.dequeueReusableCell(
            withReuseIdentifier: MixinHorizontalTableViewCell.SmallCardNib,
            for: indexPath as IndexPath) as? SmallCard {
            let item: ItemEntity = heartstrokes[indexPath.row]
            smallCard.set(item: item)
            cell = smallCard
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return DiscoveryViewController.HorizontalSectionInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let availableWidth: CGFloat = collectionView.frame.width - DiscoveryViewController.HorizontalSectionInsets.left

        //let width: GLfloat = GLfloat((availableWidth / 2.0) + (availableWidth / 4.0))
        let width: GLfloat = GLfloat((availableWidth / 1.8))

        return CGSize(width: Int(width), height: Int(collectionView.frame.height))
    }

    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {

        if let fiche = self.heartstrokes[indexPath.row] as? Fiche {
            self.performSegueTo(item: fiche)
        }
        else if let playlist = self.heartstrokes[indexPath.row] as? Playlist {
            self.performSegueTo(item: playlist)
        }

    }
}

//
//  FicheNewsViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 08.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Material
import DZNEmptyDataSet

private let FicheNewsViewControllerNib: String = "FicheNewsViewController"
private let SocialFeedCellNib: String = "SocialFeedCard"

class FicheNewsViewController: UIViewController,
    UITableViewDelegate,
    UITableViewDataSource,
    DZNEmptyDataSetSource,
    DZNEmptyDataSetDelegate,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    AddFeedDelegate {
    private static let Title: String = NSLocalizedString("News", comment: "News")
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var feedTableView: UITableView!
    @IBOutlet weak var ficheHashtagLabel: UILabel!
    @IBOutlet weak var photoView: UIView!
    private let fiche: Fiche
    private var feeds: [Feed] = []
    private var imagePicker: UIImagePickerController!
    private var parentNavigationController: UINavigationController?

    init(fiche: Fiche, navigationController: UINavigationController?) {
        self.fiche = fiche
        self.parentNavigationController = navigationController
        super.init(nibName: FicheNewsViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.feedTableView.dataSource = self
        self.feedTableView.delegate = self

        // register nib
        self.feedTableView.register(
            UINib(nibName: SocialFeedCellNib, bundle: nil),
            forCellReuseIdentifier: SocialFeedCellNib)
        // set dynamic table cell dimension
        self.feedTableView.rowHeight = UITableViewAutomaticDimension
        self.feedTableView.estimatedRowHeight = 300

        self.feedTableView.emptyDataSetSource = self
        self.feedTableView.emptyDataSetDelegate = self

        // A little trick for removing the cell separators
        self.feedTableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
        self.tabItem.title = FicheNewsViewController.Title
        self.tabItem.titleColor = UIColor.textColor
        self.tabItem.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 14)

        let tapShareHashtagLabel: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.shareHashtag))
        self.ficheHashtagLabel.isUserInteractionEnabled = true
        self.ficheHashtagLabel.addGestureRecognizer(tapShareHashtagLabel)

        let tapShareHashtagPicture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.sharePicture))
        self.photoView.isUserInteractionEnabled = true
        self.photoView.addGestureRecognizer(tapShareHashtagPicture)

        self.imagePicker = UIImagePickerController()
        self.imagePicker?.delegate = self

        ficheHashtagLabel.text = fiche.slug

        getFeed()
    }

    func getFeed() {
        FeedAPI.getFeedForFiche(id: self.fiche.id).then(in: .main) { (feeds: [Feed]) in
            self.feeds = feeds
            self.feedTableView.reloadData()
        }.catch(in: .main) { (_) -> Void in
            self.showError(NSLocalizedString("Error! Impossible to load data!", comment: "Error! Impossible to load data!"))
        }.always(in: .main, body: {
            self.loadingView.isHidden = true
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func shareHashtag() {
        self.copyToClipboard()
        let activityViewController: UIActivityViewController =
            UIActivityViewController(activityItems: [self.ficheHashtagLabel.text ?? ""], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: {})
    }

    func sharePicture() {
        if UserAPI.getCurrentUser() != nil {
            //self.copyToClipboard()

            let optionMenu: UIAlertController = UIAlertController(title: NSLocalizedString("Add a picture", comment: "Add a picture"), message: nil, preferredStyle: .actionSheet)

            optionMenu.view.tintColor = UIColor.textColor
            let backView = optionMenu.view.subviews.last?.subviews.last
            backView?.layer.cornerRadius = 24.0
            backView?.backgroundColor = UIColor.backgroundDefaultColor

            let deleteAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Open camera", comment: "Open camera"), style: .default, handler: {_ in
                self.imagePicker?.sourceType = .camera
                self.present(self.imagePicker!, animated: true, completion: nil)
            })
            let saveAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Open library", comment: "Open library"), style: .default, handler: {_ in
                self.imagePicker?.sourceType = .photoLibrary
                self.present(self.imagePicker!, animated: true, completion: nil)
            })
            let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel, handler: nil)

            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(cancelAction)

            self.present(optionMenu, animated: true, completion: nil)
        } else {
            let alertController: UIAlertController =
                UIAlertController(title: NSLocalizedString("You must login to share a picture!", comment: "You must login to share a picture!"), message: nil, preferredStyle: .alert)

            alertController.view.tintColor = UIColor.textColor
            let backView = alertController.view.subviews.last?.subviews.last
            backView?.layer.cornerRadius = 24.0
            backView?.backgroundColor = UIColor.backgroundDefaultColor

            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel) { _ in
                // ...
            }
            alertController.addAction(cancelAction)

            let okAction = UIAlertAction(title: "OK", style: .default) { _ in
                let loginview: LoginViewController = LoginViewController()
                loginview.parentNavigationController = self.parentNavigationController
                self.present(loginview, animated: true, completion: nil)
            }
            alertController.addAction(okAction)

            self.present(alertController, animated: true) {
                // ...
            }
        }
    }

    // MARK: - Image Picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        self.imagePicker?.dismiss(animated: true, completion: nil)

        if let selectedImage: UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if let user: User = UserAPI.getCurrentUser() {
                let addNewFeedController: AddTraverseFeedViewController = AddTraverseFeedViewController(ficheId: self.fiche.id, tag: self.fiche.slug!, photo: selectedImage, userId: user.id, addFeedDelegate: self)
                self.present(addNewFeedController, animated: true, completion: nil)
            }
        }
    }

    // MARK: - TableView

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feeds.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell()
        if let feedCardCell: SocialFeedCard =
            feedTableView.dequeueReusableCell(withIdentifier: SocialFeedCellNib,
                                          for: indexPath) as? SocialFeedCard {
            feedCardCell.set(feed: self.feeds[indexPath.row], parentController: self)
            cell = feedCardCell
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let title: String = NSLocalizedString("There are no posts for this fiche!",
                                              comment: "There are no posts this fiche!")
        let attributes: [String: Any] = [NSFontAttributeName: UIFont(name: "Roboto-Regular", size: 14.0)!, NSForegroundColorAttributeName: UIColor.textColor]
        return NSAttributedString(string: title, attributes: attributes)
    }

    func feedAdded(feed: Feed) {
        self.feeds.insert(feed, at: 0)
        self.feedTableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    private func copyToClipboard() {
        UIPasteboard.general.string = self.ficheHashtagLabel.text ?? ""
        self.showMessage(NSLocalizedString("Hashtag copied to clipboard!", comment: "Hashtag copied to clipboard!"))
    }
}

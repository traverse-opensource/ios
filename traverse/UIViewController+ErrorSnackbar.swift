//
//  UIviewController+ErrorSnackbar.swift
//  traverse
//
//  Created by Mattia Gustarini on 20.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Material

extension UIViewController {
    func showGenericError(_ asAlert: Bool = false) {
        if asAlert {
            showAlert(NSLocalizedString("Error! An unknown error occurred!", comment: "Error! An unknown error occurred!"))
        } else {
            if let appDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
                if let snackbarViewController = appDelegate.window!.rootViewController as? AppSnackbarController {
                    if let rootViewController: RootViewController =
                        snackbarViewController.rootViewController as? RootViewController {
                        rootViewController.genericError()
                    }
                }
            }
        }
    }

    func showError(_ errorText: String, _ asAlert: Bool = false) {
        if asAlert {
            showAlert(errorText)
        } else {
            if let appDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
                if let snackbarViewController = appDelegate.window!.rootViewController as? AppSnackbarController {
                    if let rootViewController: RootViewController =
                        snackbarViewController.rootViewController as? RootViewController {
                        rootViewController.error(errorText)
                    }
                }
            }
        }
    }

    func showMessage(_ messageText: String, _ asAlert: Bool = false) {
        if asAlert {
            showAlert(messageText)
        } else {
            if let appDelegate: AppDelegate  = UIApplication.shared.delegate as? AppDelegate {
                if let snackbarViewController = appDelegate.window!.rootViewController as? AppSnackbarController {
                    if let rootViewController: RootViewController =
                        snackbarViewController.rootViewController as? RootViewController {
                        rootViewController.message(messageText)
                    }
                }
            }
        }
    }

    private func showAlert(_ message: String) {
        let alertController: UIAlertController =
            UIAlertController(title: message, message: nil, preferredStyle: .alert)

        alertController.view.tintColor = UIColor.textColor
        let backView = alertController.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 24.0
        backView?.backgroundColor = UIColor.backgroundDefaultColor

        let okAction = UIAlertAction(title: NSLocalizedString("Close", comment: "Close"), style: .default) { _ in

        }
        alertController.addAction(okAction)

        self.present(alertController, animated: true) {
            // ...
        }
    }
}

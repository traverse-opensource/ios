//
//  TitleViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 09.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

private let TitleViewControllerNib: String = "TitleViewController"

class TitleViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    private let ficheTitle: String

    init(_ title: String) {
        self.ficheTitle = title
        super.init(nibName: TitleViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.titleLabel.text = self.ficheTitle
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

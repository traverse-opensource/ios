//
//  PlaylistClickDelegate.swift
//  traverse
//
//  Created by Mattia Gustarini on 16.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Foundation

protocol PlaylistClickDelegate: class {
    func onClick(playlist: Playlist)
}

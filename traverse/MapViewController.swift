//
//  MapViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 24.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import TagCellLayout

class MapViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    GMUClusterManagerDelegate,
    UICollectionViewDelegateFlowLayout,
    GMSMapViewDelegate,
    TagCellLayoutDelegate,
    CLLocationManagerDelegate {

    private static let FilterCell: String = "FilterCell"
    private static let MapFicheCellNib: String = "MapFicheCell"
    private static let VerticalSectionInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)

    private let locationManager = CLLocationManager()
    private var currentLocation: CLLocationCoordinate2D?

    @IBOutlet weak var collectionViewHolderHeight: NSLayoutConstraint!
    
    @IBOutlet weak var googleMapButtonImage: UIImageView!
    @IBOutlet weak var itineraryButtonImage: UIImageView!
    
    @IBOutlet weak var itineraryButton: UIView!
    @IBOutlet weak var googleMapButton: UIView!

    @IBOutlet weak var zoomInButton: UIImageView!
    @IBOutlet weak var zoomOutButton: UIImageView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var collectionViewHolder: UIView!
    @IBOutlet weak var mapFicheCollectionView: UICollectionView!
    @IBOutlet weak var myLocationButton: UIImageView!
    @IBOutlet weak var mapViewHolder: UIView!
    private var clusterManager: GMUClusterManager!
    private var mapView: GMSMapView?

    private var fichesToDisplay: [ItemEntity] = []
    private var items: [ItemEntity] = []

    private var isDataLoading: Bool = false

    var exploreMode: Bool = false
    var exploreViewController: ExploreViewController?
    var searchLocation: CLLocationCoordinate2D?

    @IBOutlet weak var filterCollectionView: UICollectionView!
    @IBOutlet weak var filterCollectionViewHeightConstraint: NSLayoutConstraint!

    var activeFilters: Filter?

    var staticMode: Bool = false
    var staticItems: [ItemEntity]?
    var isPlaylist: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setHidesBackButton(true, animated: false)

        if exploreMode {
            self.title = NSLocalizedString("Explore", comment: "Explore")
        }
        else {
            self.title = NSLocalizedString("Discovery", comment: "Discovery")
        }

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic-liste-v2"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(MapViewController.displayList))

        // Create a waterfall layout
        let tagCellLayout: TagCellLayout =
            TagCellLayout(tagAlignmentType: .left, delegate: self)
        // Add the waterfall layout to your collection view
        self.filterCollectionView.collectionViewLayout = tagCellLayout

        // Set up the nib for the cell
        let viewNib: UINib = UINib(nibName: MapViewController.FilterCell,
                                   bundle: nil)
        self.filterCollectionView.register(viewNib,
                                           forCellWithReuseIdentifier: MapViewController.FilterCell)

        self.mapFicheCollectionView.register(
            UINib(nibName: MapViewController.MapFicheCellNib,
                  bundle: nil),
            forCellWithReuseIdentifier: MapViewController.MapFicheCellNib)

        self.mapFicheCollectionView.delegate = self
        self.mapFicheCollectionView.dataSource = self

        self.filterCollectionView.delegate = self
        self.filterCollectionView.dataSource = self

        self.loadingView.isHidden = false
        self.googleMapButton.isHidden = true
        self.itineraryButton.isHidden = true

        self.googleMapButtonImage.tintColor = UIColor.lightGray
        self.itineraryButtonImage.tintColor = UIColor.lightGray

        //Get user location
        self.locationManager.requestAlwaysAuthorization()

        self.myLocationButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MapViewController.myLocationButtonPressed)))

        self.zoomInButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MapViewController.zoomIn)))
        self.zoomOutButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MapViewController.zoomOut)))

        self.googleMapButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MapViewController.displayItemOnGoogleMap)))
        self.itineraryButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MapViewController.displayTripOnGoogleMap)))

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if self.mapView == nil {

            let camera = GMSCameraPosition.camera(withLatitude: Const.DEFAULT_LATITUDE, longitude: Const.DEFAULT_LONGITUDE, zoom: 13.0)
            //self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
            self.mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), camera: camera)
            self.mapView?.isMyLocationEnabled = true

            let iconGenerator = GMUDefaultClusterIconGenerator()
            let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
            let renderer = GMUDefaultClusterRenderer(mapView: mapView!,
                                                     clusterIconGenerator: iconGenerator)
            clusterManager = GMUClusterManager(map: self.mapView!, algorithm: algorithm,
                                               renderer: renderer)

            clusterManager.setDelegate(self, mapDelegate: self)

            do {
                if let styleURL = Bundle.main.url(forResource: "style_json", withExtension: "json") {
                    self.mapView?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                } else {
                    NSLog("Unable to find style_json.json")
                }
            } catch {
                NSLog("One or more of the map styles failed to load. \(error)")
            }

            mapViewHolder.addSubview(mapView!)

            if !staticMode {
                if CLLocationManager.locationServicesEnabled() && self.searchLocation == nil {
                    self.locationManager.delegate = self
                    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                    self.locationManager.distanceFilter = 50
                    locationManager.startUpdatingLocation()
                }
                else if self.searchLocation != nil {
                    self.currentLocation = searchLocation
                    mapView?.animate(to: GMSCameraPosition.camera(withLatitude: (self.currentLocation?.latitude)!, longitude: (self.currentLocation?.longitude)!, zoom: 13))
                    updateMapItems(with: self.currentLocation!)
                }
            }
            else {
                if CLLocationManager.locationServicesEnabled() {
                    self.locationManager.delegate = self
                    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                    self.locationManager.distanceFilter = 50
                    locationManager.startUpdatingLocation()
                }
                loadItems(staticItems!)

                if isPlaylist {
                    var bounds: GMSCoordinateBounds = GMSCoordinateBounds()
                    for item in staticItems! {
                        if let fiche = item as? Fiche {
                            bounds = bounds.includingCoordinate(CLLocationCoordinate2DMake((fiche.latitude)!, (fiche.longitude)!))
                        }
                    }

                    self.loadingView.isHidden = true
                    self.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100.0))
                }
                else {
                    self.loadingView.isHidden = true
                    //There is normally only one item in the list
                    if !(self.staticItems?.isEmpty)!, let item = self.staticItems![0] as? Fiche {
                        mapView?.animate(to: GMSCameraPosition.camera(withLatitude: item.latitude!, longitude: item.longitude!, zoom: 15))
                    }
                    else {
                        mapView?.animate(to: GMSCameraPosition.camera(withLatitude: (self.currentLocation?.latitude)!, longitude: (self.currentLocation?.longitude)!, zoom: 13))
                    }

                }
            }

        }
    }

    func displayItemOnGoogleMap() {
        if !self.fichesToDisplay.isEmpty {
            if let fiche = self.fichesToDisplay[0] as? Fiche {
                UIApplication.shared.openURL(URL(string: "comgooglemaps://?center=\(fiche.latitude!),\(fiche.longitude!)")!)
            }

        }
    }

    func displayTripOnGoogleMap() {
        if !self.fichesToDisplay.isEmpty {
            if let fiche = self.fichesToDisplay[0] as? Fiche {
                UIApplication.shared.openURL(URL(string: "comgooglemaps://?daddr=\(fiche.latitude!),\(fiche.longitude!)")!)
            }

        }
    }

    func zoomIn() {
        self.mapView?.animate(toZoom: (self.mapView?.camera.zoom)! + 1)
    }

    func zoomOut() {
        self.mapView?.animate(toZoom: (self.mapView?.camera.zoom)! - 1)
    }

    func myLocationButtonPressed() {
        self.updateCollectionViewConstraints()

        if self.currentLocation != nil {

            if !self.fichesToDisplay.isEmpty {
                self.fichesToDisplay.removeAll()
                self.collectionViewHolderHeight.priority = 999
                self.mapFicheCollectionView.reloadData()
            }
            
            let camera = GMSCameraPosition.camera(withLatitude: (self.currentLocation?.latitude)!, longitude: (self.currentLocation?.longitude)!, zoom: 13.0)
            self.mapView?.animate(to: camera)
        }
    }

    func displayList() {

        if self.exploreViewController != nil {
            self.exploreViewController?.activeFilters = self.activeFilters!
            self.exploreViewController?.mayNeedRefresh = true
        }
        self.navigationController?.popViewController(animated: false)

    }

//    private func generateClusterItems() {
//        clusterManager.
//        for item in self.items {
//            if let ficheItem = item as? Fiche {
//                
//            }
//        }
//
//        self.loadingView.isHidden = true
//        clusterManager.cluster()
//    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue: CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")

        manager.stopUpdatingLocation()

        if self.currentLocation == nil {
            self.currentLocation = locValue

            if !staticMode {
                mapView?.animate(to: GMSCameraPosition.camera(withLatitude: (self.currentLocation?.latitude)!, longitude: (self.currentLocation?.longitude)!, zoom: 13))

                updateMapItems(with: self.currentLocation!)
            }
        }
    }

    func updateMapItems(with position: CLLocationCoordinate2D) {
        self.loadingView.isHidden = false

        var radius = Const.DEFAULT_RADIUS

        if exploreMode {
            let center = CLLocation(latitude: (self.mapView?.getCenterCoordinate().latitude)!, longitude: (self.mapView?.getCenterCoordinate().longitude)!)
            let top = CLLocation(latitude: (self.mapView?.getTopCenterCoordinate().latitude)!, longitude: (self.mapView?.getTopCenterCoordinate().longitude)!)
            let distance = center.distance(from: top)

            radius = Int(distance * 1.25)
        }

        MixAPI.getMixins(with: radius, latitude: position.latitude, longitude: position.longitude, page: 1, limit: -1, filter: self.activeFilters).then(in: .main, { (newItems: [ItemEntity]) in

            self.loadItems(newItems)

            self.loadingView.isHidden = true
            self.clusterManager.cluster()

            if !self.exploreMode {
                if self.items.isEmpty {
                    self.showMessage(NSLocalizedString("Nothing to display near you", comment: "Nothing to display near you"))
                }
            }

        }).catch(in: .main) { (_: Error) in
            self.loadingView.isHidden = true
            self.showError(NSLocalizedString("Error! Impossible to load data!", comment: "Error! Impossible to load data!"))
        }

    }

    func loadItems(_ newItems: [ItemEntity]) {
        if !staticMode || !isPlaylist {
            for newItem in newItems {
                if !self.items.contains(where: { item in item.id == newItem.id }) {
                    self.items.append(newItem)
                    if let ficheItem = newItem as? Fiche {

                        if ficheItem.latitude != nil && ficheItem.longitude != nil {
                            let cluster =
                                MapCluster(position: CLLocationCoordinate2DMake((ficheItem.latitude)!, (ficheItem.longitude)!), name: ficheItem.name != nil ? ficheItem.name! : "", item: ficheItem)
                            self.clusterManager.add(cluster)
                        }
                    }
                }
            }
        }
        else {
            self.items.removeAll()
            for (index, item) in newItems.enumerated() {
                    self.items.append(item)
                    if let ficheItem = item as? Fiche {
                        if ficheItem.latitude != nil && ficheItem.longitude != nil {
                            let cluster = MapCluster(position: CLLocationCoordinate2DMake((ficheItem.latitude)!, (ficheItem.longitude)!), name: ficheItem.name != nil ? ficheItem.name! : "", item: ficheItem, isInPlaylist: true, itemIndex: index)
                                //MapCluster(position: , name: , item: ficheItem)
                            self.clusterManager.add(cluster)
                        }
                    }
            }

        }

    }

    func toggleGoogleMapsAndTripButtons() {
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            self.googleMapButton.isHidden = self.fichesToDisplay.isEmpty
            self.itineraryButton.isHidden = self.fichesToDisplay.isEmpty
        }

    }

    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if !gesture {
            return
        }
        if !self.fichesToDisplay.isEmpty {
            self.fichesToDisplay.removeAll()
            self.collectionViewHolderHeight.priority = 999
            self.mapFicheCollectionView.reloadData()

            toggleGoogleMapsAndTripButtons()
        }
    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if exploreMode {
            updateMapItems(with: position.target)
        }
    }

    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {

        self.fichesToDisplay.removeAll()

        for item in cluster.items {
            if let mapCluster = item as? MapCluster {
                self.fichesToDisplay.append(mapCluster.item!)
            }
        }

        self.collectionViewHolderHeight.priority = 997
        self.mapFicheCollectionView.reloadData()

        toggleGoogleMapsAndTripButtons()

        return false
    }

    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {

        self.fichesToDisplay.removeAll()

        if let mapCluster = clusterItem as? MapCluster {
            self.fichesToDisplay.append(mapCluster.item!)
        }

        self.collectionViewHolderHeight.priority = 997
        self.mapFicheCollectionView.reloadData()

        toggleGoogleMapsAndTripButtons()

        return false
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.updateCollectionViewConstraints()
    }

    func updateCollectionViewConstraints() {
        if self.filterCollectionView.numberOfItems(inSection: 0) > 0 {
            let height: CGFloat = self.filterCollectionView.collectionViewLayout.collectionViewContentSize.height
            self.filterCollectionViewHeightConstraint.constant = height
        }
        else {
            self.filterCollectionViewHeightConstraint.constant = 0
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == filterCollectionView {
            if self.activeFilters == nil {
                return 0
            }
            return self.activeFilters!.types.count + self.activeFilters!.themes.count + self.activeFilters!.categories.count + self.activeFilters!.tags.count
        }
        else {
            return self.fichesToDisplay.count
        }

    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        var cell: UICollectionViewCell = UICollectionViewCell()

        if collectionView == filterCollectionView {
            if let filterCell: FilterCell =
                collectionView.dequeueReusableCell(withReuseIdentifier: MapViewController.FilterCell,
                                                   for: indexPath) as? FilterCell {
                filterCell.set(label: Filter.getFilterLabelForIndex(self.activeFilters!, index: indexPath.row), filter: Filter.getFilterForIndex(self.activeFilters!, index: indexPath.row))
                cell = filterCell
            }
        }
        else {
            if let mapFicheCell: MapFicheCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: MapViewController.MapFicheCellNib,
                for: indexPath as IndexPath) as? MapFicheCell {

                let item: ItemEntity = fichesToDisplay[indexPath.row]
                mapFicheCell.set(item, currentLocation: self.currentLocation)
                cell = mapFicheCell
            }
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == filterCollectionView {
            return UIEdgeInsets()
        }
        else {
            return MapViewController.VerticalSectionInsets
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        if collectionView == filterCollectionView {
            return CGSize(width: Int(collectionView.frame.width), height: 32)
        }
        else {
            return CGSize(width: Int(collectionView.frame.width), height: 128)
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {

        if collectionView == filterCollectionView {

            self.activeFilters?.removeItemAt(indexPath.row)

            self.items.removeAll()
            self.clusterManager.clearItems()

            updateMapItems(with: self.currentLocation!)

            self.filterCollectionView.reloadData()
            self.updateCollectionViewConstraints()
            self.view.layoutIfNeeded()
        }
        else {
            if !self.isDataLoading {
                if let fiche = self.fichesToDisplay[indexPath.row] as? Fiche {
                    isDataLoading = true
                    self.loadingView.isHidden = false
                    FicheAPI.getFiche(with: fiche.id).then({ (fiche) in
                        self.isDataLoading = false
                        self.loadingView.isHidden = true
                        UserDefaults.standard.set(true, forKey: fiche.id)
                        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "DiscoveryFicheDetails") as? DiscoveryFicheViewController else {
                            print("Could not instantiate view controller with identifier of type DiscoveryFicheViewController")
                            return
                        }

                        vc.set(fiche: fiche)

                        self.navigationController?.pushViewController(vc, animated:true)

                    }).catch({ (error) -> (Void) in
                        self.showError("Failed loading fiche")
                    })
                }
            }

        }
    }

    func tagCellLayoutTagWidth(_ layout: TagCellLayout, atIndex index: Int) -> CGFloat {
        let text = Filter.getFilterLabelForIndex(self.activeFilters!, index: index)
        let width: CGFloat = TagCellLayout.textWidth(text, font: UIFont(name: "Roboto-Regular", size: 12.0)!)
        return width + 16.0 + 32.0
    }

    func tagCellLayoutTagFixHeight(_ layout: TagCellLayout) -> CGFloat {
        return CGFloat(32.0)
    }
}

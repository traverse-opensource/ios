//
//  LinkedFichesViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 28.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Hydra

private let BigLinkedFichesViewControllerNib: String = "BigLinkedFichesViewController"

class BigLinkedFichesViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {

    private static let BigFicheCell: String = "BigFicheCell"
    private static let CellHeight: CGFloat = 72.0
    private static let LastCellHeightDifference: CGFloat = 8

    @IBOutlet weak var linkedFichesCollectionView: UICollectionView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!

    private var fiches: [Fiche] = []
    private weak var fichesDelegate: FicheClickDelegate!

    init(linkedFiches: [LinkedFiche], delegate: FicheClickDelegate) {
        for linkedFiche: LinkedFiche in linkedFiches {
            self.fiches.append(linkedFiche.fiche)
        }
        self.fichesDelegate = delegate
        super.init(nibName: BigLinkedFichesViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.linkedFichesCollectionView.dataSource = self
        self.linkedFichesCollectionView.delegate = self

        // Set up the nib for the cell
        let viewNib: UINib = UINib(nibName: BigLinkedFichesViewController.BigFicheCell,
                                   bundle: nil)
        self.linkedFichesCollectionView.register(viewNib,
                                          forCellWithReuseIdentifier: BigLinkedFichesViewController.BigFicheCell)
        self.resetView(numberOfFiches: self.fiches.count)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return self.fiches.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        var cell: UICollectionViewCell = UICollectionViewCell()
        if let ficheCell: BigFicheCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: BigLinkedFichesViewController.BigFicheCell,
                                               for: indexPath) as? BigFicheCell {
            ficheCell.set(fiche: self.fiches[indexPath.row],
                          index: indexPath.row,
                          last: indexPath.row == self.fiches.count - 1)
            cell = ficheCell
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = UIScreen.main.bounds
        let cellWidth: CGFloat = screenSize.width - 48.0
        var cellHeight: CGFloat = BigLinkedFichesViewController.CellHeight
        if indexPath.row == self.fiches.count - 1 {
            cellHeight -= BigLinkedFichesViewController.LastCellHeightDifference
        }
        return CGSize(width: cellWidth,
                      height: cellHeight)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        let fiche: Fiche = self.fiches[indexPath.row]
        UserDefaults.standard.set(true, forKey: fiche.id)
        if let cell: BigFicheCell =
            collectionView.cellForItem(at: indexPath) as? BigFicheCell {
            cell.setAsSeen()
        }

        self.fichesDelegate.onClick(fiche: fiche)
    }

    private func resetView(numberOfFiches: Int) {
        self.heightConstraint.constant =
            BigLinkedFichesViewController.CellHeight * CGFloat(numberOfFiches) -
            BigLinkedFichesViewController.LastCellHeightDifference
        self.view.layoutIfNeeded()
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//    }
}

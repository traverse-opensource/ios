//
//  User.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class User: MongoEntity {
    static let EMAIL: String = "email"
    static let GROUP: String = "groups"
    static let ROLE: String = "role"
    static let PROFILE: String = "profile"
    static let FAVORITES: String = "favorites"
    static let RESET_PASSWORD_TOKEN: String = "resetPasswordToken"
    static let RESET_PASSWORD_EXP: String = "resetPasswordExpires"

    var email: String?
    var role: Role?
    let groups: [Group]
    var profile: UserProfile?
    var resetPasswordToken: String?
    var resetPasswordExpires: Date?
    var favorites: [String]?

    required init?(json: JSON) {
//        guard let email: String = User.EMAIL <~~ json,
//            let role: Role = User.GROUP <~~ json,
        guard let groups: [Group] = User.GROUP <~~ json
//            let profile: UserProfile = User.PROFILE <~~ json
            else {
                return nil
        }

        self.email = User.EMAIL <~~ json
        self.role = User.ROLE <~~ json
        self.groups = groups
        self.profile = User.PROFILE <~~ json
        self.resetPasswordToken = User.RESET_PASSWORD_TOKEN <~~ json

        if let _: String = User.RESET_PASSWORD_EXP <~~ json {
            let dateFormatter: DateFormatter = MongoEntity.getDateFormatter()
            self.resetPasswordExpires =
                Decoder.decode(dateForKey: User.RESET_PASSWORD_EXP, dateFormatter: dateFormatter)(json)!
        } else {
            self.resetPasswordExpires = nil
        }

        self.favorites = User.FAVORITES <~~ json

        super.init(json: json)
    }

    func toJSON() -> JSON? {

        var jsonGroups: [JSON] = []
        groups.forEach { (group) in
            jsonGroups.append(group.toJSON()!)
        }

        return jsonify([
            "_id" ~~> self.id,
            "email" ~~> self.email,
            "role" ~~> self.role?.toJSON(),
            "resetPasswordToken" ~~> self.resetPasswordToken,
            "resetPasswordExpires" ~~> self.resetPasswordExpires,
            ["groups" : jsonGroups],
            "profile" ~~> self.profile?.toJSON(),
            "favorites" ~~> self.favorites
        ])
    }
}

//
//  ProfileViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 31.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var userPicture: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var helpView: UILabel!
    @IBOutlet weak var logoutView: UILabel!
    @IBOutlet weak var feedbackView: UILabel!
    @IBOutlet weak var aboutView: UILabel!
    @IBOutlet weak var tutorialView: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up navigation bar for all controllers under this ones
        self.navigationController?.hidesNavigationBarHairline = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.textColor]
        self.navigationController?.navigationBar.tintColor = UIColor.textColor
        self.navigationController?.navigationBar.isTranslucent = false

        logoutView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.logout)))

        helpView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.displayTermsOfUse)))

        feedbackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.displayFeedbackForm)))

        aboutView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.displayAbout)))

        tutorialView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.displayTutorial)))

        if let user = UserAPI.getCurrentUser() {
            if let pictureUrl = user.profile?.getPictureUrl() {
                self.userPicture.hnk_setImageFromURL(pictureUrl, placeholder: #imageLiteral(resourceName: "default_contributor_big"), format: nil, failure: nil, success: {(image) in
                    self.userPicture.image = image
                })
            }

            usernameLabel.text = user.profile?.name

        }
        else {
            //This should never happens!
            print("No user, force logout..")
            logout()
        }
    }

    func displayTutorial() {
        let tutorialIntro = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tutorialIntro") as! TutuorialIntroViewController
        tutorialIntro.parentController = self
        self.present(tutorialIntro, animated: true, completion: nil)
    }

    func displayAbout() {
        let url = "https://www.traverse-patrimoines.com/partenaires/"
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: url)!, options: [:]) { (_) in
                //Nothing to do
            }
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(URL(string: url)!)
        }
    }

    func displayTermsOfUse() {
        let url = "https://traverse-patrimoines.com/policies/utilisation.pdf"
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: url)!, options: [:]) { (_) in
                //Nothing to do
            }
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(URL(string: url)!)
        }
    }

    func displayFeedbackForm() {
        let url = "https://traversepep.typeform.com/to/lDJQYk"
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: url)!, options: [:]) { (_) in
                //Nothing to do
            }
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(URL(string: url)!)
        }
    }

    func logout() {
        UserAPI.logout()
        AppDelegate.resetApplication()
    }
}

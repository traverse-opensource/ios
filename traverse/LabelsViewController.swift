//
//  LabelView.swift
//  traverse
//
//  Created by Mattia Gustarini on 27.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import TagCellLayout

private let LabelsViewControllerNib: String = "LabelsViewController"
private let LabelCellNib: String = "LabelCell"

class LabelsViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    TagCellLayoutDelegate {

    @IBOutlet weak var labelCollectionView: UICollectionView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!

    private let labels: [String]
    private let theme: ItemTheme
    private let inCell: Bool

    init(labels: [String], theme: ItemTheme, inCell: Bool! = true) {
        self.labels = labels
        self.theme = theme
        self.inCell = inCell
        super.init(nibName: LabelsViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        // Attach datasource and delegate
        self.labelCollectionView.dataSource  = self
        self.labelCollectionView.delegate = self

        // Create a waterfall layout
        let tagCellLayout: TagCellLayout =
            TagCellLayout(tagAlignmentType: .left, delegate: self)
        // Add the waterfall layout to your collection view
        self.labelCollectionView.collectionViewLayout = tagCellLayout

        // Se the nib for the cell
        let viewNib: UINib = UINib(nibName: LabelCellNib, bundle: nil)
        self.labelCollectionView.register(viewNib, forCellWithReuseIdentifier: LabelCellNib)

        self.labelCollectionView.reloadData()
        DispatchQueue.main.async {
            if !self.inCell {
                let height: CGFloat = self.labelCollectionView.collectionViewLayout.collectionViewContentSize.height
                self.heightConstraint.constant = height
            }

//            self.view.setNeedsLayout()
//            self.view.layoutIfNeeded()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//    func set(labels: [String], theme: ItemTheme) {
//        self.labels = labels
//        self.theme = theme
//        self.labelCollectionView.reloadData()
//    }

    func tagCellLayoutTagFixHeight(_ layout: TagCellLayout) -> CGFloat {
        return CGFloat(32.0)
    }

    func getSize() -> CGSize {
        // If the cell's size has to be exactly the content
        // Size of the collection View, just return the
        // collectionViewLayout's collectionViewContentSize.
        return self.labelCollectionView.collectionViewLayout.collectionViewContentSize
    }

//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)

//        let height: CGFloat = self.labelCollectionView.collectionViewLayout.collectionViewContentSize.height
//        self.heightConstraint.constant = height
//
//        self.view.invalidateIntrinsicContentSize()
//        self.view.setNeedsLayout()
//        self.view.layoutIfNeeded()
//    }

//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//
//

//        let height: CGFloat = self.labelCollectionView.collectionViewLayout.collectionViewContentSize.height
//        self.heightConstraint.constant = height
//        self.view.updateConstraints()
//        self.labelCollectionView.updateConstraints()
//        self.updateViewConstraints()
//        self.container?.updateConstraints()
//        self.view.invalidateIntrinsicContentSize()
//        self.labelCollectionView.invalidateIntrinsicContentSize()
//
//        self.view.layoutIfNeeded()
//        self.labelCollectionView.layoutIfNeeded()
//    }

    func tagCellLayoutTagWidth(_ layout: TagCellLayout, atIndex index: Int) -> CGFloat {

        let width: CGFloat = TagCellLayout.textWidth(self.labels[index],
                                            font: UIFont(name: "Roboto-Regular", size: 12.0)!)
        return width + 24.0
    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return self.labels.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        var cell: UICollectionViewCell = UICollectionViewCell()
        if let labelCell: LabelCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: LabelCellNib,
                                               for: indexPath) as? LabelCell {
            labelCell.set(label: self.labels[indexPath.row], theme: self.theme)
            cell = labelCell
        }

        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

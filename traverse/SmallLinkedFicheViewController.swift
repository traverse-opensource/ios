//
//  SmallLinkedFicheViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

private let SmallLinkedFicheViewControllerNib: String = "SmallLinkedFicheViewController"

class SmallLinkedFicheViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {
    private static let SmallFicheCell: String = "SmallFicheCell"

    @IBOutlet weak var nextValueView: UIView!
    @IBOutlet weak var linkValueLabel: UILabel!
    @IBOutlet weak var nextImage: UIImageView!
    @IBOutlet weak var ficheCollectionView: UICollectionView!
    @IBOutlet weak var nextValueViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var wrapperHeightConstraint: NSLayoutConstraint!

    private var currentFiche: Fiche!
    private var currentIndex: Int!
    private var isLast: Bool = false
    private var fiches: [Fiche] = []
    private var nextsValues: [String] = []
    private weak var fichesDelegate: FicheClickDelegate!

    init(linkedFiches: [LinkedFiche],
         currentFiche: Fiche,
         delegate: FicheClickDelegate? = nil) {
        self.fichesDelegate = delegate
        self.currentFiche = currentFiche
        self.fiches = []
        self.nextsValues = []
        var index: Int = 0
        for linkedFiche: LinkedFiche in linkedFiches {
            self.fiches.append(linkedFiche.fiche)
            if self.currentFiche.id == linkedFiche.fiche.id {
                self.currentIndex = index
            }
            if linkedFiche.hasNext() {
                self.nextsValues.append(linkedFiche.value ?? "")
            } else {
                if self.currentFiche.id == linkedFiche.fiche.id {
                    self.isLast = true
                }
            }
            index += 1
        }
        super.init(nibName: SmallLinkedFicheViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.ficheCollectionView.dataSource = self
        self.ficheCollectionView.delegate = self

        // Set up the nib for the cell
        let viewNib: UINib = UINib(nibName: SmallLinkedFicheViewController.SmallFicheCell,
                                   bundle: nil)
        self.ficheCollectionView.register(viewNib,
                                          forCellWithReuseIdentifier: SmallLinkedFicheViewController.SmallFicheCell)
        setUpView()

        // Add touch action to the label view
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector (self.tapLabel (_:)))
        self.nextValueView.addGestureRecognizer(gesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return self.fiches.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell = UICollectionViewCell()
        if let ficheCell: SmallFicheCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: SmallLinkedFicheViewController.SmallFicheCell,
                                               for: indexPath) as? SmallFicheCell {
            let fiche: Fiche = self.fiches[indexPath.row]
            ficheCell.set(fiche: fiche,
                          index: indexPath.row,
                          selected: self.currentFiche.id == fiche.id)
            cell = ficheCell
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 72.0, height: 88.0)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        let fiche: Fiche = self.fiches[indexPath.row]
        if self.currentFiche.id != fiche.id {
            UserDefaults.standard.set(true, forKey: fiche.id)
            self.fichesDelegate.onClick(fiche: fiche)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if self.fichesDelegate != nil &&
            self.currentIndex != indexPath.row {
            let fiche: Fiche = self.fiches[indexPath.row]
            self.fichesDelegate.onClick(fiche: fiche)
        }
    }

    private func setUpView() {
        self.ficheCollectionView.reloadData()
        DispatchQueue.main.async {
            self.ficheCollectionView.scrollToItem(at: IndexPath(row: self.currentIndex, section: 0),
                                              at: UICollectionViewScrollPosition.centeredHorizontally,
                                              animated: false)
        }
        if isLast {
            self.wrapperHeightConstraint.constant =
                self.wrapperHeightConstraint.constant - self.nextValueViewHeightConstraint.constant
            self.nextValueViewHeightConstraint.constant = 0
            self.nextImage.isHidden = true
            self.linkValueLabel.isHidden = true
            self.view.setNeedsLayout()
        } else {
            let nextFiche: Fiche = self.fiches[self.currentIndex + 1]
            let theme: ItemTheme = nextFiche.mainTheme
            if let themeColor: UIColor = theme.getThemeColor() {
                self.nextValueView.backgroundColor = themeColor
                self.linkValueLabel.text = self.nextsValues[self.currentIndex]
            }
        }
    }

    @objc private func tapLabel(_ sender: UITapGestureRecognizer) {
        if !isLast {
            if self.fichesDelegate != nil {
                let fiche: Fiche = self.fiches[currentIndex + 1]
                self.fichesDelegate.onClick(fiche: fiche)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

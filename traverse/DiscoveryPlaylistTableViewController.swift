//
//  DiscoveryPlaylistTableViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 28.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Material
import DZNEmptyDataSet

class DiscoveryPlaylistTableViewController: UITableViewController,
    FicheClickDelegate,
    ItemQuickActionBarDelegate,
    DZNEmptyDataSetSource,
    DZNEmptyDataSetDelegate {
    public static let StoryBoardReference: String = "DiscoveryPlaylistTable"
    private static let Title: String = NSLocalizedString("Related Playlists", comment: "Related Playlists")
    private static let BigCard: String = "BigCard"
    private static let SegueToFiche: String = "segueToFiche"
    private var playlists: [Playlist] = []
    private var selectedFiche: Fiche!
    private var isTab: Bool = false
    private weak var playlistDelegate: PlaylistClickDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self

        // A little trick for removing the cell separators
        self.tableView.tableFooterView = UIView()

        // register nib
        self.tableView.register(
            UINib(nibName: DiscoveryPlaylistTableViewController.BigCard, bundle: nil),
            forCellReuseIdentifier: DiscoveryPlaylistTableViewController.BigCard)
        // set dynamic table cell dimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 444
        // set tab view properties if it is a tab
        self.tabItem.title = DiscoveryPlaylistTableViewController.Title
        self.tabItem.titleColor = UIColor.textColor
        self.tabItem.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 14)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic-map"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(DiscoveryPlaylistTableViewController.displayMap))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func set(playlist: Playlist) {
        self.isTab = false
        self.playlists = []

        PlaylistAPI.getPlaylist(with: playlist.id).then { (playlist) in
            self.playlists.append(playlist)
            self.tableView.reloadData()
        }
            .catch { (_) -> Void in
                self.playlists.append(playlist)
                self.tableView.reloadData()
        }
    }

    public func updatePlaylist(_ playlist: Playlist) {
        self.playlists = [playlist]
        self.tableView.reloadData()
    }

    func setRelatedFrom(fiche: Fiche, playlistDelegate: PlaylistClickDelegate) {
        self.isTab = true
        self.playlistDelegate = playlistDelegate
        
        FicheAPI.getFichePlaylists(with: fiche.id).then(in: .main) { (playlists: [Playlist]) in
            self.playlists = playlists
            self.tableView.reloadData()
        }
    }

    func displayMap() {
        let mapView = MapViewController()

        var items: [ItemEntity] = []

        if !self.playlists.isEmpty && self.playlists[0].linkedFiches != nil {
            for item in self.playlists[0].linkedFiches! {
                items.append(item.fiche)
            }
        }
        mapView.staticItems = items
        mapView.staticMode = true
        mapView.isPlaylist = true

        //self.navigationController?.popToRootViewController(animated: false)
        self.navigationController?.pushViewController(mapView, animated: false)

    }

    // MARK: - Table view data source

    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        var title: String = ""

        if self.isTab {
            title = NSLocalizedString("There are no related playlist for this fiche!",
                                                  comment: "There are no related playlist for this fiche!")
        } else {
            title = NSLocalizedString("Loading...",
                                      comment: "Loading...")
        }

        let attributes: [String: Any] = [NSFontAttributeName: UIFont(name: "Roboto-Regular", size: 14.0)!,
                                         NSForegroundColorAttributeName: UIColor.textColor]
        return NSAttributedString(string: title, attributes: attributes)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.playlists.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell()
        if let bigCardCell: BigCard =
            tableView.dequeueReusableCell(withIdentifier: DiscoveryPlaylistTableViewController.BigCard,
                                          for: indexPath) as? BigCard {
            let playlist: Playlist = self.playlists[indexPath.row]
            cell = bigCardCell

            if self.isTab && indexPath.row == 0 {
                bigCardCell.set(withTopMargin: playlist,
                                parentController: self,
                                actionBarDelegate: self)
            } else if self.isTab {
                bigCardCell.set(withoutTopMargin: playlist,
                                parentController: self,
                                actionBarDelegate: self)
            } else {
                bigCardCell.set(item: playlist,
                                showLinked: true,
                                parentController: self,
                                actionBarDelegate: self,
                                linkedFichesDelegate: self)
            }
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isTab {
            if self.playlistDelegate != nil {
                let playlist: Playlist = self.playlists[indexPath.row]
                self.playlistDelegate.onClick(playlist: playlist)
            }
        }
    }

    func onClick(fiche: Fiche) {
        self.selectedFiche = fiche
        performSegue(withIdentifier: DiscoveryPlaylistTableViewController.SegueToFiche,
                     sender: self)
    }

    func onAddClick(fiche: Fiche) {
        ItemQuickActionBarViewController.addFicheToPlaylistHelper(controller: self, ficheToAdd: fiche)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == DiscoveryPlaylistTableViewController.SegueToFiche {
            if let destination: DiscoveryFicheViewController =
                segue.destination as? DiscoveryFicheViewController {
                if isTab {
                    destination.set(fiche: self.selectedFiche)
                } else {
                    destination.set(fiche: self.selectedFiche, playlist: self.playlists[0])
                }
            }
        }
    }
}

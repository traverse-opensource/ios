//
//  GalleryPageViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 18.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import SimpleImageViewer

class GalleryPageViewController: UIViewController {

    @IBOutlet weak var arrowLeft: UIImageView!
    @IBOutlet weak var arrowRight: UIImageView!
    @IBOutlet weak var bottomView: UIView!

    @IBOutlet weak var titleTextView: UILabel!
    @IBOutlet weak var descriptionTextView: UILabel!

    @IBOutlet weak var imageBackground: UIImageView!

    var ficheMedia: FicheMedia?
    var position: Int = -1

    var pager: GalleryPager?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrowLeft.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))

        titleTextView.text = ficheMedia?.name

        if let cover: Cover = ficheMedia?.cover {
            self.imageBackground.hnk_setImageFromURL(cover.getBigImageUrl())
            descriptionTextView.text = cover.credit
        }

        arrowLeft.addGestureRecognizer(UITapGestureRecognizer(target: pager, action: #selector(GalleryPager.previousPage)))
        arrowRight.addGestureRecognizer(UITapGestureRecognizer(target: pager, action: #selector(GalleryPager.nextPage)))

        imageBackground.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(GalleryPageViewController.openFullView)))
    }

    func set(_ media: FicheMedia, position: Int, pager: GalleryPager) {
        self.position = position
        self.ficheMedia = media

        self.pager = pager
    }

    func openFullView() {
        let configuration = ImageViewerConfiguration { config in
            config.imageView = imageBackground
        }

        let imageViewerController = ImageViewerController(configuration: configuration)
        for view in imageViewerController.view.subviews {
            //Small trick to force the close button to have transparent background
            if view is UIButton {
                view.backgroundColor = UIColor.clear
            }
        }
        present(imageViewerController, animated: true)
    }
}

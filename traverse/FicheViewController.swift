//
//  FicheViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 08.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Material
import Hydra

private let FicheViewControllerNib: String = "FicheViewController"

class FicheViewController: UIViewController {
    private static let Title: String = "Fiche"

    @IBOutlet weak var placeholdeView: UIView!
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var wrapper: UIView!

    private var relatedMediaPosition: Int = 4
    private var playlistDetailsPosition: Int = 10

    private var fiche: Fiche
    private let playlist: Playlist!
    private weak var fichesDelegate: FicheClickDelegate!
    private weak var actionBarDelegate: ItemQuickActionBarDelegate!

    init(fiche: Fiche,
         playlist: Playlist? = nil,
         actionBarDelegate: ItemQuickActionBarDelegate,
         fichesDelegate: FicheClickDelegate? = nil) {
        self.fiche = fiche
        self.playlist = playlist
        self.actionBarDelegate = actionBarDelegate
        self.fichesDelegate = fichesDelegate
        super.init(nibName: FicheViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the size of the stackview
        let screenSize: CGRect = UIScreen.main.bounds
        let stackViewWidth: CGFloat = screenSize.width - 32.0
        self.contentStackView.frame.size.width = stackViewWidth

        self.wrapper.backgroundColor = UIColor.backgroundDefaultColor
        
        self.tabItem.title = FicheViewController.Title
        self.tabItem.titleColor = UIColor.textColor
        self.tabItem.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 14)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic-map"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(DiscoveryPlaylistTableViewController.displayMap))
        
        // Do any additional setup after loading the view.
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func setUpView() {
        FicheAPI.getFiche(with: self.fiche.id).then({ (fiche) in
            self.fiche = fiche
        }).always {
            DispatchQueue.main.async {
                self.cleanUpContent()
                self.addTitle()
                self.addTags()
                self.addEventDate()
                self.addDescription()
                self.addRelatedMedias()
                self.addLongDescription()
                self.addBibliography()
                self.addAuthor()
                self.addQuickActionBar()

                let labelAdded = self.addLabels()
                let playlistDetailsAdded = self.addPlaylistDetails(labelAdded)
                self.addRelatedFiches(labelAdded || playlistDetailsAdded)
            }
        }
    }

    private func addTitle() {
        let titleController: TitleViewController = TitleViewController(self.fiche.name != nil ? self.fiche.name! : "")
        self.addChildViewController(titleController)
        titleController.didMove(toParentViewController: self)
        self.contentStackView.addArrangedSubview(titleController.view)
        self.view.setNeedsLayout()
    }

    private func addDescription() {
        let descriptionController: DescriptionViewController = DescriptionViewController(self.fiche.shortDescription ?? "")
        self.addChildViewController(descriptionController)
        descriptionController.didMove(toParentViewController: self)
        self.contentStackView.addArrangedSubview(descriptionController.view)
    }

    private func addLongDescription() {
        if let longDescription: String = self.fiche.longDescription, !longDescription.isEmpty {
            let longDescriptionController: ExpandableDescriptionViewController =
                ExpandableDescriptionViewController(longDescription)
            self.addChildViewController(longDescriptionController)
            longDescriptionController.didMove(toParentViewController: self)
            self.contentStackView.addArrangedSubview(longDescriptionController.view)
        } else {
            self.playlistDetailsPosition -= 1
        }
    }

    private func addBibliography() {
        if let bibliography: String = self.fiche.references, !bibliography.isEmpty {
            let bibliographyController: BibliographyViewController = BibliographyViewController(bibliography)
            self.addChildViewController(bibliographyController)
            bibliographyController.didMove(toParentViewController: self)
            self.contentStackView.addArrangedSubview(bibliographyController.view)
        } else {
            self.playlistDetailsPosition -= 1
        }
    }

    private func addEventDate() {
        if let eventFiche: FicheEvent = self.fiche as? FicheEvent {
            let eventDateController: EventDateViewController = EventDateViewController(eventFiche)
            self.addChildViewController(eventDateController)
            eventDateController.didMove(toParentViewController: self)
            self.contentStackView.addArrangedSubview(eventDateController.view)
        } else {
            self.relatedMediaPosition -= 1
            self.playlistDetailsPosition -= 1
        }
    }

    private func addTags() {
        let tags: [Tag] = self.fiche.tags!
        if !tags.isEmpty {
            let tagsViewController: TagsViewController = TagsViewController(tags: tags, theme: self.fiche.mainTheme)
            self.addChildViewController(tagsViewController)
            tagsViewController.didMove(toParentViewController: self)
            self.contentStackView.addArrangedSubview(tagsViewController.view)
        } else {
            self.relatedMediaPosition -= 1
            self.playlistDetailsPosition -= 1
        }
    }

    private func addAuthor() {
        if let userProfile: UserProfile = self.fiche.createdBy?.profile {
            let authorController: FicheAuthorViewController = FicheAuthorViewController(userProfile: userProfile, showMenu: false, showTopBar: true)
            self.addChildViewController(authorController)
            authorController.didMove(toParentViewController: self)
            self.contentStackView.addArrangedSubview(authorController.view)
        } else {
            self.playlistDetailsPosition -= 1
        }
    }

    private func addQuickActionBar() {
        let quickActionBarController: ItemQuickActionBarViewController =
            ItemQuickActionBarViewController(item: self.fiche, actionBarDelegate: self.actionBarDelegate)
        self.addChildViewController(quickActionBarController)
        quickActionBarController.didMove(toParentViewController: self)
        self.contentStackView.addArrangedSubview(quickActionBarController.view)
    }

    private func addLabels() -> Bool {
        let categories: [Category] = self.fiche.categories!

        var categoryList: [String] = []
        categoryList.append(contentsOf: categories.map({ (cat) -> String in
            return cat.name
        }))
        
        if fiche.subCategories != nil {
            categoryList.append(contentsOf: fiche.subCategories!)
        }

        if !categoryList.isEmpty {
            let labelsViewController: LabelsViewController = LabelsViewController(labels: categoryList, theme: self.fiche.mainTheme, inCell: false)
            self.addChildViewController(labelsViewController)
            labelsViewController.didMove(toParentViewController: self)
            self.contentStackView.addArrangedSubview(labelsViewController.view)
            return true
        } else {
            self.playlistDetailsPosition -= 1
        }

        return false
    }
    
    private func addRelatedMedias() {
        if self.fiche.isIdsOnly() {
            if !(self.fiche.relatedFichesIds?.isEmpty)! {
                let promises: [Promise<Fiche>] = self.fiche.relatedFichesIds!.map { return FicheAPI.getFiche(with: $0) }
                all(promises)
                    .then(in: .main) { (fiches: [Fiche]) in
                        self.addRelatedMediasController(medias: self.filterRelatedMedias(fiches: fiches))
                    }
                    .catch(in: .main) { (_: Error) -> Void in
                        self.playlistDetailsPosition -= 1
                        self.showError(NSLocalizedString("Error! Impossible to load the related medias of this fiche!", comment: "Error! Impossible to load the related medias of this fiche!"))
                }
            }
        }
        else {
            addRelatedMediasController(medias: self.filterRelatedMedias(fiches: self.fiche.getRelatedFiches()!))
        }
    }

    private func filterRelatedMedias(fiches: [Fiche]) -> [Fiche] {
        return fiches.filter({ (fiche) -> Bool in
            if let media = fiche as? FicheMedia {
                return media.path == nil
            }
            else {
                return false
            }
        })
    }

    private func addRelatedMediasController(medias: [Fiche]) {
        if !medias.isEmpty {
            let relatedMediasController: RelatedMediasHorizontalViewController =
                RelatedMediasHorizontalViewController(relatedFiches: medias)
            self.addChildViewController(relatedMediasController)
            relatedMediasController.didMove(toParentViewController: self)
            self.contentStackView.insertArrangedSubview(relatedMediasController.view,
                                                        at: self.relatedMediaPosition)
        } else {
            self.playlistDetailsPosition -= 1
        }
    }
    
    private func addRelatedFiches(_ needSeparator: Bool) {
        // Check if to display related
        if self.fiche.isIdsOnly() {
            if (self.fiche.relatedFichesIds?.isEmpty)! {
                return
            }
        } else {
            if  self.fiche.getRelatedFiches() == nil || (self.fiche.getRelatedFiches()?.isEmpty)! {
                return
            }
        }

        if needSeparator {
            addSeparator()
        }

        if self.fiche.isIdsOnly() {
            let promises: [Promise<Fiche>] = self.fiche.relatedFichesIds!.map { return FicheAPI.getFiche(with: $0) }
            all(promises)
                .then(in: .main) { (fiches: [Fiche]) in
                    self.addRelatedFicheController(relatedFiches: fiches)
                }
                .catch(in: .main) { (_: Error) -> Void in
                    self.showError(NSLocalizedString("Error! Impossible to load the related fiches of this fiche!", comment: "Error! Impossible to load the related fiches of this fiche!"))
            }
        } else {
            self.addRelatedFicheController(relatedFiches: self.fiche.getRelatedFiches()!)
        }
    }

    private func addSeparator() {
        let separator: SeparatorViewController = SeparatorViewController()
        self.addChildViewController(separator)
        separator.didMove(toParentViewController: self)
        self.contentStackView.addArrangedSubview(separator.view)
        self.view.setNeedsLayout()
    }

    private func addRelatedFicheController(relatedFiches: [Fiche]) {
        let relatedFichesController: RelatedFichesHorizontalViewController =
            RelatedFichesHorizontalViewController(relatedFiches: relatedFiches, fichesDelegate: self.fichesDelegate)
        self.addChildViewController(relatedFichesController)
        relatedFichesController.didMove(toParentViewController: self)
        self.contentStackView.addArrangedSubview(relatedFichesController.view)
    }
    
    private func addPlaylistDetails(_ needSeparator: Bool) -> Bool {
        if self.playlist != nil {
            if self.playlist.isIdsOnly() {
                PlaylistAPI.getPlaylist(with: playlist.id).then(in: .main, { (playlist: Playlist) in
                    self.addPlaylistDetailsController(linkedFiches: playlist.linkedFiches!, needSeparator: needSeparator)
                })
            } else {
                self.addPlaylistDetailsController(linkedFiches: self.playlist.linkedFiches!, needSeparator: needSeparator)
            }
            return true
        }
        return false
    }

    private func addPlaylistDetailsController(linkedFiches: [LinkedFiche], needSeparator: Bool) {
        if needSeparator {
            addSeparator()
            self.playlistDetailsPosition += 1
        }
        let playlistDetailsController: SmallLinkedFicheViewController = SmallLinkedFicheViewController(linkedFiches: linkedFiches, currentFiche: self.fiche, delegate: self.fichesDelegate)
        self.addChildViewController(playlistDetailsController)
        playlistDetailsController.didMove(toParentViewController: self)
        self.contentStackView.insertArrangedSubview(playlistDetailsController.view, at: self.playlistDetailsPosition)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    private func cleanUpContent() {
        for view: UIView in self.contentStackView.arrangedSubviews {
            self.contentStackView.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
}

//
//  Theme.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class Theme: MongoEntity {
    static let HEX_COLOR: String = "color"
    static let DESCRIPTION: String = "description"
    static let NAME: String = "name"

    let hexColor: String
    let description: String
    let name: String

    required init?(json: JSON) {
        guard let hexColor: String = Theme.HEX_COLOR <~~ json,
            let description: String = Theme.DESCRIPTION <~~ json,
            let name: String = Theme.NAME <~~ json
            else {
                return nil
        }

        self.hexColor = hexColor
        self.description = description
        self.name = name

        super.init(json: json)
    }

    func toJSON() -> JSON? {

        return jsonify([
            "_id" ~~> self.id,
            "color" ~~> self.hexColor,
            "description" ~~> self.description,
            "name" ~~> self.name
            ])
    }

}

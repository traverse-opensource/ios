//
//  FilterHistoryViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 12.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class FilterHistoryViewController: UIViewController {

    @IBOutlet weak var filterDetailsLabel: UILabel!
    @IBOutlet weak var filterSavedDateLabel: UILabel!

    @IBOutlet weak var cancelButton: UIImageView!
    
    var filter: Filter?
    var index: Int?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.tag = self.index!
        
        var filterDetails: String = ""

        if !(filter?.types.isEmpty)! {
            filterDetails += NSLocalizedString("Types", comment: "") + ":"
            filter?.types.forEach({ (type) in
                filterDetails.append(" " + NSLocalizedString(type.rawValue, comment: "") + ",")
            })
            filterDetails.remove(at: filterDetails.index(before: filterDetails.endIndex))
            filterDetails += "\n"
        }

        if !(filter?.themes.isEmpty)! {
            filterDetails += NSLocalizedString("Themes", comment: "") + ":"
            filter?.themes.forEach({ (theme) in
                filterDetails.append(" " + theme.name + ",")
            })
            filterDetails.remove(at: filterDetails.index(before: filterDetails.endIndex))
            filterDetails += "\n"
        }

        if !(filter?.categories.isEmpty)! {
            filterDetails += NSLocalizedString("Categories", comment: "") + ":"
            filter?.categories.forEach({ (cat) in
                filterDetails.append(" " + cat.name + ",")
            })
            filterDetails.remove(at: filterDetails.index(before: filterDetails.endIndex))
            filterDetails += "\n"
        }

        if !(filter?.tags.isEmpty)! {
            filterDetails += NSLocalizedString("Tags", comment: "") + ":"
            filter?.tags.forEach({ (tag) in
                filterDetails.append(" " + tag.name + ",")
            })
            filterDetails.remove(at: filterDetails.index(before: filterDetails.endIndex))
            filterDetails += "\n"
        }

        filterDetailsLabel.text = filterDetails

        if self.filter?.date != nil {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateStyle = .long
            dateFormatter.timeStyle = .medium
            filterSavedDateLabel.text = dateFormatter.string(from: (filter?.date)!)
        }

        cancelButton.tag = self.index!
        cancelButton.addGestureRecognizer(UITapGestureRecognizer(target: self.parent, action: #selector(FiltersViewController.removeFilter(_:))))
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.parent, action: #selector(FiltersViewController.applyFilter(_:))))
        //        String(format: NSLocalizedString("%d theme(s)", comment: ""), (activeFilters?.themes.count)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func set(_ filter: Filter, index: Int) {
        self.filter = filter
        self.index = index
    }
}

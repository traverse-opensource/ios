//
//  UserArray.swift
//  traverse
//
//  Created by Mattia Gustarini on 03.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class UserArray: Gloss.Decodable {
    static let USERS: String = "users"

    let users: [User]

    required init?(json: JSON) {
        guard let users: [User] = UserArray.USERS <~~ json
            else {
                return nil
        }
        self.users = users
    }
}

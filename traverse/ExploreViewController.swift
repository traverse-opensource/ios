//
//  ExploreViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 01.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import GooglePlaces
import SearchTextField
import TagCellLayout

class ExploreViewController: UIViewController,
    UITableViewDelegate,
    UITableViewDataSource,
    CLLocationManagerDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    TagCellLayoutDelegate,
    ItemQuickActionBarDelegate {
    private static let FilterCell: String = "FilterCell"

    private static let BigCard: String = "BigCard"
    private static let SegueToPlaylist: String = "segueToPlaylist"
    private static let SegueToFiche: String = "segueToFiche"
    private static let SegueToFilters: String = "segueToFilters"

    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var myLocationButton: UIView!

    var fetcher: GMSAutocompleteFetcher?
    @IBOutlet weak var searchField: SearchTextField!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    @IBOutlet weak var filterCollectionViewHeightConstraint: NSLayoutConstraint!

    private let locationManager = CLLocationManager()
    private var currentLocation: CLLocationCoordinate2D?
    private var searchLocation: CLLocationCoordinate2D?

    private var currentPage: Int = 0
    private var isLoadingNextPage: Bool = false

    private var isDataLoading: Bool = false

    private var selectedItem: ItemEntity!
    
    private let addressesSearchBounds = GMSCoordinateBounds(
        coordinate: CLLocationCoordinate2D(latitude: 45.55252525134013, longitude: 5.55908203125),
        coordinate: CLLocationCoordinate2D(latitude: 47.36115300722623, longitude: 8.404541015625)
    )

    var items: [ItemEntity] = []
    var predictions: [GMSAutocompletePrediction] = []

    var activeFilters: Filter = Filter()
    var mayNeedRefresh: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.filterCollectionView.dataSource = self
        self.filterCollectionView.delegate = self


        // Create a waterfall layout
        let tagCellLayout: TagCellLayout =
            TagCellLayout(tagAlignmentType: .left, delegate: self)
        // Add the waterfall layout to your collection view
        self.filterCollectionView.collectionViewLayout = tagCellLayout

        // Set up the nib for the cell
        let viewNib: UINib = UINib(nibName: ExploreViewController.FilterCell,
                                   bundle: nil)
        self.filterCollectionView.register(viewNib,
                                          forCellWithReuseIdentifier: ExploreViewController.FilterCell)

        self.navigationController?.hidesNavigationBarHairline = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.textColor]
        self.navigationController?.navigationBar.tintColor = UIColor.textColor
        self.navigationController?.navigationBar.isTranslucent = false

        self.navigationItem.rightBarButtonItems = [
            UIBarButtonItem(image: #imageLiteral(resourceName: "ic-map"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ExploreViewController.displayMap)),
            UIBarButtonItem(image: #imageLiteral(resourceName: "ic_filter"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ExploreViewController.displayFilterView))
        ]

        self.tableView.register(
            UINib(nibName: ExploreViewController.BigCard, bundle: nil),
            forCellReuseIdentifier: ExploreViewController.BigCard)

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 444

        //Get user location
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.distanceFilter = 50
            locationManager.startUpdatingLocation()
        }
        else {
            self.searchLocation = CLLocationCoordinate2D(latitude: Const.DEFAULT_LATITUDE, longitude: Const.DEFAULT_LONGITUDE)
            self.currentPage = 0
            self.items.removeAll()
            self.updateItems()
        }

        // Set up the autocomplete filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter

        // Create the fetcher.
        fetcher = GMSAutocompleteFetcher(bounds: addressesSearchBounds, filter: filter)
        fetcher?.delegate = self

        searchField.theme.cellHeight = 60
        searchField.theme.bgColor = UIColor.white.withAlphaComponent(0.8)
        searchField.theme.font = searchField.theme.font.withSize(14)
        searchField.theme.fontColor = UIColor.black
        searchField.highlightAttributes = [NSForegroundColorAttributeName: UIColor.accentColor, NSFontAttributeName: searchField.theme.font]

        searchField?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                             for: .editingChanged)

        searchField.itemSelectionHandler = {items, itemPosition in
            self.searchField.text = items[itemPosition].title
            if itemPosition < self.predictions.count {
                let prediction = self.predictions[itemPosition]
                self.loadingView.isHidden = false
                GMSPlacesClient.shared().lookUpPlaceID(prediction.placeID!, callback: { (place, _) in
                    self.currentPage = 0
                    self.items.removeAll()
                    self.searchLocation = place?.coordinate
                    self.updateItems()
                })

            }

        }

        self.myLocationButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ExploreViewController.myLocationButtonPressed)))

    }

    func onAddClick(fiche: Fiche) {
        ItemQuickActionBarViewController.addFicheToPlaylistHelper(controller: self, ficheToAdd: fiche)
    }

    func myLocationButtonPressed() {
        self.currentPage = 0
        self.items.removeAll()
        self.searchLocation = self.currentLocation

        self.loadingView.isHidden = false
        self.updateItems()
    }

    func textFieldDidChange(textField: UITextField) {
        fetcher?.sourceTextHasChanged(textField.text!)
    }

    func updateItems() {
        if !self.isLoadingNextPage {
            self.isLoadingNextPage = true
            MixAPI.getMixins(with: Const.DEFAULT_RADIUS * 10000, latitude: (self.searchLocation?.latitude)!, longitude: (self.searchLocation?.longitude)!, page: self.currentPage + 1, limit: 10, includePlaylists: true, filter: self.activeFilters).then(in: .main, { (items: [ItemEntity]) in
                self.currentPage += 1
                self.items.append(contentsOf: items)
                self.tableView.reloadData()
                self.isLoadingNextPage = false
                self.loadingView.isHidden = true
            }).catch(in: .main) { (_: Error) in
                self.showError(NSLocalizedString("Error! Impossible to load data!", comment: "Error! Impossible to load data!"))
                self.isLoadingNextPage = false
                self.loadingView.isHidden = true
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue: CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")

        manager.stopUpdatingLocation()

        if self.currentLocation == nil {
            self.currentPage = 0
            self.items.removeAll()
            self.currentLocation = locValue
            self.searchLocation = currentLocation
            self.updateItems()
        }
    }

    func displayFilterView() {
        self.performSegue(withIdentifier: ExploreViewController.SegueToFilters, sender: self)
    }

    func displayMap() {
        self.navigationController?.popToRootViewController(animated: false)
        let mapController = MapViewController()
        mapController.activeFilters = self.activeFilters
        mapController.exploreMode = true
        mapController.exploreViewController = self
        mapController.searchLocation = self.searchLocation
        self.navigationController?.pushViewController(mapController, animated: false)

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let bigCardCell: BigCard = tableView.dequeueReusableCell(withIdentifier: ExploreViewController.BigCard, for: indexPath) as? BigCard {
            if self.items.count > indexPath.row {
                bigCardCell.set(item: self.items[indexPath.row],
                                parentController: self,
                                actionBarDelegate: self)
            }

            return bigCardCell
        }

        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if !self.items.isEmpty && indexPath.row > self.items.count - 2 && self.items.count > 9 {
            updateItems()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            if let fiche = self.items[indexPath.row] as? Fiche {
                self.selectedItem = fiche
                self.performSegue(withIdentifier: ExploreViewController.SegueToFiche, sender: self)
            }
            else if let playlist = self.items[indexPath.row] as? Playlist {
                self.selectedItem = playlist
                self.performSegue(withIdentifier: ExploreViewController.SegueToPlaylist, sender: self)
            }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if self.mayNeedRefresh {
            self.loadingView.isHidden = false
            self.updateItems()

            self.filterCollectionView.reloadData()
            self.updateCollectionViewConstraints()
        }
        else {
            self.updateCollectionViewConstraints()
        }
    }

    func updateCollectionViewConstraints() {
        if self.filterCollectionView.numberOfItems(inSection: 0) > 0 {
            let height: CGFloat = self.filterCollectionView.collectionViewLayout.collectionViewContentSize.height
            self.filterCollectionViewHeightConstraint.constant = height
        }
        else {
            self.filterCollectionViewHeightConstraint.constant = 0
        }

    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return self.activeFilters.types.count + self.activeFilters.themes.count + self.activeFilters.categories.count + self.activeFilters.tags.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        var cell: UICollectionViewCell = UICollectionViewCell()
        if let filterCell: FilterCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: ExploreViewController.FilterCell,
                                               for: indexPath) as? FilterCell {
            filterCell.set(label: Filter.getFilterLabelForIndex(self.activeFilters, index: indexPath.row), filter: Filter.getFilterForIndex(self.activeFilters, index: indexPath.row))
            cell = filterCell
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.activeFilters.removeItemAt(indexPath.row)

        self.currentPage = 0
        self.items.removeAll()

        self.loadingView.isHidden = false
        self.updateItems()

        self.filterCollectionView.reloadData()
        self.updateCollectionViewConstraints()
        self.view.layoutIfNeeded()
    }

    func tagCellLayoutTagWidth(_ layout: TagCellLayout, atIndex index: Int) -> CGFloat {
        let text = Filter.getFilterLabelForIndex(self.activeFilters, index: index)
        let width: CGFloat = TagCellLayout.textWidth(text, font: UIFont(name: "Roboto-Regular", size: 12.0)!)
        return width + 16.0 + 32.0
    }

    func tagCellLayoutTagFixHeight(_ layout: TagCellLayout) -> CGFloat {
        return CGFloat(32.0)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ExploreViewController.SegueToPlaylist {
            if let destination: DiscoveryPlaylistTableViewController =
                segue.destination as? DiscoveryPlaylistTableViewController {
                if let playlist: Playlist = self.selectedItem as? Playlist {
                    destination.set(playlist: playlist)
                }
            }
        } else if segue.identifier == ExploreViewController.SegueToFiche {
            if let destination: DiscoveryFicheViewController =
                segue.destination as? DiscoveryFicheViewController {
                if let fiche: Fiche = self.selectedItem as? Fiche {
                    destination.set(fiche: fiche)
                }
            }
        }
        else if segue.identifier == ExploreViewController.SegueToFilters {
            if let destination: FiltersViewController =
                segue.destination as? FiltersViewController {
                    destination.activeFilters = self.activeFilters
            }
        }
    }

    @IBAction func unwindFromFiltersView(segue:UIStoryboardSegue) {
        self.currentPage = 0
        self.items.removeAll()

        self.loadingView.isHidden = false
        self.updateItems()

        self.filterCollectionView.reloadData()
    }

}

extension ExploreViewController: GMSAutocompleteFetcherDelegate {
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {

        var results: [SearchTextFieldItem] = []
        self.predictions = predictions

        for prediction in predictions {
            let item = SearchTextFieldItem(title: prediction.attributedFullText.string)
            results.append(item)
        }

        searchField.filterItems(results)

        //resultText?.text = resultsStr as String
    }

    func didFailAutocompleteWithError(_ error: Error) {
        //resultText?.text = error.localizedDescription
    }
    
}

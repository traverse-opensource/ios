//
//  FicheEvent.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class FicheEvent: Fiche {
    static let START_DATE: String = "start_date"
    static let END_DATE: String = "end_date"
    static let DELTA_START: String = "delta_start"
    static let DELTA_END: String = "delta_end"

    var startDate: String?
    var endDate: String?
    var deltaStart: Int?
    var deltaEnd: Int?

    required init?(json: JSON) {

        self.startDate = FicheEvent.START_DATE <~~ json
        self.endDate = FicheEvent.END_DATE <~~ json
        self.deltaStart = FicheEvent.DELTA_START <~~ json
        self.deltaEnd = FicheEvent.DELTA_END <~~ json

        super.init(json: json)
    }

    override func toJSON() -> JSON? {

        var jsonThemes: [JSON] = []
        themes.forEach { (theme) in
            jsonThemes.append(theme.toJSON()!)
        }

        var jsonCategories: [JSON] = []
        categories?.forEach { (cat) in
            jsonCategories.append(cat.toJSON()!)
        }

        var jsonTags: [JSON] = []
        tags?.forEach { (tag) in
            jsonTags.append(tag.toJSON()!)
        }

        return jsonify([
            MongoEntity.ID ~~> self.id,
            ItemEntity.TYPE ~~> self.type,
            ItemEntity.NAME ~~> self.name,
            Gloss.Encoder.encode(dateForKey: ItemEntity.CREATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.createdAt),
            Gloss.Encoder.encode(dateForKey: ItemEntity.LAST_UPDATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.lastUpdatedAt),
            ItemEntity.STATUS ~~> self.status,
            ItemEntity.COVER ~~> self.cover?.toJSON(),
            ItemEntity.CREATED_BY ~~> self.createdBy?.toJSON(),
            ItemEntity.LAST_UPDATED_BY ~~> self.lastUpdatedBy?.toJSON(),
            ItemEntity.THEME ~~> self.mainTheme.toJSON(),
            ItemEntity.SUB_CATEGORIES ~~> self.subCategories,
            ItemEntity.LIKES ~~> self.likes,
            Fiche.REFERENCES ~~> self.references,
            Fiche.LIKES ~~> self.city,
            Fiche.LATITUDE ~~> self.latitude,
            Fiche.LONGITUDE ~~> self.longitude,
            Fiche.MONGO_LOCATION ~~> self.mongoLocation?.toJSON(),
            Fiche.GOOGLE_MAP_LOCATION ~~> self.googleMapLocation?.toJSON(),
            Fiche.SHORT_DESCRIPTION ~~> self.shortDescription,
            Fiche.LONG_DESCRIPTION ~~> self.longDescription,
            FicheEvent.START_DATE ~~> self.startDate,
            FicheEvent.END_DATE ~~> self.endDate,
            FicheEvent.DELTA_START ~~> self.deltaStart,
            FicheEvent.DELTA_END ~~> self.deltaEnd,
            [ItemEntity.CATEGORIES: jsonCategories],
            [ItemEntity.TAGS: jsonTags],
            [Fiche.THEMES: jsonThemes]
            ])
    }
}

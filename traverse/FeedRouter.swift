//
//  FeedRouter.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 28.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Alamofire

enum FeedRouter: URLRequestConvertible {
    case getFeedForFiche(id: String)
    case flagFeed(id: String)

    var method: HTTPMethod {
        switch self {
        case .getFeedForFiche:
            return .get
        case .flagFeed:
            return .put
        }
    }

    var path: String {
        switch self {
        case .getFeedForFiche(let id):
            return "/fiches/\(id)/feeds"
        case .flagFeed(let id):
            return "/feeds/\(id)/flag"
        }
    }

    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Const.API_URL.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        // if arguments are needed
        //        switch self {
        //        case .createUser(let parameters):
        //            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        //        case .updateUser(_, let parameters):
        //            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        //        default:
        //            break
        //        }

        return urlRequest
    }
}

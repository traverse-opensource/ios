//
//  MixinHorizontalTableViewCell.swift
//  traverse
//
//  Created by Mattia Gustarini on 20.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class MixinHorizontalTableViewCell: UITableViewCell {
    static let SmallCardNib: String = "SmallCard"

    @IBOutlet private weak var horizontalView: UICollectionView!

    var collectionViewOffset: CGFloat {
        get {
            return horizontalView.contentOffset.x
        }

        set {
            horizontalView.contentOffset.x = newValue
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.horizontalView.register(
            UINib(nibName: MixinHorizontalTableViewCell.SmallCardNib,
                  bundle: nil),
            forCellWithReuseIdentifier: MixinHorizontalTableViewCell.SmallCardNib)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCollectionViewDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate & UICollectionViewDelegateFlowLayout>
        (delegate: D, forRow row: Int) {

        horizontalView.delegate = delegate
        horizontalView.dataSource = delegate
        horizontalView.tag = row
        horizontalView.reloadData()
    }
}

//
//  ItemQuickActionBarDelegate.swift
//  traverse
//
//  Created by Mattia Gustarini on 06.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Foundation

protocol ItemQuickActionBarDelegate: class {
    func onAddClick(fiche: Fiche)
}

//
//  ApiRequest.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Alamofire
import Gloss
import Alamofire_Gloss
import Hydra

class ApiRequest {
    private static let API_KEY_HEADER = "x-mtsk"

    static func request(object urlRequest: URLRequestConvertible) -> Promise<Any> {
        return Promise<Any>(in: .background, { resolve, reject, _ in
            var keyUrlRequest: URLRequest!
            do {
                keyUrlRequest = try urlRequest.asURLRequest()
                keyUrlRequest.addValue(Const.API_KEY, forHTTPHeaderField: ApiRequest.API_KEY_HEADER)
            } catch {
                reject(APIError.invalidRequest())
            }

            Alamofire.request(keyUrlRequest).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success(let result):
                    resolve(result)
                case .failure(let error):
                    reject(error)
                }
            })
        })
    }

    static func request<T: Gloss.Decodable>(object urlRequest: URLRequestConvertible, type: T.Type) -> Promise<T> {
        return Promise<T>(in: .background, { resolve, reject, _ in
            var keyUrlRequest: URLRequest!
            do {
                keyUrlRequest = try urlRequest.asURLRequest()
                keyUrlRequest.addValue(Const.API_KEY, forHTTPHeaderField: ApiRequest.API_KEY_HEADER)
            } catch {
                reject(APIError.invalidRequest())
            }

            Alamofire.request(keyUrlRequest).responseObject(type) { (response: DataResponse<T>) in
                switch response.result {
                case .success(let result):
                    resolve(result)
                case .failure(let error):
                    reject(error)
                }
            }
        })
    }

    static func requestMultipartImage(object urlRequest: URLRequestConvertible, parameters: [String: String], image: UIImage) -> Promise<JSON> {

        let jpegImageData: Data = UIImageJPEGRepresentation(image, 0.5)!

        return Promise<JSON>(in: .background, { resolve, reject, _ in
            var keyUrlRequest: URLRequest!
            do {
                keyUrlRequest = try urlRequest.asURLRequest()
                keyUrlRequest.addValue(Const.API_KEY, forHTTPHeaderField: ApiRequest.API_KEY_HEADER)
            } catch {
                reject(APIError.invalidRequest())
            }

            Alamofire.upload(
                multipartFormData: { formData in
                    formData.append(jpegImageData, withName: "file", fileName: "comment-image.jpeg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        formData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                },
                with: keyUrlRequest,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            if let json: JSON = response.result.value as? JSON {
                                resolve(json)
                            }
                        }
                    case .failure(let encodingError):
                        reject(encodingError)
                    }
                }
            )
        })
    }

    static func request<T: Gloss.Decodable>(array urlRequest: URLRequestConvertible, type: T.Type) -> Promise<[T]> {
        return Promise<[T]>(in: .background, { resolve, reject, _ in
            var keyUrlRequest: URLRequest!
            do {
                keyUrlRequest = try urlRequest.asURLRequest()
                keyUrlRequest.addValue(Const.API_KEY, forHTTPHeaderField: ApiRequest.API_KEY_HEADER)
            } catch {
                reject(APIError.invalidRequest())
            }

            Alamofire.request(keyUrlRequest).responseArray(type) { (response: DataResponse<[T]>) in
                switch response.result {
                case .success(let result):
                    resolve(result)
                case .failure(let error):
                    reject(error)
                }
            }
        })
    }
}

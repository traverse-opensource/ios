//
//  MapFicheCell.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 25.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class MapFicheCell: UICollectionViewCell {

    @IBOutlet weak var ficheImage: UIImageView!

    @IBOutlet weak var ficheType: UIView!
    @IBOutlet weak var ficheIcon: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    
    @IBOutlet weak var ficheNameView: UILabel!
    @IBOutlet weak var addressView: UILabel!
    @IBOutlet weak var distanceView: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func set(_ fiche: ItemEntity, currentLocation: CLLocationCoordinate2D?) {
        let fiche = fiche as! Fiche

        if let cover: Cover = fiche.cover {
            self.ficheImage.contentMode = .center
            self.ficheImage.hnk_setImageFromURL(cover.getImageUrl(), placeholder: UIView.getFicheTypeImage(fiche: fiche), format: nil, failure: nil, success: {(image) in
                self.ficheImage.image = image
                self.ficheImage.contentMode = .scaleAspectFill
            })
        }

        UIView.setFicheTypeImage(ficheIcon, fiche: fiche)
        UIView.setTheme(fiche: fiche, iconView: ficheType, separatorView: separatorView)

        ficheNameView.text = fiche.name
        //addressView.text = fiche.googleMapLocation?.fullAddrStr
        addressView.text = fiche.shortDescription

        if currentLocation != nil {
            let location = CLLocation(latitude: currentLocation!.latitude, longitude: currentLocation!.longitude)
            let ficheLocation = CLLocation(latitude: (fiche.latitude)!, longitude: (fiche.longitude)!)
            let distance = location.distance(from: ficheLocation)

            distanceView.text = String(format: NSLocalizedString("at %.01fkm", comment: "at %.01fkm"), distance / 1000)
        }
        else {
            distanceView.text = ""
        }
    }
}

//
//  RelatedFichesHorizontalViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 09.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Hydra

private let RelatedFichesHorizontalViewControllerNib: String = "RelatedFichesHorizontalViewController"

class RelatedFichesHorizontalViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {
    private static let SmallCardNib: String = "SmallCard"
    private static let HorizontalSectionInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)

    @IBOutlet weak var relatedFichesCollectionView: UICollectionView!

    private var relatedFiches: [Fiche] = []
    private weak var fichesDelegate: FicheClickDelegate!

    init(relatedFiches: [Fiche], fichesDelegate: FicheClickDelegate) {
        self.relatedFiches = relatedFiches
        self.fichesDelegate = fichesDelegate
        super.init(nibName: RelatedFichesHorizontalViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.relatedFichesCollectionView.dataSource = self
        self.relatedFichesCollectionView.delegate = self

        self.relatedFichesCollectionView.register(
            UINib(nibName: RelatedFichesHorizontalViewController.SmallCardNib,
                  bundle: nil),
            forCellWithReuseIdentifier: RelatedFichesHorizontalViewController.SmallCardNib)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return relatedFiches.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell = UICollectionViewCell()
        if let smallCard: SmallCard = collectionView.dequeueReusableCell(
            withReuseIdentifier: RelatedFichesHorizontalViewController.SmallCardNib,
            for: indexPath as IndexPath) as? SmallCard {
            let item: ItemEntity = relatedFiches[indexPath.row]

            smallCard.set(item: item)
            cell = smallCard
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return RelatedFichesHorizontalViewController.HorizontalSectionInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let availableWidth: CGFloat = collectionView.frame.width - RelatedFichesHorizontalViewController.HorizontalSectionInsets.left

        //let width: GLfloat = GLfloat((availableWidth / 2.0) + (availableWidth / 4.0))
        let width: GLfloat = GLfloat((availableWidth / 1.8))

        return CGSize(width: Int(width), height: Int(collectionView.frame.height))
    }

    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        if self.fichesDelegate != nil {
            let fiche: Fiche = self.relatedFiches[indexPath.row]
            self.fichesDelegate.onClick(fiche: fiche)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

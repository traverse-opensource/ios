//
//  ItemType.swift
//  traverse
//
//  Created by Mattia Gustarini on 27.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

enum ItemType: String {
    case places = "Places"
    case people = "People"
    case media = "Media"
    case objects = "Objects"
    case events = "Events"
    case playlist = "playlist"
}

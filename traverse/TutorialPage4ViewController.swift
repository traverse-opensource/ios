//
//  TutorialPage4ViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 20.11.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class TutorialPage4ViewController: UIViewController {

    @IBOutlet weak var titleImage: UIImageView!

    @IBOutlet weak var contentImage1: UIImageView!

    @IBOutlet weak var contentImage2: UIImageView!

    @IBOutlet weak var contentImage3: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(red: 63/255, green: 188/255, blue: 167/255, alpha: 1)
        titleImage.image = titleImage.image?.withRenderingMode(.alwaysTemplate).tint(with: UIColor.white)

        contentImage1.image = contentImage1.image?.withRenderingMode(.alwaysTemplate).tint(with: UIColor.white)
        contentImage2.image = contentImage2.image?.withRenderingMode(.alwaysTemplate).tint(with: UIColor.white)
        contentImage3.image = contentImage3.image?.withRenderingMode(.alwaysTemplate).tint(with: UIColor.white)
    }
}


//
//  ItemQuickActionBar.swift
//  traverse
//
//  Created by Mattia Gustarini on 26.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Haneke

private let ItemQuickActionBarViewControllerNib: String = "ItemQuickActionBarViewController"

class ItemQuickActionBarViewController: UIViewController {

    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var likeImage: UIImageView!

    @IBOutlet weak var likeClickableView: UIView!
    @IBOutlet weak var shareClickableView: UIView!
    @IBOutlet weak var favoriteClickableView: UIView!

    @IBOutlet weak var favoriteIcon: UIImageView!
    @IBOutlet weak var favoriteLabel: UILabel!

    private var item: ItemEntity
    private weak var actionBarDelegate: ItemQuickActionBarDelegate!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    init(item: ItemEntity, actionBarDelegate: ItemQuickActionBarDelegate) {
        self.item = item
        self.actionBarDelegate = actionBarDelegate
        super.init(nibName: ItemQuickActionBarViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.likeClickableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ItemQuickActionBarViewController.likeItem)))
        self.shareClickableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ItemQuickActionBarViewController.shareItem)))
        self.favoriteClickableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ItemQuickActionBarViewController.addOrfavoriteItem)))

        updateView()
    }

    func shareItem() {

        var link: NSURL?

        if item is Fiche {
            link = NSURL(string: Const.SHARE_FICHE_URL + (item.id))
        }
        else if item is Playlist {
            link = NSURL(string: Const.SHARE_PLAYLIST_URL + (item.id))
        }
        else {
            return
        }

        let objectsToShare = [link]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)

    }

    func likeItem() {
        if let user = UserAPI.getCurrentUser() {

            if let fiche = self.item as? Fiche {
                if fiche.likes.contains(user.id) {
                    FicheAPI.dislikeFiche(id: fiche.id, userId: user.id).then({ (fiche) in
                        self.item = fiche
                        self.updateLikeAndCount()
                    })
                }
                else {
                    FicheAPI.likeFiche(id: fiche.id, userId: user.id).then({ (fiche) in
                        self.item = fiche
                        self.updateLikeAndCount()
                    })
                }
            }
            else if let playlist = self.item as? Playlist {
                if playlist.likes.contains(user.id) {
                    PlaylistAPI.dislikePlaylist(id: playlist.id, userId: user.id).then({ (playlist) in
                        self.item = playlist
                        self.updateLikeAndCount()
                    })
                }
                else {
                    PlaylistAPI.likePlaylist(id: playlist.id, userId: user.id).then({ (playlist) in
                        self.item = playlist
                        self.updateLikeAndCount()
                    })
                }
            }

        }
    }

    func addOrfavoriteItem() {
        if self.item is Fiche {
            addFicheToPlaylist()
        }
        else if self.item is Playlist {
            toggleFavorite()
        }
    }

    func toggleFavorite() {

            var ficheIndex = -1

            var favArray = UserDefaults.standard.array(forKey: Const.FAVORITES_PLAYLIST_KEY)
            var favDataArray = UserDefaults.standard.array(forKey: Const.FAVORITES_PLAYLIST_DATA)

            if favArray != nil {
                var index = 0
                for key in favArray as! [String] {
                    if key == (self.item.id) {
                        ficheIndex = index
                        break
                    }
                    index += 1
                }
            }

            if ficheIndex > -1 {
                favArray?.remove(at: ficheIndex)
                favDataArray?.remove(at: ficheIndex)

                UserDefaults.standard.set(favArray, forKey: Const.FAVORITES_PLAYLIST_KEY)
                UserDefaults.standard.set(favDataArray, forKey: Const.FAVORITES_PLAYLIST_DATA)

                self.updateFavoriteView()

            }
            else {
                if favArray == nil {
                    favArray = []
                    favDataArray = []
                }

                PlaylistAPI.getPlaylist(with: (self.item.id)).then { (playlist) in

                    //Load covers (for caching images)
                    if playlist.cover != nil {
                        let cache = Shared.imageCache
                        let fetcherThumb = NetworkFetcher<UIImage>(URL: (playlist.cover?.getThumbImageUrl())!)
                        let fetcherImg = NetworkFetcher<UIImage>(URL: (playlist.cover?.getImageUrl())!)
                        cache.fetch(fetcher: fetcherThumb)
                        cache.fetch(fetcher: fetcherImg)
                    }
                    
                    favArray?.append((playlist.id))
                    let playlist = playlist
                    favDataArray?.append((playlist.toJSON())!)

                    UserDefaults.standard.set(favArray, forKey: Const.FAVORITES_PLAYLIST_KEY)
                    UserDefaults.standard.set(favDataArray, forKey: Const.FAVORITES_PLAYLIST_DATA)

                    self.updateFavoriteView()

                }.catch { (_) -> Void in
                    self.showError(NSLocalizedString("Error! Impossible to load data!", comment: "Error! Impossible to load data!"))
                }

            }
    }

    func updateFavoriteView() {

        favoriteIcon.isHidden = false
        favoriteLabel.isHidden = false
        var isFav = false

        let favArray = UserDefaults.standard.array(forKey: Const.FAVORITES_PLAYLIST_KEY)
        if favArray != nil {
            isFav = favArray!.contains(where: { (key) -> Bool in
                return key as? String == (self.item.id)
            })
        }

        if isFav {
            favoriteIcon.image = #imageLiteral(resourceName: "ic_favorite")
        }
        else {
            favoriteIcon.image = #imageLiteral(resourceName: "ic_favorite_empty")
        }
    }

    func addFicheToPlaylist() {
        if self.actionBarDelegate != nil {
            if let fiche: Fiche = self.item as? Fiche {
                self.actionBarDelegate.onAddClick(fiche: fiche)
            }
        }
    }

//    func set(item: ItemEntity, actionBarDelegate: ItemQuickActionBarDelegate) {
//        self.item = item
//        self.actionBarDelegate = actionBarDelegate
//        updateView()
//    }

    func updateLikeAndCount() {
        likeCount.text = "\((self.item.likes.count))"

        if let user = UserAPI.getCurrentUser() {
            if self.item.likes.contains(user.id) {
                likeImage.image = #imageLiteral(resourceName: "ic_liked")
            }
        }
    }

    func updateView() {

        updateLikeAndCount()

        if self.item is Fiche {
            if UserAPI.getCurrentUser() != nil {
                favoriteIcon.image = #imageLiteral(resourceName: "ic_playlists")
                favoriteLabel.text = NSLocalizedString("Add", comment: "Add")
            }
            else {
                favoriteClickableView.isHidden = true
                favoriteIcon.isHidden = true
                favoriteLabel.isHidden = true
            }
        }
        else {
            updateFavoriteView()
        }
    }

    static func addFicheToPlaylistHelper(controller: UIViewController, ficheToAdd: Fiche) {
        if let userId: String = UserAPI.getCurrentUser()?.id {
            UserAPI.getMyPlaylists(userId: userId).then(in: .main) { (playlists: [Playlist]) in
                let addPlaylistController: AddPlaylistViewController = AddPlaylistViewController()
                addPlaylistController.set(playlists: playlists, ficheToAdd: ficheToAdd)
                controller.present(addPlaylistController, animated: true)
                }.catch(in: .main) { (_: Error) in
                    controller.showError(NSLocalizedString("Error! Impossible to load data!", comment: "Error! Impossible to load data!"))
            }
        }
    }
}

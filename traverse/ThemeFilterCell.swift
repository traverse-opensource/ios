//
//  ThemeFilterCell.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 11.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class ThemeFilterCell: UITableViewCell {

    @IBOutlet weak var themeLabel: UILabel!
    @IBOutlet weak var toggleImage: UIImageView!
    @IBOutlet weak var emptySelectView: UIView!
    @IBOutlet weak var coloredView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        toggleImage.isHidden = !selected
        emptySelectView.isHidden = selected
    }

    func set(_ theme: Theme) {
        themeLabel.text = theme.name
        coloredView.backgroundColor = UIColor(hexString: theme.hexColor)
    }
    
}

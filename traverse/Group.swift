//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class Group: MongoEntity {
    static let NAME: String = "name"
    static let DESCRIPTION: String = "description"

    var name: String?
    var description: String?

    required init?(json: JSON) {
//        guard let name: String = Group.NAME <~~ json,
//            let description: String = Group.DESCRIPTION <~~ json
//            else {
//                return nil
//        }

        self.name = Group.NAME <~~ json
        self.description = Group.DESCRIPTION <~~ json

        super.init(json: json)
    }

    func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "name" ~~> self.name,
            "description" ~~> self.description
        ])
    }
}

//
//  PlaylistArray.swift
//  traverse
//
//  Created by Mattia Gustarini on 08.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class PlaylistArray: Gloss.Decodable {
    static let PLAYLIST: String = "playlists"

    let playlists: [Playlist]

    required init?(json: JSON) {
        guard let playlists: [Playlist] = PlaylistArray.PLAYLIST <~~ json
            else {
                return nil
        }
        self.playlists = playlists
    }
}

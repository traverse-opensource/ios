//
//  WebViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 19.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!

    var url: URL?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "traverse"
        self.webView.loadRequest(URLRequest(url: url!))

    }

    override func viewDidAppear(_ animated: Bool) {
        UIButton.appearance().backgroundColor = UIColor.black
        super.viewDidAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        UIButton.appearance().backgroundColor = UIColor.primaryColor
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  AppDelegate.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import FontBlaster
import ChameleonFramework
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import GoogleSignIn
import FBSDKLoginKit
import TwitterKit
import Material
import Firebase
//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var universalLinkUserActivity: NSUserActivity?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        FirebaseApp.configure()

        Chameleon.setGlobalThemeUsingPrimaryColor(UIColor.primaryColor, with: .contrast)

        if let universalLinkUserDic = launchOptions?[UIApplicationLaunchOptionsKey.userActivityDictionary] as? [String: Any],
            let universalLinkUserAct  = universalLinkUserDic["UIApplicationLaunchOptionsUserActivityKey"] as? NSUserActivity {

            universalLinkUserActivity = universalLinkUserAct
            NSLog("OPEN UNIVERSAL LINK type \(universalLinkUserDic.self),\(universalLinkUserDic)") // using NSLog for logging as print did not log to my 'Device and Simulator' logs
        }
        else {
            universalLinkUserActivity = nil
        }
        
        //Google Maps API
        GMSPlacesClient.provideAPIKey(Const.GOOGLE_PLACES_API_KEY)
        GMSServices.provideAPIKey(Const.GOOGLE_MAPS_API_KEY)

        // Loading fonts
        FontBlaster.blast()

        // Set up theme

        
        IQKeyboardManager.sharedManager().enable = true

        GIDSignIn.sharedInstance().clientID = "-...com"
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")

        Twitter.sharedInstance().start(withConsumerKey: "", consumerSecret: "")

        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//    }

    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any])
        -> Bool {

            if FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation]) {
                return true
            }

            if GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation]) {
                return true
            }

            if Twitter.sharedInstance().application(application, open: url, options: options) {
                return true
            }

            return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. 
        // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
        // or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, 
        // and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {

        return handleUserActivityForUniversalLink(userActivity)
    }

    func handleUserActivityForUniversalLink(_ userActivity: NSUserActivity) -> Bool {
        var parameters = userActivity.webpageURL?.absoluteString.replacingOccurrences(of: Const.BASE_URL, with: "").split(separator: "/")
        if parameters?.count == 3 {

            let type = parameters![1]
            let itemId = parameters![2]

            switch type {
            case "fiche":
                FicheAPI.getFiche(with: String(itemId)).then({ (fiche) in
                    UserDefaults.standard.set(true, forKey: fiche.id)
                    guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "DiscoveryFicheDetails") as? DiscoveryFicheViewController else {
                        print("Could not instantiate view controller with identifier of type DiscoveryFicheViewController")
                        return
                    }

                    vc.set(fiche: fiche)

                    if let applicationDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
                        if let window: UIWindow = applicationDelegate.window {
                            //window.rootViewController?.present(vc, animated: true, completion: nil)
                            if let snackController = window.rootViewController as? SnackbarController {
                                if let tabController = snackController.rootViewController as? RootViewController {
                                    if let navController = tabController.selectedViewController as? UINavigationController {
                                        navController.pushViewController(vc, animated:true)
                                    }
                                }
                            }
                        }
                    }

                }).catch({ (error) -> (Void) in
                    print(error)
                })
                break
            case "playlist":
                PlaylistAPI.getPlaylist(with: String(itemId)).then({ (playlist) in

                    guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "DiscoveryPlaylistTable") as? DiscoveryPlaylistTableViewController else {
                        print("Could not instantiate view controller with identifier of type DiscoveryPlaylistTableViewController")
                        return
                    }

                    vc.updatePlaylist(playlist)

                    if let applicationDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
                        if let window: UIWindow = applicationDelegate.window {
                            //window.rootViewController?.present(vc, animated: true, completion: nil)
                            if let snackController = window.rootViewController as? SnackbarController {
                                if let tabController = snackController.rootViewController as? RootViewController {
                                    if let navController = tabController.selectedViewController as? UINavigationController {
                                        navController.pushViewController(vc, animated:true)
                                    }
                                }
                            }
                        }
                    }

                }).catch({ (error) -> (Void) in
                    print(error)
                })
                break
            default:
                return false
            }
            return true
        }

        return false
    }


    static func resetApplication() {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        if let applicationDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let window: UIWindow = applicationDelegate.window {
                for view in window.subviews {
                    view.removeFromSuperview()
                }

                let startViewController = storyboard.instantiateInitialViewController()
                window.rootViewController = startViewController
            }
        }


    }
}

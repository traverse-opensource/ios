//
//  Const.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

class Const {
//    static let BASE_URL = "http://192.168.0.151:3001/"
//    static let BASE_URL: String = "http://mobilethinking.ch:3001/"
    static let BASE_URL: String = "https://traverse-patrimoines.com/"
    static let API_URL: String = "\(Const.BASE_URL)api/v1.2"
    static let API_KEY: String = "="
    static let SHARE_FICHE_URL: String = "\(Const.BASE_URL)public/fiche/"
    static let SHARE_PLAYLIST_URL: String = "\(Const.BASE_URL)public/playlist/"

    static let MY_PLAYLISTS_DATA: String = "MY_PLAYLISTS_DATA"
    static let FAVORITES_PLAYLIST_KEY: String = "FAVORITES_PLAYLIST_KEY"
    static let FAVORITES_PLAYLIST_DATA: String = "FAVORITES_PLAYLIST_DATA"
    static let FILTER_HISTORY: String = "FILTER_HISTORY"

    static let DEFAULT_LATITUDE: Double = 46.2050242
    static let DEFAULT_LONGITUDE: Double = 6.1090692
    static let DEFAULT_RADIUS: Int = 30000

    static let GOOGLE_MAPS_API_KEY: String = ""
    static let GOOGLE_PLACES_API_KEY: String = ""

    static let TRAVERSE_AUTH_USER_KEY = "TRAVERSE_AUTH_USER_KEY"
    
    static let IS_USER_LOGGED_IN_KEY: String = "IS_USER_LOGGED_IN_KEY"
    static let LOGIN_METHOD: String = "LOGIN_METHOD"

    static let GOOGLE_AUTH: String = "google"
    static let FACEBOOK_AUTH: String = "facebook"
    static let TWITTER_AUTH: String = "twitter"
    static let CUSTOM_AUTH: String = "traverse"

    static let GOOGLE_USER_CLIENT_ID_KEY: String = "GOOGLE_USER_CLIENT_ID_KEY"
    static let GOOGLE_USER_TOKEN_KEY: String = "GOOGLE_USER_TOKEN_KEY"
    static let GOOGLE_USER_EMAIL_KEY: String = "GOOGLE_USER_EMAIL_KEY"
    static let GOOGLE_USER_NAME_KEY: String = "GOOGLE_USER_NAME_KEY"

    static let FACEBOOK_USER_ID_KEY: String = "FACEBOOK_USER_ID_KEY"
    static let FACEBOOK_USER_EMAIL_KEY: String = "FACEBOOK_USER_EMAIL_KEY"
    static let FACEBOOK_USER_NAME_KEY: String = "FACEBOOK_USER_NAME_KEY"

    static let TWITTER_USER_ID_KEY: String = "TWITTER_USER_ID_KEY"
    static let TWITTER_USER_EMAIL_KEY: String = "TWITTER_USER_EMAIL_KEY"
    static let TWITTER_USER_NAME_KEY: String = "TWITTER_USER_NAME_KEY"

    static let TRAVERSE_USER_ID_KEY: String = "TRAVERSE_USER_ID_KEY"
    static let TRAVERSE_USER_EMAIL_KEY: String = "TRAVERSE_USER_EMAIL_KEY"
    static let TRAVERSE_USER_NAME_KEY: String = "TRAVERSE_USER_NAME_KEY"
}

//
//  APIError.swift
//  traverse
//
//  Created by Mattia Gustarini on 18.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

enum APIError: Error {
    case decode(what: String)
    case invalidRequest()

    var error: String {
        switch self {
        case .decode(let what):
            return "Can't decode \(what)"
        case .invalidRequest():
            return "Invalid request!"
        }
    }
}

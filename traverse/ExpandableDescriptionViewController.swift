//
//  ExpandableDescriptionViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import ReadMoreTextView

private let ExpandableDescriptionViewControllerNib: String = "ExpandableDescriptionViewController"

class ExpandableDescriptionViewController: UIViewController {
    @IBOutlet weak var descriptionTextView: ReadMoreTextView!
    private let longDescription: String

    init(_ longDescription: String) {
        self.longDescription = longDescription
        super.init(nibName: ExpandableDescriptionViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.descriptionLabel.numberOfLines = 5

        descriptionTextView.shouldTrim = true
        descriptionTextView.maximumNumberOfLines = 9

        descriptionTextView.textContainerInset.bottom = 24

        //descriptionTextView.attributedReadMoreText = NSAttributedString(string: )

        descriptionTextView.attributedReadMoreText = NSAttributedString(string: NSLocalizedString("... More", comment: ""), attributes: [
            NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 14.0)!,
            NSForegroundColorAttributeName: UIColor.accentColor
        ])

        descriptionTextView.attributedReadLessText = NSAttributedString(string: NSLocalizedString("... Less", comment: ""), attributes: [
            NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 14.0)!,
            NSForegroundColorAttributeName: UIColor.accentColor
        ])

        parseHTML()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func parseHTML() {
        do {
            let attrStr = try NSMutableAttributedString(
                data: self.longDescription.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                documentAttributes: nil)
            var attributes: [String: Any] = attrStr.attributes(at: 0, effectiveRange: nil)
            attributes[NSFontAttributeName] = UIFont(name: "Roboto-Regular", size: 14.0)
            attributes[NSForegroundColorAttributeName] = UIColor.textColor
            attrStr.setAttributes(attributes, range: NSRange(location: 0, length: attrStr.length))

            self.descriptionTextView.attributedText = attrStr
        } catch {
            self.descriptionTextView.text = self.longDescription
        }

        self.descriptionTextView.setNeedsLayout()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

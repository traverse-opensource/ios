//
//  LabelCell.swift
//  traverse
//
//  Created by Mattia Gustarini on 27.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import ChameleonFramework

class LabelCell: UICollectionViewCell {

    @IBOutlet weak var labelLabel: UILabel!

    private var label: String!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(label: String, theme: ItemTheme) {
        self.label = label
        self.labelLabel.text = label
        if let themeColor: UIColor = theme.getThemeColor() {
            self.labelLabel.backgroundColor = themeColor
        }
        self.contentView.setNeedsLayout()
    }

    func set(label: String) {
        self.label = label
        self.labelLabel.text = label
        deselect()
    }

    func getLabel() -> String {
        return self.label
    }

    func select() {
        self.labelLabel.textColor = UIColor.white
        self.labelLabel.backgroundColor = UIColor.accentColor
        self.labelLabel.borderColor = UIColor.accentColor
        self.labelLabel.borderWidth = 2
    }

    func deselect() {
        self.labelLabel.textColor = UIColor.accentColor
        self.labelLabel.backgroundColor = UIColor.white
        self.labelLabel.borderColor = UIColor.accentColor
        self.labelLabel.borderWidth = 2
    }

}

//
//  GalleryGridViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 18.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

private let GalleryGridViewControllerNib: String = "GalleryGridViewController"

class GalleryGridViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {

    private static let HorizontalSectionInsets: UIEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)

    @IBOutlet weak var gridView: UICollectionView!

    private let medias: [FicheMedia]
    private let pager: GalleryPager

    private static let MediaCellNib: String = "MediaCell"

    init(medias: [FicheMedia], pager: GalleryPager) {
        self.medias = medias
        self.pager = pager
        super.init(nibName: GalleryGridViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let imageView = UIImageView(image: #imageLiteral(resourceName: "ic_diapo"))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        imageView.frame = titleView.bounds
        titleView.addSubview(imageView)
        titleView.isUserInteractionEnabled = true
        self.navigationItem.titleView = titleView

        let closeGalleryGrid: UITapGestureRecognizer = UITapGestureRecognizer()
        closeGalleryGrid.addTarget(self, action: #selector(GalleryGridViewController.closeGalleryGrid))
        titleView.addGestureRecognizer(closeGalleryGrid)

        self.view.backgroundColor = UIColor.backgroundDefaultColor

        self.gridView.dataSource = self
        self.gridView.delegate = self

        self.gridView.register(
            UINib(nibName: GalleryGridViewController.MediaCellNib,
                  bundle: nil),
            forCellWithReuseIdentifier: GalleryGridViewController.MediaCellNib)

        self.navigationItem.backBarButtonItem?.title = ""

        self.gridView.reloadData()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return medias.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        var cell: UICollectionViewCell = UICollectionViewCell()

        if let mediaCell: MediaCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: GalleryGridViewController.MediaCellNib,
            for: indexPath as IndexPath) as? MediaCell {

            let item: ItemEntity = medias[indexPath.row]
            mediaCell.set(item: item)
            cell = mediaCell
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return GalleryGridViewController.HorizontalSectionInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width: GLfloat = GLfloat(collectionView.frame.width / 3) - 16

        return CGSize(width: Int(width), height: Int(width))
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.pager.gotoPage(index: indexPath.row)
        self.navigationController?.popViewController(animated: false)
    }

    func closeGalleryGrid() {
        self.navigationController?.popViewController(animated: false)
    }
}

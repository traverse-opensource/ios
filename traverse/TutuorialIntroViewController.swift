//
//  TutuorialIntroViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 20.11.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class TutuorialIntroViewController : UIViewController {

    var parentController: UIViewController?
    @IBOutlet weak var skipButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(red: 63/255, green: 188/255, blue: 167/255, alpha: 1)
        skipButton.backgroundColor = UIColor(red: 63/255, green: 188/255, blue: 167/255, alpha: 1)
    }

    @IBAction func onSkip(_ sender: Any) {

        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickStartTutorial(_ sender: Any) {


        self.dismiss(animated: false, completion: {
            self.parentController?.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TutorialPager"), animated: true, completion: nil)
        })


    }
}

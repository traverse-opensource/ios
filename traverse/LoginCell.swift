//
//  LoginCell.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 28.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class LoginCell: UITableViewCell {

    var delegate: UIViewController?

    @IBOutlet weak var loginButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()

        loginButton.backgroundColor = UIColor.accentColor
    }

    @IBAction func onLoginClicked(_ sender: Any) {
        let loginview = LoginViewController()
        loginview.parentNavigationController = delegate?.navigationController
        delegate?.present(loginview, animated: true, completion: nil)
    }
}

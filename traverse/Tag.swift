//
//  Tag.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class Tag: MongoEntity {
    static let NAME: String = "name"
    static let COUNT: String = "count"

    let name: String
    let count: Int

    required init?(json: JSON) {
        guard let name: String = Tag.NAME <~~ json,
            let count: Int = Tag.COUNT <~~ json
            else {
                return nil
        }

        self.name = name
        self.count = count

        super.init(json: json)
    }


    func toJSON() -> JSON? {

        return jsonify([
            "_id" ~~> self.id,
            "count" ~~> self.count,
            "name" ~~> self.name
            ])
    }
}

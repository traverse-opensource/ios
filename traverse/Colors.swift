//
//  Colors.swift
//  traverse
//
//  Created by Mattia Gustarini on 20.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//
import UIKit
import ChameleonFramework

extension UIColor {
    static var accentColor: UIColor {
        return UIColor(hexString: "#386f8d")!
    }

    static var primaryColor: UIColor {
        return UIColor(hexString: "#FFFFFF")!
    }

    static var primaryDarkColor: UIColor {
        return UIColor(hexString: "#E2E2E2")!
    }

    static var textColor: UIColor {
        return UIColor(hexString: "#838282")!
    }

    static var shadowColor: UIColor {
        return UIColor(hexString: "#E2E2E2")!
    }

    static var separatorColor: UIColor {
        return UIColor(hexString: "#E2E2E2")!
    }

    static var backgroundDefaultColor: UIColor {
        return UIColor(hexString: "#F8F8F8")!
    }

    static var twitterColor: UIColor {
        return UIColor(hexString: "#1dcaff")!
    }

    static var facebookColor: UIColor {
        return UIColor(hexString: "#3b5998")!
    }

    static var googleColor: UIColor {
        return UIColor(hexString: "#d34836")!
    }
}

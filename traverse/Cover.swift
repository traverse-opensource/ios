//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class Cover: Gloss.Decodable {
//    static let FIELD_NAME: String = "fieldname"
//    static let ORIGINAL_NAME: String = "originalname"
//    static let ENCODING: String = "encoding"
//    static let MIME_TYPE: String = "mimetype"
//    static let DESTINATION: String = "destination"
//    static let FILE_NAME: String = "filename"
    static let PATH: String = "path"
        static let BIG: String = "big"
        static let THUMB: String = "thumb"
//    static let SIZE: String = "size"
    static let COVER_CREDIT: String = "cover_credit"

//    private let fieldName: String!
//    private let originalName: String!
//    private let encoding: String!
//    private let mimeType: String!
//    private let fileName: String!
    var path: String?
    private var big: String?
    private var thumb: String?
//    private let size: Int!
    let credit: String?
//    private let destination: String!

    required init?(json: JSON) {
//        guard let fieldName: String = Cover.FIELD_NAME <~~ json,
//          let originalName: String = Cover.ORIGINAL_NAME <~~ json,
//          let encoding: String = Cover.ENCODING <~~ json,
//          let mimeType: String = Cover.MIME_TYPE <~~ json,
//            let fileName: String = Cover.FILE_NAME <~~ json,
//            guard let path: String = Cover.PATH <~~ json
//            let destination: String = Cover.DESTINATION <~~ json,
//            let size: Int = Cover.SIZE <~~ json
//            let coverCredit: String = Cover.COVER_CREDIT <~~ json
//            else {
//                return nil
//        }

//        self.fieldName = Cover.FIELD_NAME <~~ json
//        self.originalName = Cover.ORIGINAL_NAME <~~ json
//        self.encoding = Cover.ENCODING <~~ json
//        self.mimeType = Cover.MIME_TYPE <~~ json
//        self.fileName = Cover.FILE_NAME <~~ json
        self.path = Cover.PATH <~~ json
        self.big = Cover.BIG <~~ json
        self.thumb = Cover.THUMB <~~ json
//        self.destination = Cover.DESTINATION <~~ json
//        self.size = Cover.SIZE <~~ json
        self.credit = Cover.COVER_CREDIT <~~ json
    }

    func toJSON() -> JSON? {

        return jsonify([
            Cover.PATH ~~> self.path,
            Cover.BIG ~~> self.big,
            Cover.THUMB ~~> self.thumb,
            Cover.COVER_CREDIT ~~> self.credit
            ])
    }

    func getBigImageUrl() -> URL {
        var url: String = ""
        if let path: String = self.big {
            if path.hasPrefix("http") || path.hasPrefix("https") {
                url = path
            } else {
                url = Const.BASE_URL + path
            }
        } else {
            url = Const.BASE_URL + "images/defaultImage_Site.png"
        }

        let finalUrl = URL(string: url)
        if finalUrl != nil {
            return finalUrl!
        }
        else {
            return URL(string: Const.BASE_URL + "images/defaultImage_Site.png")!
        }
    }

    func getThumbImageUrl() -> URL {
        var url: String = ""
        if let path: String = self.thumb {
            if path.hasPrefix("http") || path.hasPrefix("https") {
                url = path
            } else {
                url = Const.BASE_URL + path
            }
        } else {
            url = Const.BASE_URL + "images/defaultImage_Site.png"
        }

        let finalUrl = URL(string: url)
        if finalUrl != nil {
            return finalUrl!
        }
        else {
            return URL(string: Const.BASE_URL + "images/defaultImage_Site.png")!
        }
    }

    func getImageUrl() -> URL {
        var url: String = ""
        if let path: String = self.path {
            if path.hasPrefix("http") || path.hasPrefix("https") {
                url = path
            } else {
                url = Const.BASE_URL + path
            }
        } else {
            url = Const.BASE_URL + "images/defaultImage_Site.png"
        }

        let finalUrl = URL(string: url)
        if finalUrl != nil {
            return finalUrl!
        }
        else {
            return URL(string: Const.BASE_URL + "images/defaultImage_Site.png")!
        }
    }
}

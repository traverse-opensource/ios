//
//  ThemeFilterViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 06.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class ThemeFilterViewController: FilterViewController, FilterViewTableDelegate {

    private static let ThemeFilterNib: String = "ThemeFilterCell"
    var filterData: [Theme] = []

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: ThemeFilterViewController.ThemeFilterNib,
                                      bundle: nil), forCellReuseIdentifier: ThemeFilterViewController.ThemeFilterNib)

        ThemeAPI.getThemes().then { (themes) in
            self.filterData = themes
            self.tableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }

    func getFilterCount() -> Int {
        return filterData.count
    }

    func getFilterViewAt(_ index: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ThemeFilterViewController.ThemeFilterNib, for: IndexPath(row: index, section: 0)) as! ThemeFilterCell
        cell.set(filterData[index])

        if (self.filter?.themes.contains(where: { (theme) -> Bool in
            return theme.id == filterData[index].id
        }))! {
            tableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.none)
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.filter?.themes.append(filterData[indexPath.row])
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.filter?.themes = (self.filter?.themes.filter({ (theme) -> Bool in
            return theme.id != filterData[indexPath.row].id
        }))!
    }
}

//
//  MixRouter.swift
//  traverse
//
//  Created by Mattia Gustarini on 18.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Alamofire

enum MixRouter: URLRequestConvertible {
    case getMixins()
    case getMixinsWithParams(maxDistance: Int,
        latitude: Double,
        longitude: Double,
        page: Int,
        limit: Int,
        includePlaylists: Bool,
        filter: Filter?)
    case getMixinsWithoutPlaylists()
    case getHeartstrokes()

    var method: HTTPMethod {
        switch self {
        case .getMixins:
            return .get
        case .getMixinsWithParams:
            return .get
        case .getMixinsWithoutPlaylists:
            return .get
        case .getHeartstrokes:
            return .get
        }
    }

    var path: String {
        switch self {
        case .getMixins:
            return "/mixins"
        case .getMixinsWithParams:
            return "/mixins"
        case .getMixinsWithoutPlaylists:
            return "/mixins"
        case .getHeartstrokes:
            return "/heartstrokes"
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Const.API_URL.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        // if arguments are needed
        switch self {
        case .getMixinsWithParams(let maxDistance,
                                       let latitude,
                                       let longitude,
                                       let page,
                                       let limit,
                                       let includePlaylists,
                                       let filter):
            var parameters: Parameters = ["maxDistance": maxDistance,
                          "lat": latitude,
                          "lng": longitude,
                          "page": page,
                          "limit": limit,
                          "includesPlaylist": includePlaylists ? 1: 0
            ]

            var tags: [String] = []
            var themes: [String] = []
            var categories: [String] = []
            var types: [String] = []

            if filter != nil {
                filter?.tags.forEach({ (tag) in
                    tags.append(tag.id)
                })

                filter?.themes.forEach({ (theme) in
                    themes.append(theme.name)
                })

                filter?.categories.forEach({ (cat) in
                    categories.append(cat.id)
                })

                filter?.types.forEach({ (type) in
                    types.append(type.rawValue)
                })
                
                parameters.updateValue(tags, forKey: "tags")
                parameters.updateValue(themes, forKey: "themes")
                parameters.updateValue(categories, forKey: "categories")
                parameters.updateValue(types, forKey: "types")
            }

            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .getMixinsWithoutPlaylists():
            let parameters: Parameters = ["includesPlaylist": 0]
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        default:
            break
        }

        return urlRequest
    }
}

//
//  FilterViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 06.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    private static let UnwindSegueIdentifier: String = "unwindToFilters"

    @IBOutlet weak var tableView: UITableView!

    var delegate: FilterViewTableDelegate!
    var filter: Filter?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: NSLocalizedString("Apply", comment: "Apply"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(FilterViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }

    func back(sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: FilterViewController.UnwindSegueIdentifier, sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == FilterViewController.UnwindSegueIdentifier {
            if let dest = segue.destination as? FiltersViewController {
                dest.activeFilters = self.filter!
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate.getFilterCount()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return delegate.getFilterViewAt(indexPath.row)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    func set(filter: Filter) {
        self.filter = filter
    }
}

protocol FilterViewTableDelegate {
    func getFilterCount() -> Int
    func getFilterViewAt(_ index: Int) -> UITableViewCell
}

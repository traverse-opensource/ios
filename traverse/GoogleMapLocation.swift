//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class GoogleMapLocation: Gloss.Decodable {
    static let PLACE_ID: String = "placeId"
    static let FULL_ADDRESS: String = "fullAddrStr"
    static let COUNTRY: String = "country"
    static let LAT: String = "latLng.lat"
    static let LNG: String = "latLng.lng"

    let placeId: String
    let fullAddrStr: String
    let country: String
    var lat: Double?
    var lng: Double?

    required init?(json: JSON) {
        guard let placeId: String = GoogleMapLocation.PLACE_ID <~~ json,
            let fullAddrStr: String = GoogleMapLocation.FULL_ADDRESS <~~ json,
            let country: String = GoogleMapLocation.COUNTRY <~~ json
            else {
                return nil
        }

        self.placeId = placeId
        self.fullAddrStr = fullAddrStr
        self.country = country
        self.lat = GoogleMapLocation.LAT <~~ json
        self.lng = GoogleMapLocation.LNG <~~ json
    }

    func toJSON() -> JSON? {

        return jsonify([
            GoogleMapLocation.PLACE_ID ~~> self.placeId,
            GoogleMapLocation.FULL_ADDRESS ~~> self.fullAddrStr,
            GoogleMapLocation.COUNTRY ~~> self.country,
            GoogleMapLocation.LAT ~~> self.lat,
            GoogleMapLocation.LNG ~~> self.lng
        ])
    }
}

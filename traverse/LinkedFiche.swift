//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class LinkedFiche: MongoEntity {
    static let FICHE: String = "fiche"
    static let VALUE: String = "value"
    static let NEXT: String = "next"

    let fiche: Fiche
    var value: String?
    var next: String?

    required init?(json: JSON) {
        guard let factory: FicheFactory = LinkedFiche.FICHE <~~ json
            else {
                return nil
        }

        guard let fiche: Fiche = factory.getFiche()
            else {
                return nil
        }

        self.fiche = fiche
        self.value = LinkedFiche.VALUE <~~ json
        self.next = LinkedFiche.NEXT <~~ json

        super.init(json: json)
    }

    func hasNext() -> Bool {
        return self.next !=  nil
    }

    func toJSON() -> JSON? {

        return jsonify([
            MongoEntity.ID ~~> self.id,
            LinkedFiche.FICHE ~~> self.fiche.toJSON(),
            LinkedFiche.VALUE ~~> self.value,
            LinkedFiche.NEXT ~~> self.next
            ])
    }
}

//
//  RelatedMediasHorizontalViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 16.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Hydra

private let RelatedMediasHorizontalViewControllerNib: String = "RelatedMediasHorizontalViewController"
private let MediaCellNib: String = "MediaCell"

class RelatedMediasHorizontalViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {

    private static let HorizontalSectionInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)

    @IBOutlet weak var relatedMediasCollectionView: UICollectionView!

    private let relatedFiches: [Fiche]

    init(relatedFiches: [Fiche]) {
        self.relatedFiches = relatedFiches
        super.init(nibName: RelatedMediasHorizontalViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.relatedMediasCollectionView.dataSource = self
        self.relatedMediasCollectionView.delegate = self

        self.relatedMediasCollectionView.register(
            UINib(nibName: MediaCellNib, bundle: nil), forCellWithReuseIdentifier: MediaCellNib)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return relatedFiches.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell = UICollectionViewCell()
        if let mediaCell: MediaCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: MediaCellNib,
            for: indexPath as IndexPath) as? MediaCell {
            let item: ItemEntity = relatedFiches[indexPath.row]
            mediaCell.set(item: item)
            cell = mediaCell
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return RelatedMediasHorizontalViewController.HorizontalSectionInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Int(collectionView.frame.height), height: Int(collectionView.frame.height))
    }

    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        if let relatedMediaFiches: [FicheMedia] = self.relatedFiches as? [FicheMedia] {
            let pager: GalleryPager = GalleryPager(medias: relatedMediaFiches, selectedIndex: indexPath.row)
            pager.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(pager, animated: true)
        }
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class MongoLocation: Gloss.Decodable {
    static let TYPE: String = "type"
    static let INDEX: String = "index"
    static let COORDINATES: String = "coordinates"

    var type: String?
    var index: Bool?
    var coordinates: [Double]?

    required init?(json: JSON) {
        self.type = MongoLocation.TYPE <~~ json
        self.index = MongoLocation.INDEX <~~ json
        self.coordinates = MongoLocation.COORDINATES <~~ json
    }

    func toJSON() -> JSON? {

        return jsonify([
            MongoLocation.TYPE ~~> self.type,
            MongoLocation.INDEX ~~> self.index,
            MongoLocation.COORDINATES ~~> self.coordinates
            ])
    }
}

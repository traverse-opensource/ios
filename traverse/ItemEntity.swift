//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class ItemEntity: MongoEntity, ItemProtocol {
    static let TYPE: String = "type"
 static let SLUG: String = "slug"
    static let NAME: String = "name"
    static let CREATED_AT: String = "created_at"
    static let LAST_UPDATED_AT: String = "last_updated_at"
    static let STATUS: String = "status"
    static let COVER: String = "cover"
    static let CREATED_BY: String = "created_by"
    static let LAST_UPDATED_BY: String = "last_update_by"
    static let THEME: String = "theme"

    static let LABELS: String = "labels"
    static let CATEGORIES: String = "categories"
    static let SUB_CATEGORIES: String = "sousCategories"
    static let TAGS: String = "tags"

    static let LIKES: String = "userIds"

    let slug: String?
    let type: ItemType
    let name: String?
    let createdAt: Date
    var lastUpdatedAt: Date?
    let status: Int
    let cover: Cover?
    var createdBy: User?
    var lastUpdatedBy: User?
    let mainTheme: ItemTheme
    var categories: [Category]?
    var subCategories: [String]?
    var tags: [Tag]?

    var likes: [String]

    required init?(json: JSON) {
        guard
            let type: ItemType = ItemEntity.TYPE <~~ json,
            let _: String = ItemEntity.CREATED_AT <~~ json,
            let _: String = ItemEntity.LAST_UPDATED_AT <~~ json,
            let status: Int = ItemEntity.STATUS <~~ json,
            let createdBy: User = ItemEntity.CREATED_BY <~~ json,
            let theme: ItemTheme = ItemEntity.THEME <~~ json,
            let likes: [String] = ItemEntity.LIKES <~~ json
            else {
                return nil
        }

        self.slug = ItemEntity.SLUG <~~ json
        self.type = type
        self.name = ItemEntity.NAME <~~ json
        let dateFormatter: DateFormatter = MongoEntity.getDateFormatter()
        self.createdAt =
            Decoder.decode(dateForKey: ItemEntity.CREATED_AT, dateFormatter: dateFormatter)(json)!
        self.lastUpdatedAt =
            Decoder.decode(dateForKey: ItemEntity.LAST_UPDATED_AT, dateFormatter: dateFormatter)(json)!
        self.status = status
        self.cover = ItemEntity.COVER <~~ json
        self.createdBy = createdBy
        self.lastUpdatedBy = ItemEntity.LAST_UPDATED_BY <~~ json
        self.mainTheme = theme

        self.likes = likes

        // set the tags, categories and subcategories differently for playlist and the fiches
        switch self.type {
        case .playlist:
            self.categories = "\(ItemEntity.LABELS).\(ItemEntity.CATEGORIES)" <~~ json
            self.subCategories = "\(ItemEntity.LABELS).\(ItemEntity.SUB_CATEGORIES)" <~~ json
            self.tags = "\(ItemEntity.LABELS).\(ItemEntity.TAGS)" <~~ json
        default:
            self.categories = ItemEntity.CATEGORIES <~~ json
            self.subCategories = ItemEntity.SUB_CATEGORIES <~~ json
            self.tags = ItemEntity.TAGS <~~ json
        }

        super.init(json: json)
    }
}

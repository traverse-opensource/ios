//
//  FicheEvent.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class FichePlace: Fiche {
    static let SCHEDULE: String = "schedule"
    static let PRESENTATION: String = "presentation"
    static let HISTORY: String = "history"
    static let TECHNICAL_INFO: String = "technical_information"
    static let MORE_INFO: String = "more_information"
    static let ACCESSIBILITY: String = "accessibility"
    static let CONTACT: String = "contact"

    var history: String?
    var schedule: String?
    var presentation: String?
    var technicalInformation: String?
    var moreInformation: String?
    var accessibility: String?
    var contact: String?

    required init?(json: JSON) {

        self.history = FichePlace.HISTORY <~~ json
        self.schedule = FichePlace.SCHEDULE <~~ json
        self.technicalInformation = FichePlace.TECHNICAL_INFO <~~ json
        self.presentation = FichePlace.PRESENTATION <~~ json
        self.accessibility = FichePlace.ACCESSIBILITY <~~ json
        self.moreInformation = FichePlace.MORE_INFO <~~ json
        self.contact = FichePlace.CONTACT <~~ json

        super.init(json: json)
    }


    override func toJSON() -> JSON? {

        var jsonThemes: [JSON] = []
        themes.forEach { (theme) in
            jsonThemes.append(theme.toJSON()!)
        }

        var jsonCategories: [JSON] = []
        categories?.forEach { (cat) in
            jsonCategories.append(cat.toJSON()!)
        }

        var jsonTags: [JSON] = []
        tags?.forEach { (tag) in
            jsonTags.append(tag.toJSON()!)
        }

        return jsonify([
            MongoEntity.ID ~~> self.id,
            ItemEntity.TYPE ~~> self.type,
            ItemEntity.NAME ~~> self.name,
            Gloss.Encoder.encode(dateForKey: ItemEntity.CREATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.createdAt),
            Gloss.Encoder.encode(dateForKey: ItemEntity.LAST_UPDATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.lastUpdatedAt),
            ItemEntity.STATUS ~~> self.status,
            ItemEntity.COVER ~~> self.cover?.toJSON(),
            ItemEntity.CREATED_BY ~~> self.createdBy?.toJSON(),
            ItemEntity.LAST_UPDATED_BY ~~> self.lastUpdatedBy?.toJSON(),
            ItemEntity.THEME ~~> self.mainTheme.toJSON(),
            ItemEntity.SUB_CATEGORIES ~~> self.subCategories,
            ItemEntity.LIKES ~~> self.likes,
            Fiche.REFERENCES ~~> self.references,
            Fiche.LIKES ~~> self.city,
            Fiche.LATITUDE ~~> self.latitude,
            Fiche.LONGITUDE ~~> self.longitude,
            Fiche.MONGO_LOCATION ~~> self.mongoLocation?.toJSON(),
            Fiche.GOOGLE_MAP_LOCATION ~~> self.googleMapLocation?.toJSON(),
            Fiche.SHORT_DESCRIPTION ~~> self.shortDescription,
            Fiche.LONG_DESCRIPTION ~~> self.longDescription,
            FichePlace.SCHEDULE ~~> self.schedule,
            FichePlace.PRESENTATION ~~> self.presentation,
            FichePlace.HISTORY ~~> self.history,
            FichePlace.TECHNICAL_INFO ~~> self.technicalInformation,
            FichePlace.MORE_INFO ~~> self.moreInformation,
            FichePlace.ACCESSIBILITY ~~> self.accessibility,
            FichePlace.CONTACT ~~> self.contact,
            [ItemEntity.CATEGORIES: jsonCategories],
            [ItemEntity.TAGS: jsonTags],
            [Fiche.THEMES: jsonThemes]
        ])
    }
}

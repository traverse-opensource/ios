//
//  RootViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 04.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Material
import ESTabBarController_swift

class RootViewController: ESTabBarController {
    static let DISCOVERY_INDEX: Int = 0

    var profileTab: ESTabBarItem?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let applicationDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {

            if applicationDelegate.universalLinkUserActivity != nil {
                _ = applicationDelegate.handleUserActivityForUniversalLink(applicationDelegate.universalLinkUserActivity!)
                applicationDelegate.universalLinkUserActivity = nil
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        if let applicationDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let window: UIWindow = applicationDelegate.window {
                window.rootViewController = AppSnackbarController(rootViewController: self)
            }
        }

        let defaults = UserDefaults.standard
        let isFirst = defaults.bool(forKey: "FIRST_LAUNCH")
        
        if !isFirst {
            defaults.set(true, forKey: "FIRST_LAUNCH")

            let tutorialIntro = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tutorialIntro") as! TutuorialIntroViewController
            tutorialIntro.parentController = self
            self.present(tutorialIntro, animated: true, completion: nil)
        }

        // Set up tab bar
        let discoveryTabView: ESTabBarItemContentView =
            RootViewController.createTabBarView()
        let discoverTab: ESTabBarItem = ESTabBarItem.init(discoveryTabView, title: NSLocalizedString("Discovery", comment: "Discovery"), image: #imageLiteral(resourceName: "ic_discovery").resize(toHeight: 24), selectedImage: #imageLiteral(resourceName: "ic_discovery").resize(toHeight: 24))
        let exploreTabView: ESTabBarItemContentView =
            RootViewController.createTabBarView()
        let exploreTab: ESTabBarItem = ESTabBarItem.init(exploreTabView, title: NSLocalizedString("Explore", comment: "Explore"), image: #imageLiteral(resourceName: "ic_explore").resize(toHeight: 24), selectedImage: #imageLiteral(resourceName: "ic_explore").resize(toHeight: 24))
        let playlistsTabView: ESTabBarItemContentView =
            RootViewController.createTabBarView()
        let playlistsTab: ESTabBarItem = ESTabBarItem.init(playlistsTabView, title: NSLocalizedString("Playlists", comment: "Playlists"), image: #imageLiteral(resourceName: "ic_playlists").resize(toHeight: 24), selectedImage: #imageLiteral(resourceName: "ic_playlists").resize(toHeight: 24))
        let profileTabView: ESTabBarItemContentView =
            RootViewController.createTabBarView()
        self.profileTab = ESTabBarItem.init(profileTabView, title: NSLocalizedString("Profile", comment: "Profile"), image: #imageLiteral(resourceName: "ic_profile").resize(toHeight: 24), selectedImage: #imageLiteral(resourceName: "ic_profile").resize(toHeight: 24))

        self.viewControllers?[0].tabBarItem = discoverTab
        self.viewControllers?[1].tabBarItem = exploreTab
        self.viewControllers?[2].tabBarItem = playlistsTab
        self.viewControllers?[3].tabBarItem = profileTab

        self.tabBar.barTintColor = UIColor.primaryColor
        self.tabBar.tintColor = UIColor.primaryColor
        self.tabBar.isTranslucent = false
        self.tabBar.backgroundImage = UIImage()

        self.selectedIndex = RootViewController.DISCOVERY_INDEX
        view.backgroundColor = UIColor.primaryColor

    }

    private static func createTabBarView() -> ESTabBarItemContentView {
        let tabBarView: ESTabBarItemContentView = TraverseTabBarContentView()
        tabBarView.backdropColor = UIColor.white
        tabBarView.highlightTextColor = UIColor.accentColor
        tabBarView.iconColor = UIColor.textColor
        tabBarView.highlightIconColor = UIColor.accentColor
        tabBarView.textColor = UIColor.textColor
        tabBarView.highlightBackdropColor = UIColor.white
        return tabBarView
    }

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {

        if item != profileTab {
            super.tabBar(tabBar, didSelect: item)
        }
        else {
            if UserAPI.getCurrentUser() == nil {
                self.profileTab?.contentView?.selected = false
                self.profileTab?.contentView?.updateDisplay()

                (tabBar.selectedItem as! ESTabBarItem).contentView?.selected = true
                (tabBar.selectedItem as! ESTabBarItem).contentView?.updateDisplay()
                
                let noUserAlert = UIAlertController(title: "Traverse", message: NSLocalizedString("You have to be autentified to use this functionality. Would you like to login?", comment: "You have to be autentified to use this functionality. Would you like to login?"), preferredStyle: UIAlertControllerStyle.alert)
                //
                //                notifAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
                //                    notifAlert.dismiss(animated: true, completion: {
                //                    })
                //                }))

                noUserAlert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel, handler: { (_) in
                    noUserAlert.dismiss(animated: true, completion: nil)
                }))

                noUserAlert.addAction(UIAlertAction(title: NSLocalizedString("Login", comment: "Login"), style: .default, handler: { (_) in
                    noUserAlert.dismiss(animated: true, completion: nil)

                    //super.tabBar(tabBar, didSelect: tabBar.selectedItem!)
                    let loginview = LoginViewController()
                    loginview.parentNavigationController = self.selectedViewController as? UINavigationController
                    self.present(loginview, animated: true, completion: nil)

                }))

                self.present(noUserAlert, animated: true, completion: nil)

            }
            else {
                super.tabBar(tabBar, didSelect: item)
            }
        }
    }
}

extension RootViewController {
    func genericError() {
        guard let snackbarController: SnackbarController = self.snackbarController else {
            return
        }

        let snackbar: Snackbar = snackbarController.snackbar

        snackbar.text = NSLocalizedString("Error! An unknown error occurred!", comment: "Error! An unknown error occurred!")
        showErrorSnackbar(snackbarController)
    }

    func error(_ errorText: String) {
        guard let snackbarController: SnackbarController = self.snackbarController else {
            return
        }

        let snackbar: Snackbar = snackbarController.snackbar

        snackbar.text = errorText
        showErrorSnackbar(snackbarController)
    }

    func message(_ messageText: String) {
        guard let snackbarController: SnackbarController = self.snackbarController else {
            return
        }

        let snackbar: Snackbar = snackbarController.snackbar

        snackbar.text = messageText
        showMessageSnackbar(snackbarController)
    }


    private func showErrorSnackbar(_ snackbarController: SnackbarController) {
        snackbarController.snackbar.textLabel.textColor = UIColor.red

        _ = snackbarController.animate(snackbar: .visible, delay: 1)
        _ = snackbarController.animate(snackbar: .hidden, delay: 4)
    }

    private func showMessageSnackbar(_ snackbarController: SnackbarController) {
        snackbarController.snackbar.textLabel.textColor = UIColor.green

        _ = snackbarController.animate(snackbar: .visible, delay: 1)
        _ = snackbarController.animate(snackbar: .hidden, delay: 4)
    }
}

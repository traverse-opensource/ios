//
//  AddPlaylistViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 05.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class AddPlaylistViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {
    private static let SmallCardNib: String = "SmallCard"
    private static let NewPlaylistCardNib: String = "NewPlaylistCard"
    private static let Insets: UIEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 0, right: 8.0)
    private static let SmallCardHeight: CGFloat = 248

    @IBOutlet weak var cancelButton: UIImageView!
    @IBOutlet weak var myPlaylistCollectionView: UICollectionView!
    @IBOutlet weak var loadingView: UIView!
    
    var myPlaylists: [Playlist] = []
    var ficheToAdd: Fiche!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.myPlaylistCollectionView.delegate = self
        self.myPlaylistCollectionView.dataSource = self

        self.myPlaylistCollectionView.register(
            UINib(nibName: AddPlaylistViewController.NewPlaylistCardNib,
                  bundle: nil),
            forCellWithReuseIdentifier: AddPlaylistViewController.NewPlaylistCardNib)
        self.myPlaylistCollectionView.register(
            UINib(nibName: AddPlaylistViewController.SmallCardNib,
                  bundle: nil),
            forCellWithReuseIdentifier: AddPlaylistViewController.SmallCardNib)

        self.myPlaylistCollectionView.reloadData()

        self.cancelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AddPlaylistViewController.onCancel)))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func set(playlists: [Playlist], ficheToAdd: Fiche) {
        self.myPlaylists = playlists
        self.ficheToAdd = ficheToAdd
        if isViewLoaded {
            self.myPlaylistCollectionView.reloadData()
        }
    }

    private static let HorizontalSectionInsets: UIEdgeInsets = UIEdgeInsets(top: 16, left: 16, bottom: 0, right: 8)

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.myPlaylists.count + 1
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell = UICollectionViewCell()

        if indexPath.row == 0 {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: AddPlaylistViewController.NewPlaylistCardNib, for: indexPath)
        } else {
            if let smallCard: SmallCard = collectionView.dequeueReusableCell(
                withReuseIdentifier: AddPlaylistViewController.SmallCardNib,
                for: indexPath as IndexPath) as? SmallCard {

                let item: ItemEntity = self.myPlaylists[indexPath.row - 1] as ItemEntity
                smallCard.set(item: item)
                cell = smallCard
            }
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let playlistSettingsController: PlaylistSettingsViewController = PlaylistSettingsViewController(fiche: self.ficheToAdd, container: self)
            present(playlistSettingsController, animated: true)
        } else {
            let playlist: Playlist = self.myPlaylists[indexPath.row - 1]
            if !playlist.contains(fiche: self.ficheToAdd) {
                self.loadingView.isHidden = false
                PlaylistAPI.addFiche(playlistId: playlist.id, ficheId: self.ficheToAdd.id).then(in: .main, { (playlist :Playlist) in
                    Playlist.updateMyPlaylistLocally(playlist)

                    self.dismiss(animated: true)
                    self.loadingView.isHidden = true
                }).catch(in: .main, { (_: Error) -> Void in
                    self.showGenericError(true)
                    self.loadingView.isHidden = true
                })
            } else {
                self.showError(NSLocalizedString("This playlist contains already this fiche", comment: "This playlist contains already this fiche!"), true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return AddPlaylistViewController.Insets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let availableWidth: CGFloat = collectionView.frame.width - AddPlaylistViewController.Insets.left - AddPlaylistViewController.Insets.right
        let width: GLfloat = GLfloat((availableWidth / 2.0))
        return CGSize(width: Int(width), height: Int(AddPlaylistViewController.SmallCardHeight))
    }

    func onCancel() {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

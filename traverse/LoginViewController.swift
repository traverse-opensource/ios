//
//  LoginViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 29.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import TwitterKit
import Alamofire
import Hydra

class LoginViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {

    @IBOutlet weak var cancelButton: UIImageView!
    @IBOutlet weak var logoTraverseImageView: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!

    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    var parentNavigationController: UINavigationController?

    override func viewDidLoad() {
        super.viewDidLoad()

        logoTraverseImageView.image = #imageLiteral(resourceName: "logo-traverse-white").withRenderingMode(.alwaysTemplate)
        logoTraverseImageView.tintColor = UIColor.black

        loginButton.backgroundColor = UIColor.accentColor
        registerButton.backgroundColor = UIColor.clear

        twitterButton.backgroundColor = UIColor.twitterColor
        facebookButton.backgroundColor = UIColor.facebookColor
        googleButton.backgroundColor = UIColor.googleColor

        cancelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LoginViewController.onCancel)))

        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }

    func onCancel() {
        self.dismiss(animated: true, completion: nil)
    }

    func signInFromFacebook(facebookData: [String : AnyObject]?) {

        if facebookData != nil {

            var pictureUrl = ""
            if let pictureObj = facebookData?["picture"] {
                if let data = pictureObj.value(forKey: "data") as? AnyObject {
                    pictureUrl = data.value(forKey: "url") as! String
                }
            }

            UserAPI.signupFromSN(kind: Const.FACEBOOK_AUTH, userId: facebookData!["id"] as! String, userName: facebookData!["name"] as! String, email: facebookData!["email"] as! String, picture: pictureUrl).then(in: .main, { (traverseUser: User) in

                UserDefaults.standard.set(true, forKey: Const.IS_USER_LOGGED_IN_KEY)
                UserDefaults.standard.set(Const.FACEBOOK_AUTH, forKey: Const.LOGIN_METHOD)
                UserDefaults.standard.set(facebookData!["id"] as! String, forKey: Const.FACEBOOK_USER_ID_KEY)
                UserDefaults.standard.set(facebookData!["email"] as! String, forKey: Const.FACEBOOK_USER_EMAIL_KEY)
                UserDefaults.standard.set(facebookData!["name"] as! String, forKey: Const.FACEBOOK_USER_NAME_KEY)

                UserAPI.syncFavorites(traverseUser).then({ (user) in
                     UserDefaults.standard.set(user.toJSON(), forKey: Const.TRAVERSE_AUTH_USER_KEY)
                }).catch({ (_) -> Void in
                    UserDefaults.standard.set(traverseUser.toJSON(), forKey: Const.TRAVERSE_AUTH_USER_KEY)
                }).always(body: {
                    self.syncPlaylistsAndDismiss()
                })

            }).catch({ (error) -> Void in
                print(error.localizedDescription)
                self.displayError(error)
            })

        }
        else {
            self.showError(NSLocalizedString("Login failed, please try again", comment: "Login failed, please try again"), true)
        }
    }

    func syncPlaylistsAndDismiss() {
        UserAPI.getMyPlaylists(userId: (UserAPI.getCurrentUser()?.id)!).then({ (playlists) in
            let promises: [Promise<Playlist>] = playlists.map { return PlaylistAPI.getPlaylist(with: $0.id) }
            all(promises)
                .then(in: .main) { (playlists: [Playlist]) in
                    Playlist.saveMyPlaylistsLocally(playlists)
            }
                .catch({ (error) -> Void in
                    print(error)
                })
        }).always {
            self.dismiss(animated: true, completion: {
                self.parentNavigationController?.showMessage(NSLocalizedString("Login succeed!", comment: "Login succeed!"))
            })
        }
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {

            UserAPI.signupFromSN(kind: Const.GOOGLE_AUTH, userId: user.authentication.idToken, userName: user.profile.name, email: user.profile.email, picture: user.profile.imageURL(withDimension: 200).absoluteString).then(in: .main, { (traverseUser: User) in

                UserDefaults.standard.set(true, forKey: Const.IS_USER_LOGGED_IN_KEY)
                UserDefaults.standard.set(Const.GOOGLE_AUTH, forKey: Const.LOGIN_METHOD)
                UserDefaults.standard.set(user.authentication.clientID, forKey: Const.GOOGLE_USER_CLIENT_ID_KEY)
                UserDefaults.standard.set(user.authentication.idToken, forKey: Const.GOOGLE_USER_TOKEN_KEY)
                UserDefaults.standard.set(user.profile.email, forKey: Const.GOOGLE_USER_EMAIL_KEY)
                UserDefaults.standard.set(user.profile.name, forKey: Const.GOOGLE_USER_NAME_KEY)

                UserAPI.syncFavorites(traverseUser).then({ (user) in
                    UserDefaults.standard.set(user.toJSON(), forKey: Const.TRAVERSE_AUTH_USER_KEY)
                }).catch({ (_) -> (Void) in
                    UserDefaults.standard.set(traverseUser.toJSON(), forKey: Const.TRAVERSE_AUTH_USER_KEY)
                }).always(body: {
                    self.syncPlaylistsAndDismiss()
                })

            }).catch({ (error) -> Void in
                print(error.localizedDescription)
                self.displayError(error)
            })
        } else {
            print("\(error.localizedDescription)")
            self.showError(NSLocalizedString("Login failed, please try again", comment: "Login failed, please try again"), true)
        }
    }

    func getFBUserData() {
        if (FBSDKAccessToken.current()) != nil {
            FBSDKGraphRequest(
                graphPath: "me",
                parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]
                ).start(completionHandler: { (_, result, error) -> Void in
                    if error == nil {
                        self.signInFromFacebook(facebookData: result as? [String : AnyObject])

                    } else {
                        print("\(String(describing: error?.localizedDescription))")
                    }
                })
        }
    }

    @IBAction func onClickLogin(_ sender: Any) {
        if !(loginField.text?.isEmpty)! && !(passwordField.text?.isEmpty)! {
            UserAPI.login(email: loginField.text!, password: passwordField.text!).then({ (user) in
                UserDefaults.standard.set(true, forKey: Const.IS_USER_LOGGED_IN_KEY)
                UserDefaults.standard.set(Const.CUSTOM_AUTH, forKey: Const.LOGIN_METHOD)
                UserDefaults.standard.set(user.id, forKey: Const.TRAVERSE_USER_ID_KEY)
                UserDefaults.standard.set(user.profile?.name, forKey: Const.TRAVERSE_USER_NAME_KEY)
                UserDefaults.standard.set(user.email, forKey: Const.TRAVERSE_USER_EMAIL_KEY)

                UserAPI.syncFavorites(user).then({ (updatedUser) in
                    UserDefaults.standard.set(updatedUser.toJSON(), forKey: Const.TRAVERSE_AUTH_USER_KEY)
                }).catch({ (_) -> (Void) in
                    UserDefaults.standard.set(user.toJSON(), forKey: Const.TRAVERSE_AUTH_USER_KEY)
                }).always(body: {
                    self.syncPlaylistsAndDismiss()
                })

            }).catch({ (error) -> Void in
                print(error.localizedDescription)
                self.displayError(error)
            })
        }

    }

    @IBAction func onClickRegister(_ sender: Any) {

        if self.parentNavigationController != nil {
            self.dismiss(animated: true)
            let registerView = RegisterViewController()
            registerView.parentNavigationController = self.parentNavigationController
            self.parentNavigationController?.present(registerView, animated: true, completion: nil)
        }
        else {
            self.present(RegisterViewController(), animated: true, completion: nil)
        }

    }

    @IBAction func onClickTwitter(_ sender: Any) {
        Twitter.sharedInstance().logIn { (session, error) in
            if session != nil {
                let client = TWTRAPIClient.withCurrentUser()

                client.requestEmail(forCurrentUser: { (email, error) in
                    client.loadUser(withID: (session?.userID)!, completion: { (user, error) in

                        UserAPI.signupFromSN(kind: Const.TWITTER_AUTH, userId: (session?.userID)!, userName: (user?.screenName)!, email: email!, picture: (user?.profileImageURL)!).then(in: .main, { (traverseUser: User) in

                            UserDefaults.standard.set(true, forKey: Const.IS_USER_LOGGED_IN_KEY)
                            UserDefaults.standard.set(Const.TWITTER_AUTH, forKey: Const.LOGIN_METHOD)
                            UserDefaults.standard.set(session?.userID, forKey: Const.TWITTER_USER_ID_KEY)
                            UserDefaults.standard.set(session?.userName, forKey: Const.TWITTER_USER_NAME_KEY)
                            UserDefaults.standard.set(email, forKey: Const.TWITTER_USER_EMAIL_KEY)

                            UserAPI.syncFavorites(traverseUser).then({ (user) in
                                UserDefaults.standard.set(user.toJSON(), forKey: Const.TRAVERSE_AUTH_USER_KEY)
                            }).catch({ (_) -> (Void) in
                                UserDefaults.standard.set(traverseUser.toJSON(), forKey: Const.TRAVERSE_AUTH_USER_KEY)
                            }).always(body: {
                                self.syncPlaylistsAndDismiss()
                            })

                        }).catch({ (error) -> Void in
                            print(error.localizedDescription)
                            self.displayError(error)
                        })

                    })

                })
            }
            else if error != nil {
                print("\(String(describing: error?.localizedDescription))")
            }

        }
    }

    @IBAction func onClickFacebook(_ sender: Any) {
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if error == nil {
                let fbLoginResult: FBSDKLoginManagerLoginResult = result!
                if fbLoginResult.grantedPermissions != nil {
                    if fbLoginResult.grantedPermissions.contains("email") {
                        self.getFBUserData()
                    }
                }
            } else {
                print("\(String(describing: error?.localizedDescription))")
            }
        }
    }
    
    @IBAction func onClickGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }

    func displayError(_ error: Error) {
        if let afError = error as? Alamofire.AFError {
            if afError.isResponseSerializationError {
                let nsError = afError.underlyingError! as NSError
                if let responseData = nsError.userInfo.valueForKeyPath(keyPath: "responseDataDump") as? Data {
                    do {
                        let errorJson = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: String]
                        self.showError(NSLocalizedString((errorJson?["message"])!, comment: (errorJson?["message"])!), true)
                    }
                    catch _ {
                        let errorMessage = String(data: responseData, encoding: .utf8)
                        if errorMessage != nil {
                            self.showError(errorMessage!, true)
                        }
                        else {
                            self.showGenericError(true)
                        }
                    }
                }

            }
        }
        else {
            self.showError(NSLocalizedString("Login failed, please try again", comment: "Login failed, please try again"), true)
        }
    }

//    override func showError(_ errorText: String) {
//
//        let errorAlert = UIAlertController(title: "Traverse", message: NSLocalizedString(errorText, comment: errorText), preferredStyle: UIAlertControllerStyle.alert)
//
//        errorAlert.addAction(UIAlertAction(title: NSLocalizedString("Close", comment: "Close"), style: .destructive, handler: { (_) in
//            errorAlert.dismiss(animated: true, completion: nil)
//        }))
//
//        self.present(errorAlert, animated: true, completion: nil)
//    }
}

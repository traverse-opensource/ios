//
//  MarkerImage.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 25.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class MarkerImage: UIView {

    @IBOutlet weak var itemBackground: UIView!
    @IBOutlet weak var itemTypeImage: UIImageView!


    class func instanceFromNib() -> MarkerImage {
        return UINib(nibName: "MarkerImage", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MarkerImage
    }
}

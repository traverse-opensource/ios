//
//  Playlist.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss
import Haneke

class Playlist: ItemEntity {
    static let LINKED_FICHES: String = "linkedFiches"
    static let DESCRIPTION: String = "description"

    var linkedFiches: [LinkedFiche]?
    var linkedFichesIds: [String]?
    var description: String?

    required init?(json: Gloss.JSON) {
        if let linkedFiches: [LinkedFiche] = Playlist.LINKED_FICHES <~~ json {
            self.linkedFiches = linkedFiches
        } else {
            if let linkedFichesIds: [String] = Playlist.LINKED_FICHES <~~ json {
                self.linkedFichesIds = linkedFichesIds
            } else {
                self.linkedFichesIds = []
            }
        }
        self.description = Playlist.DESCRIPTION <~~ json

        super.init(json: json)
    }

    func isIdsOnly() -> Bool {
        return self.linkedFiches == nil && self.linkedFichesIds != nil
    }

    func contains(fiche: Fiche) -> Bool {
        var found: Bool = false
        let ficheId: String = fiche.id
        if isIdsOnly() {
            found = (self.linkedFichesIds?.contains(ficheId))!
        } else {
            for linkedFiche in self.linkedFiches! {
                let fiche: Fiche = linkedFiche.fiche
                if fiche.id == ficheId {
                    found = true
                    break
                }
            }
        }
        return found
    }

    func toJSON() -> Gloss.JSON? {

        var jsonLinkedFiches: [Gloss.JSON] = []
        linkedFiches?.forEach { (fiche) in
            jsonLinkedFiches.append(fiche.toJSON()!)
        }

        var jsonCategories: [Gloss.JSON] = []
        categories?.forEach { (cat) in
            jsonCategories.append(cat.toJSON()!)
        }

        var jsonTags: [Gloss.JSON] = []
        tags?.forEach { (tag) in
            jsonTags.append(tag.toJSON()!)
        }

        return jsonify([
            MongoEntity.ID ~~> self.id,
            ItemEntity.TYPE ~~> self.type,
            ItemEntity.NAME ~~> self.name,
            Gloss.Encoder.encode(dateForKey: ItemEntity.CREATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.createdAt),
            Gloss.Encoder.encode(dateForKey: ItemEntity.LAST_UPDATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.lastUpdatedAt),
            ItemEntity.STATUS ~~> self.status,
            ItemEntity.COVER ~~> self.cover?.toJSON(),
            ItemEntity.CREATED_BY ~~> self.createdBy?.toJSON(),
            ItemEntity.LAST_UPDATED_BY ~~> self.lastUpdatedBy?.toJSON(),
            ItemEntity.THEME ~~> self.mainTheme.toJSON(),
            ItemEntity.SUB_CATEGORIES ~~> self.subCategories,
            ItemEntity.LIKES ~~> self.likes,
            [ItemEntity.CATEGORIES: jsonCategories],
            [ItemEntity.TAGS: jsonTags],
            Playlist.DESCRIPTION ~~> self.description,
            [Playlist.LINKED_FICHES: jsonLinkedFiches]
        ])
    }

    static func addToMyPlaylistLocally(_ playlist: Playlist) {
        var myDataArray = UserDefaults.standard.array(forKey: Const.MY_PLAYLISTS_DATA)
        if myDataArray == nil {
            myDataArray = []
        }

        myDataArray?.append(playlist.toJSON()!)
        UserDefaults.standard.set(myDataArray, forKey: Const.MY_PLAYLISTS_DATA)
    }

    static func removeFromMyPlaylistLocally(_ playlist: Playlist) {
        var myDataArray = UserDefaults.standard.array(forKey: Const.MY_PLAYLISTS_DATA)
        if myDataArray != nil {
            var oldIndex = -1
            for (index, playlistData) in (myDataArray?.enumerated())! {
                if let oldPlaylist = Playlist(json: playlistData as! Gloss.JSON), oldPlaylist.id == playlist.id {
                    oldIndex = index
                    break
                }
            }

            myDataArray?.remove(at: oldIndex)
        }

        UserDefaults.standard.set(myDataArray, forKey: Const.MY_PLAYLISTS_DATA)
    }

    static func updateMyPlaylistLocally(_ playlist: Playlist) {
        var myDataArray = UserDefaults.standard.array(forKey: Const.MY_PLAYLISTS_DATA)
        if myDataArray != nil {
            var oldIndex = -1
            for (index, playlistData) in (myDataArray?.enumerated())! {
                if let oldPlaylist = Playlist(json: playlistData as! Gloss.JSON), oldPlaylist.id == playlist.id {
                    oldIndex = index
                    break
                }
            }

            myDataArray?.remove(at: oldIndex)
            myDataArray?.insert(playlist.toJSON()!, at: oldIndex)
        }

        UserDefaults.standard.set(myDataArray, forKey: Const.MY_PLAYLISTS_DATA)
    }

    static func saveMyPlaylistsLocally(_ playlists: [Playlist]) {
        var myDataArray: [Gloss.JSON] = []

        playlists.forEach { (playlist) in
            //Load covers (for caching images)
            if playlist.cover != nil {
                let cache = Shared.imageCache
                let fetcherThumb = NetworkFetcher<UIImage>(URL: (playlist.cover?.getThumbImageUrl())!)
                let fetcherImg = NetworkFetcher<UIImage>(URL: (playlist.cover?.getImageUrl())!)
                cache.fetch(fetcher: fetcherThumb)
                cache.fetch(fetcher: fetcherImg)
            }

            myDataArray.append((playlist.toJSON())!)
        }

        UserDefaults.standard.set(myDataArray, forKey: Const.MY_PLAYLISTS_DATA)
    }
}

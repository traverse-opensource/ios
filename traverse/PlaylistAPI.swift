//
//  PlaylistAPI.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Hydra

class PlaylistAPI {
    static func getPlaylists() -> Promise<[Playlist]> {
        return ApiRequest.request(array: PlaylistRouter.getPlaylists(), type: Playlist.self)
    }

    static func getPlaylist(with id: String) -> Promise<Playlist> {
        return ApiRequest.request(object: PlaylistRouter.getPlaylist(id: id), type: Playlist.self)
    }

    static func getMyFavoritePlaylists() -> Promise<[Playlist]> {

        if let favArray: [String] = UserDefaults.standard.array(forKey: Const.FAVORITES_PLAYLIST_KEY) as? [String] {

            let promises: [Promise<Playlist>] = favArray.map { return PlaylistAPI.getPlaylist(with: $0) }
            return all(promises)
        }

        return all()
    }

    static func dislikePlaylist(id: String, userId: String) -> Promise<Playlist> {
        return ApiRequest.request(object: PlaylistRouter.dislikePlaylist(id: id, userId: userId), type: Playlist.self)
    }

    static func likePlaylist(id: String, userId: String) -> Promise<Playlist> {
        return ApiRequest.request(object: PlaylistRouter.likePlaylist(id: id, userId: userId), type: Playlist.self)
    }

    static func deletePlaylist(id: String) -> Promise<Any> {
        return ApiRequest.request(object: PlaylistRouter.deletePlaylist(id: id))
    }

    static func createPlaylist(name: String, description: String,
                               createdBy: String, photoUrl: String,
                               ficheId: String, tags: [String],
                               categories: [String], subCategories: [String],
                               status: Bool) -> Promise<Playlist> {
        var stringStatus: String = "0"
        if status {
            stringStatus = "1"
        }
        return ApiRequest.request(object: PlaylistRouter.createPlaylist(name: name,
                                                                        description: description,
                                                                        createdBy: createdBy,
                                                                        photoUrl: photoUrl,
                                                                        ficheId: ficheId,
                                                                        tags: tags,
                                                                        categories: categories,
                                                                        subCategories: subCategories,
                                                                        status: stringStatus),
                                  type: Playlist.self)
    }

    static func addFiche(playlistId: String, ficheId: String) -> Promise<Playlist> {
        return ApiRequest.request(object: PlaylistRouter.addFiche(playlistId: playlistId,
                                                                  ficheId: ficheId),
                                  type: Playlist.self)
    }

    static func updatePlaylist(id: String, name: String, description: String,
                               createdBy: String, photoUrl: String, tags: [String],
                               categories: [String], subCategories: [String],
                               status: Bool) -> Promise<Playlist> {
        var stringStatus: String = "0"
        if status {
            stringStatus = "1"
        }
        return ApiRequest.request(object: PlaylistRouter.updatePlaylist(id: id,
                                                                        name: name,
                                                                        description: description,
                                                                        createdBy: createdBy,
                                                                        photoUrl: photoUrl,
                                                                        tags: tags,
                                                                        categories: categories,
                                                                        subCategories: subCategories,
                                                                        status: stringStatus),
                                  type: Playlist.self)
    }

    static func swapFiches(id: String, ficheIds: [String]) -> Promise<Playlist> {
        return ApiRequest.request(object: PlaylistRouter.swapFiches(id: id, ficheIds: ficheIds), type: Playlist.self)
    }

}

//
//  UIView+setFicheTypeImage.swift
//  traverse
//
//  Created by Mattia Gustarini on 27.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

extension UIView {

    static func getFicheTypeImage(fiche: Fiche) -> UIImage {
        return getFicheTypeImage(ficheType: fiche.type)
    }

    static func getFicheTypeImage(ficheType: ItemType) -> UIImage {
        switch ficheType {
        case .events:
            return #imageLiteral(resourceName: "ic_event")
        case .people:
            return #imageLiteral(resourceName: "ic_people")
        case .objects:
            return #imageLiteral(resourceName: "ic_object")
        case .media:
            return #imageLiteral(resourceName: "ic_media")
        default:
            return #imageLiteral(resourceName: "ic_site")
        }
    }

    static func setFicheTypeImage(_ imageView: UIImageView, fiche: Fiche) {
        imageView.image = getFicheTypeImage(fiche: fiche)
    }
}

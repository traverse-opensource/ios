//
//  ItemProtocol.swift
//  traverse
//
//  Created by Mattia Gustarini on 27.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

protocol ItemProtocol {
    var type: ItemType { get }
}

//
//  FicheHeaderViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 09.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class FicheHeaderViewController: UIViewController {
    @IBOutlet weak var ficheImage: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var ficheTypeView: UIView!
    @IBOutlet weak var ficheTypeImage: UIImageView!
    @IBOutlet weak var ficheThemeLabel: UILabel!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!

    @IBOutlet weak var videoOverlay: UIView!
    @IBOutlet weak var playIcon: UIImageView!

    private var fiche: Fiche!
    private var initialImageHeightMultiplier: CGFloat!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialImageHeightMultiplier = self.imageHeight.multiplier
        self.playIcon.image = #imageLiteral(resourceName: "ic_diapo").withRenderingMode(.alwaysTemplate).tint(with: UIColor.white)

        // Do any additional setup after loading the view.
        if fiche != nil {
            setUpView()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func set(fiche: Fiche) {
        self.fiche = fiche
        if isViewLoaded {
            setUpView()
        }

        self.videoOverlay.isHidden = true

        if let media = self.fiche as? FicheMedia {
            if media.path != nil {
                self.videoOverlay.isHidden = false
                self.videoOverlay.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FicheHeaderViewController.displayExternalLink)))
            }
        }
    }

    func displayExternalLink() {
        if let media = self.fiche as? FicheMedia {
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(URL(string: media.path!)!, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(URL(string: media.path!)!)
//            }
            let webViewController = WebViewController()
            webViewController.url = URL(string: media.path!)

            self.navigationController?.pushViewController(webViewController, animated: true)
        }
    }

    func setImageHeightMultiplier(_ multiplier: CGFloat) {
        if multiplier <= 1.0 && multiplier >= 0.001 {
            let newMultiplier: CGFloat = self.initialImageHeightMultiplier * multiplier
            self.imageHeight = self.imageHeight.setMultiplier(multiplier: newMultiplier)
            self.ficheImage.layoutIfNeeded()
        }
    }

    private func setUpView() {
        if let cover: Cover = self.fiche.cover {
            self.ficheImage.contentMode = .center
            self.ficheImage.hnk_setImageFromURL(cover.getImageUrl(), placeholder: UIView.getFicheTypeImage(fiche: fiche), format: nil, failure: nil, success: {(image) in
                self.ficheImage.image = image
                self.ficheImage.contentMode = .scaleAspectFill
            })

        }

        UIView.setFicheTypeImage(self.ficheTypeImage, fiche: fiche)
        UIView.setTheme(fiche: fiche,
                        separatorView: self.separatorView,
                        iconView: self.ficheTypeView,
                        themeNameLabel: self.ficheThemeLabel)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

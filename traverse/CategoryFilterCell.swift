//
//  CategoryFilterCell.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 06.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class CategoryFilterCell: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var toggleImage: UIImageView!
    @IBOutlet weak var emptySelectView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state

        toggleImage.isHidden = !selected
        emptySelectView.isHidden = selected
    }

    func set(_ item: Category) {
        categoryLabel.text = item.name
    }
}

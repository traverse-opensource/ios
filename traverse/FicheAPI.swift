//
//  FicheAPI.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Hydra
import Gloss

class FicheAPI {

    static func getFiches() -> Promise<[Fiche]> {
        return ApiRequest.request(object: FicheRouter.getFiches(), type: FicheArray.self)
            .then(in: .background, { (ficheArray: FicheArray) -> (Promise<[Fiche]>) in
                let factories: [FicheFactory] = ficheArray.fiches
            return Promise<[Fiche]>(in: .background, { resolve, reject, _ in
                let fiches: [Fiche] = factories.map({ (factory: FicheFactory) -> Fiche! in
                    if let fiche: Fiche = factory.getFiche() {
                        return fiche
                    } else {
                        reject(APIError.decode(what: "Fiche"))
                    }
                    return nil
                })
                resolve(fiches)
            })
        })
    }

    static func dislikeFiche(id: String, userId: String) -> Promise<Fiche> {
        return ApiRequest.request(object: FicheRouter.dislikeFiche(id: id, userId: userId), type: FicheFactory.self)
            .then(in: .background, { (factory: FicheFactory) -> (Promise<Fiche>) in
                return Promise<Fiche>(in: .background, { resolve, reject, _ in
                    if let fiche: Fiche = factory.getFiche() {
                        resolve(fiche)
                    } else {
                        reject(APIError.decode(what: "Fiche"))
                    }
                })
            })
    }

    static func likeFiche(id: String, userId: String) -> Promise<Fiche> {
        return ApiRequest.request(object: FicheRouter.likeFiche(id: id, userId: userId), type: FicheFactory.self)
            .then(in: .background, { (factory: FicheFactory) -> (Promise<Fiche>) in
                return Promise<Fiche>(in: .background, { resolve, reject, _ in
                    if let fiche: Fiche = factory.getFiche() {
                        resolve(fiche)
                    } else {
                        reject(APIError.decode(what: "Fiche"))
                    }
                })
            })
    }

    static func getFiche(with id: String) -> Promise<Fiche> {
        return ApiRequest.request(object: FicheRouter.getFiche(id: id), type: FicheFactory.self)
        .then(in: .background, { (factory: FicheFactory) -> (Promise<Fiche>) in
            return Promise<Fiche>(in: .background, { resolve, reject, _ in
                if let fiche: Fiche = factory.getFiche() {
                    resolve(fiche)
                } else {
                    reject(APIError.decode(what: "Fiche"))
                }
            })
        })
    }

    static func getFichePlaylists(with id: String) -> Promise<[Playlist]> {
        return ApiRequest.request(object: FicheRouter.getRelatedPlaylists(id: id), type: PlaylistArray.self)
        .then(in: .background, { (playlistArray: PlaylistArray) -> (Promise<[Playlist]>) in
            return Promise<[Playlist]>(in: .background, { resolve, _, _ in
                let playlists: [Playlist] = playlistArray.playlists
                resolve(playlists)
            })
        })
    }

    static func postToFicheFeed(with id: String, description: String, image: UIImage, userId: String) -> Promise<Feed> {

        let parameters: [String: String] = ["description": description,
                                            "created_by": userId]

        return ApiRequest.requestMultipartImage(object: FicheRouter.postToFicheFeed(id: id, description: description, userId: userId), parameters: parameters, image: image).then(in: .background, { (json: JSON) -> (Promise<Feed>) in
            return Promise<Feed>(in: .background, { resolve, reject, _ in
                if let feed: Feed = Feed(json: json) {
                    resolve(feed)
                } else {
                    reject(APIError.decode(what: "Feed"))
                }
            })
        })
    }
}

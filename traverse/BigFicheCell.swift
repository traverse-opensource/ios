//
//  BigFicheCell.swift
//  traverse
//
//  Created by Mattia Gustarini on 28.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class BigFicheCell: UICollectionViewCell {
    @IBOutlet weak var topBorderView: UIView!
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var internalCircleView: UIView!
    @IBOutlet weak var ficheIconImage: UIImageView!
    @IBOutlet weak var ficheTitleLabel: UILabel!
    @IBOutlet weak var bottomLineView: UIView!
    @IBOutlet weak var counterView: UIView!
    @IBOutlet weak var counterLabel: UILabel!

    private var fiche: Fiche!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(fiche: Fiche, index: Int, last: Bool) {
        self.fiche = fiche

        resetView()

        self.ficheTitleLabel.text = self.fiche.name

        FicheCellUtils.setFicheView(fiche: self.fiche,
                                    index: index,
                                    ficheIconImage: self.ficheIconImage,
                                    internalCircleView: self.internalCircleView,
                                    counterView: self.counterView,
                                    counterLabel: self.counterLabel)

        if index == 0 {
            self.topLineView.isHidden = true
            self.topBorderView.isHidden = true
        } else if last {
            self.bottomLineView.isHidden = true
        }
    }

    func setAsSeen() {
        FicheCellUtils.setAsSeen(fiche: self.fiche,
                                 ficheIconImage: self.ficheIconImage,
                                 internalCircleView: self.internalCircleView)
    }

    private func resetView() {
        self.topLineView.isHidden = false
        self.topBorderView.isHidden = false
        self.bottomLineView.isHidden = false
    }

    private func colorIcon(_ color: UIColor) {
        let tempImage: UIImage = (self.ficheIconImage.image?.withRenderingMode(.alwaysTemplate))!
        self.ficheIconImage.image = tempImage
        self.ficheIconImage.tintColor = color
    }
}

//
//  TypeFilterCell.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 06.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class TypeFilterCell: UITableViewCell {

    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var toggleImage: UIImageView!
    @IBOutlet weak var emptySelectView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        toggleImage.isHidden = !selected
        emptySelectView.isHidden = selected
    }

    func set(_ item: ItemType) {
        typeLabel.text = getFicheTypeLabel(item)
        typeImage.image = UIView.getFicheTypeImage(ficheType: item)
    }

    func getFicheTypeLabel(_ item: ItemType) -> String {
        switch item {
        case .events:
            return NSLocalizedString("Events", comment: "")
        case .people:
            return NSLocalizedString("People", comment: "")
        case .objects:
            return NSLocalizedString("Objects", comment: "")
        case .media:
            return NSLocalizedString("Medias", comment: "")
        default:
            return NSLocalizedString("Sites", comment: "")
        }
    }
}

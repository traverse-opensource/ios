//
//  FicheTabController.swift
//  traverse
//
//  Created by Mattia Gustarini on 08.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Material

class FicheTabController: TabsController, TabBarDelegate {
    open override func prepare() {
        super.prepare()

        self.tabBarAlignment = .top
        self.tabBar.lineColor = UIColor.textColor
        self.tabBar.lineAlignment = .bottom
        self.tabBar.backgroundColor = UIColor.primaryColor

        self.tabBar.delegate = self
        
        self.automaticallyAdjustsScrollViewInsets = false
    }

    func tabBar(tabBar: TabBar, willSelect tabItem: TabItem) {


        for tabBarItem in tabBar.tabItems {
            tabBarItem.backgroundColor = UIColor.primaryColor
        }

        tabItem.backgroundColor = UIColor.backgroundDefaultColor
    }

}

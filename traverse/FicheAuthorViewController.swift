//
//  FicheAuthorViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 10.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

private let FicheAuthorViewControllerNib: String = "FicheAuthorViewController"

class FicheAuthorViewController: UIViewController {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var topBar: UIView!
    @IBAction func moreButtonAction(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alertController.view.tintColor = UIColor.textColor
        let backView = alertController.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 24.0
        backView?.backgroundColor = UIColor.backgroundDefaultColor

        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel) { _ in
            // ...
        }
        alertController.addAction(cancelAction)

        let editAction = UIAlertAction(title: NSLocalizedString("Edit playlist", comment: "Edit playlist"), style: .default) { _ in
            if self.playlist != nil {
                let playlistSettingsController: PlaylistSettingsViewController = PlaylistSettingsViewController(playlist: self.playlist!, container: self.container!)
                self.present(playlistSettingsController, animated: true)
            }
        }
        alertController.addAction(editAction)

        let editFichesAction = UIAlertAction(title: NSLocalizedString("Edit fiches", comment: "Edit fiches"), style: .default) { _ in
            if self.playlist != nil {
                let playlistFicheOrderingViewController: PlaylistFicheOrderingViewController = PlaylistFicheOrderingViewController(playlist: self.playlist!, container: self.container!)
                self.present(playlistFicheOrderingViewController, animated: true)
            }
        }
        alertController.addAction(editFichesAction)

        let destroyAction = UIAlertAction(title: NSLocalizedString("Delete playlist", comment: "Delete playlist"), style: .destructive) { _ in
            let deleteAlert = UIAlertController(title: "Traverse", message: NSLocalizedString("Delete the playlist? This action cannot be undone.", comment: "Delete the playlist? This action cannot be undone."), preferredStyle: UIAlertControllerStyle.alert)

            deleteAlert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel, handler: { (_) in
                deleteAlert.dismiss(animated: true, completion: nil)
            }))
            deleteAlert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: "Delete"), style: .destructive, handler: { (_) in

                PlaylistAPI.deletePlaylist(id: (self.playlist?.id)!).then({ (_) in
                    Playlist.removeFromMyPlaylistLocally(self.playlist!)
                    deleteAlert.dismiss(animated: true, completion: nil)
                    self.container?.navigationController?.popViewController(animated: true)

                }).catch({ (_) -> Void in

                    deleteAlert.dismiss(animated: true, completion: {
                        self.showError(NSLocalizedString("Delete failed, please try again", comment: "Delete failed"))
                    })
                })

            }))

            self.present(deleteAlert, animated: true, completion: nil)
        }
        alertController.addAction(destroyAction)

        self.present(alertController, animated: true) {
            // ...
        }
    }

    private let container: UIViewController?
    private let playlist: Playlist?
    private let userProfile: UserProfile
    private let showMenu: Bool
    private let showTopBar: Bool

    init(userProfile: UserProfile, playlist: Playlist? = nil,
         showMenu: Bool = true, showTopBar: Bool = false,
         container: UIViewController? = nil) {
        self.userProfile = userProfile
        self.showMenu = showMenu
        self.playlist = playlist
        self.container = container
        self.showTopBar = showTopBar
        super.init(nibName: FicheAuthorViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.userImage.image = #imageLiteral(resourceName: "default_contributor")
        if self.showTopBar {
            self.topBar.isHidden = false
        }
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func setUpView(_ showTopBar: Bool = false) {
        self.menuButton.isHidden = !self.showMenu
        self.usernameLabel.text = self.userProfile.name
        if let contributorImageUrl: URL = self.userProfile.getPictureUrl() {
            userImage.hnk_setImageFromURL(contributorImageUrl)
        } else {
            userImage.image = #imageLiteral(resourceName: "default_contributor")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  MapCluster.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 25.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

@objc class MapCluster: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String!
    var item: ItemEntity?
    var isInPlaylist: Bool = false
    var itemIndex: Int?

    init(position: CLLocationCoordinate2D, name: String, item: ItemEntity) {
        self.position = position
        self.name = name
        self.item = item
    }

    convenience init(position: CLLocationCoordinate2D, name: String, item: ItemEntity, isInPlaylist: Bool, itemIndex: Int) {
        self.init(position: position, name: name, item: item)
        self.isInPlaylist = isInPlaylist
        self.itemIndex = itemIndex
    }

    public func getMarkerImage() -> UIView {
        if isInPlaylist {
            let iconView = PlaylistMarkerImage.instanceFromNib()
            iconView.set(fiche: item as! Fiche, index: itemIndex!)

            return iconView
        }
        else {
            let iconView = MarkerImage.instanceFromNib()

            UIView.setFicheTypeImage(iconView.itemTypeImage, fiche: item as! Fiche)
            UIView.setTheme(fiche: item as! Fiche,
                            iconView: iconView.itemBackground)
            return iconView
        }
    }
}

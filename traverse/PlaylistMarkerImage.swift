//
//  PlaylistMarkerImage.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 21.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class PlaylistMarkerImage: UIView {

    @IBOutlet weak var internalCircleView: UIView!
    @IBOutlet weak var ficheIconImage: UIImageView!
    @IBOutlet weak var counterView: UIView!
    @IBOutlet weak var counterLabel: UILabel!

    var fiche: Fiche!

    class func instanceFromNib() -> PlaylistMarkerImage {
        return UINib(nibName: "PlaylistMarkerImage", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PlaylistMarkerImage
    }

    func set(fiche: Fiche, index: Int) {
        self.fiche = fiche
        FicheCellUtils.setFicheView(fiche: self.fiche,
                                    index: index,
                                    ficheIconImage: self.ficheIconImage,
                                    internalCircleView: self.internalCircleView,
                                    counterView: self.counterView,
                                    counterLabel: self.counterLabel)
    }

//    private func colorIcon(_ color: UIColor) {
//        let tempImage: UIImage = (self.ficheIconImage.image?.withRenderingMode(.alwaysTemplate))!
//        self.ficheIconImage.image = tempImage
//        self.ficheIconImage.tintColor = color
//    }

}

//
//  Feed.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 28.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class Feed: MongoEntity {

    enum FeedType: String {
        case image = "feed_image"
        case video = "feed_video"
    }

    enum FeedKind: String {
        case facebook
        case instagram
        case twitter
        case traverse
    }

    static let TYPE: String = "type"
    static let NAME: String = "name"
    static let DESCRIPTION: String = "description"
    static let PERMALINK: String = "permalink"
    static let KIND: String = "kind"
    static let CREATED_AT: String = "created_at"
    static let COVER: String = "cover"
    static let VIDEO: String = "video"
    static let CREATED_BY: String = "created_by"

    var type: FeedType?
    var name: String?

    var description: String?
    var permalink: String?
    let kind: FeedKind
    let createdAt: Date
    var cover: Cover?
    var video: Cover?
    var createdBy: FeedUser?

    required init?(json: JSON) {

        self.type = Feed.TYPE <~~ json
        self.name = Feed.NAME <~~ json
        self.description = Feed.DESCRIPTION <~~ json
        self.permalink = Feed.PERMALINK <~~ json
        self.kind = (Feed.KIND <~~ json)!

        if let _: String = Feed.CREATED_AT <~~ json {
            let dateFormatter: DateFormatter = MongoEntity.getDateFormatter()
            self.createdAt = Decoder.decode(dateForKey: Feed.CREATED_AT, dateFormatter: dateFormatter)(json)!
        } else {
            self.createdAt = Date()
        }

        self.cover = Feed.COVER <~~ json
        self.video = Feed.VIDEO <~~ json
        self.createdBy = Feed.CREATED_BY <~~ json

        super.init(json: json)
    }
}

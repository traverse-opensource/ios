//
//  UserAPI.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Hydra
import Gloss
import TwitterKit
import GoogleSignIn
import FBSDKLoginKit

class UserAPI {

    static func getCurrentUser() -> User? {
        if UserDefaults.standard.bool(forKey: Const.IS_USER_LOGGED_IN_KEY) {
            if let jsonData = UserDefaults.standard.dictionary(forKey: Const.TRAVERSE_AUTH_USER_KEY) {
                return User(json: jsonData)
            }
        }
        return nil
    }

    static func syncFavorites(_ user: User) -> Promise<User> {
        var favArray: [String] = []
        if let userFavs = UserDefaults.standard.array(forKey: Const.FAVORITES_PLAYLIST_KEY) as? [String] {
            favArray = userFavs
        }

        return ApiRequest.request(object: UserRouter.syncFavorites(id: user.id, favorites: favArray), type: User.self)
                .then(in: .background, { (user: User) -> (Promise<User>) in
                    return Promise<User>(in: .background, { resolve, _, _ in
                        resolve(user)
                    })
                })
    }
//
//    static func syncFavorites(_ user: User) {
//        if let favFromServer: [String] = user.favorites {
//
//            var favArray = UserDefaults.standard.array(forKey: Const.FAVORITES_PLAYLIST_KEY)
//
//            favFromServer.forEach { (fav) in
//                var found: Bool = false
//
//                if favArray != nil {
//                    for key in favArray as! [String] {
//                        if key == fav {
//                            found = true
//                            break
//                        }
//                    }
//                }
//
//                if !found {
//                    favArray?.append(fav)
//                }
//            }
//
//            UserDefaults.standard.set(favArray, forKey: Const.FAVORITES_PLAYLIST_KEY)
//            user.favorites = favArray as! [String]
//        }
//        
//    }

    static func logout() {

        //TWITTER
        let store = Twitter.sharedInstance().sessionStore

        if let userID = store.session()?.userID {
            store.logOutUserID(userID)
        }
        UserDefaults.standard.removeObject(forKey: Const.TWITTER_USER_ID_KEY)
        UserDefaults.standard.removeObject(forKey: Const.TWITTER_USER_EMAIL_KEY)
        UserDefaults.standard.removeObject(forKey: Const.TWITTER_USER_NAME_KEY)

        //GOOGLE
        GIDSignIn.sharedInstance().signOut()

        UserDefaults.standard.removeObject(forKey: Const.GOOGLE_USER_CLIENT_ID_KEY)
        UserDefaults.standard.removeObject(forKey: Const.GOOGLE_USER_TOKEN_KEY)
        UserDefaults.standard.removeObject(forKey: Const.GOOGLE_USER_EMAIL_KEY)
        UserDefaults.standard.removeObject(forKey: Const.GOOGLE_USER_NAME_KEY)

        //FACEBOOK
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        UserDefaults.standard.removeObject(forKey: Const.FACEBOOK_USER_ID_KEY)
        UserDefaults.standard.removeObject(forKey: Const.FACEBOOK_USER_EMAIL_KEY)
        UserDefaults.standard.removeObject(forKey: Const.FACEBOOK_USER_NAME_KEY)

        //CUSTOM
        UserDefaults.standard.removeObject(forKey: Const.TRAVERSE_USER_NAME_KEY)
        UserDefaults.standard.removeObject(forKey: Const.TRAVERSE_USER_ID_KEY)
        UserDefaults.standard.removeObject(forKey: Const.TRAVERSE_USER_EMAIL_KEY)
        
        //STANDARD
        UserDefaults.standard.removeObject(forKey: Const.LOGIN_METHOD)
        UserDefaults.standard.removeObject(forKey: Const.TRAVERSE_AUTH_USER_KEY)
        UserDefaults.standard.set(false, forKey: Const.IS_USER_LOGGED_IN_KEY)
    }

    static func customSignUp(email: String, password: String, name: String) -> Promise<User> {
        return ApiRequest.request(object: UserRouter.customSignUp(email: email, password: password, name: name), type: User.self)
            .then(in: .background, { (user: User) -> (Promise<User>) in
                return Promise<User>(in: .background, { resolve, _, _ in
                    resolve(user)
                })
            })
    }

    static func signupFromSN(kind: String, userId: String, userName: String, email: String, picture: String) -> Promise<User> {
        return ApiRequest.request(object: UserRouter.signupFromSN(kind: kind, userId: userId, userName: userName, email: email, picture: picture), type: User.self)
            .then(in: .background, { (user: User) -> (Promise<User>) in
                return Promise<User>(in: .background, { resolve, _, _ in
                    resolve(user)
                })
            })
    }

    static func login(email: String, password: String) -> Promise<User> {
        return ApiRequest.request(object: UserRouter.login(email: email, password: password), type: User.self)
            .then(in: .background, { (user: User) -> (Promise<User>) in
                return Promise<User>(in: .background, { resolve, _, _ in
                    resolve(user)
                })
            })
    }

    static func loginFromSN(kind: String, userId: String, userName: String) -> Promise<User> {
        return ApiRequest.request(object: UserRouter.loginFromSN(kind: kind, userId: userId, userName: userName), type: User.self)
            .then(in: .background, { (user: User) -> (Promise<User>) in
                return Promise<User>(in: .background, { resolve, _, _ in
                    resolve(user)
                })
            })
    }

    static func getUsers(page: Int, limit: Int) -> Promise<[User]> {
        return ApiRequest.request(object: UserRouter.getUsers(page: page, limit: limit), type: UserArray.self)
            .then(in: .background, { (userArray: UserArray) -> (Promise<[User]>) in
                return Promise<[User]>(in: .background, { resolve, _, _ in
                    resolve(userArray.users)
                })
            })
    }

    static func getSuggestions() -> Promise<[ItemEntity]> {
        return ApiRequest.request(array: UserRouter.getSuggestions(), type: MixFactory.self)
            .then(in: .background, { (factories: [MixFactory]) -> (Promise<[ItemEntity]>) in
                return Promise<[ItemEntity]>(in: .background, { resolve, reject, _ in
                    var items: [ItemEntity] = []
                    for factory: MixFactory in factories {
                        if let item: ItemEntity = factory.getItem() {
                            items.append(item)
                        } else {
                            reject(APIError.decode(what: "Mixin"))
                        }
                    }
                    resolve(items)
                })
            })
    }

    static func getSuggestions(with maxDistance: Int,
                               latitude: Double,
                               longitude: Double,
                               page: Int,
                               limit: Int,
                               includePlaylist: Bool) -> Promise<[ItemEntity]> {
        return ApiRequest.request(array: UserRouter.getSuggestionsWithParams(
            maxDistance: maxDistance,
            latitude: latitude,
            longitude: longitude,
            page: page,
            limit: limit, includePlaylist: includePlaylist), type: MixFactory.self)
            .then(in: .background, { (factories: [MixFactory]) -> (Promise<[ItemEntity]>) in
                return Promise<[ItemEntity]>(in: .background, { resolve, reject, _ in
                    var items: [ItemEntity] = []
                    for factory: MixFactory in factories {
                        if let item: ItemEntity = factory.getItem() {
                            items.append(item)
                        } else {
                            reject(APIError.decode(what: "Mixin"))
                        }
                    }
                    resolve(items)
                })
            })
    }

    static func getMyPlaylists(userId: String) -> Promise<[Playlist]> {
        return ApiRequest.request(object: UserRouter.getMyPlaylists(userId: userId), type: PlaylistArray.self).then(in: .background, { (playlistArray: PlaylistArray) -> (Promise<[Playlist]>) in
                return Promise<[Playlist]>(in: .background, { resolve, reject, _ in
                    resolve(playlistArray.playlists)
                })
        })
    }
}

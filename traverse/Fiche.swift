//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class Fiche: ItemEntity {
    static let RELATED_FICHES: String = "related_fiches"
    static let THEMES: String = "themes"
    static let REFERENCES: String = "references"
    static let SOCIAL: String = "social"
    static let CITY: String = "city"
    static let COUNTRY: String = "country"
    static let LATITUDE: String = "latitude"
    static let LONGITUDE: String = "longitude"
    static let MONGO_LOCATION: String = "location"
    static let GOOGLE_MAP_LOCATION: String = "map"
    static let LONG_DESCRIPTION: String = "long_description"
    static let SHORT_DESCRIPTION: String = "short_description"

    private var relatedFiches: [FicheFactory]?
    var relatedFichesIds: [String]?
    let themes: [ItemTheme]
    var references: String?
    var socialHolder: SocialHolder?
    var city: String?
    var country: String?
    var latitude: Double?
    var longitude: Double?
    var mongoLocation: MongoLocation?
    var googleMapLocation: GoogleMapLocation?
    var longDescription: String?
    var shortDescription: String?

    required init?(json: JSON) {
        if let relatedFiches: [FicheFactory] = Fiche.RELATED_FICHES <~~ json {
            self.relatedFiches = relatedFiches
        } else {
            if let relatedFichesIds: [String] = Fiche.RELATED_FICHES <~~ json {
                self.relatedFichesIds = relatedFichesIds
            } else {
                self.relatedFichesIds = []
            }
        }

        guard let themes: [ItemTheme] = Fiche.THEMES <~~ json
            else {
                return nil
        }

        self.themes = themes
        self.references = Fiche.REFERENCES <~~ json
        self.socialHolder = Fiche.SOCIAL <~~ json
        self.city = Fiche.CITY <~~ json
        self.country = Fiche.COUNTRY <~~ json
        self.latitude = Fiche.LATITUDE <~~ json
        self.longitude = Fiche.LONGITUDE <~~ json
        self.mongoLocation = Fiche.MONGO_LOCATION <~~ json
        self.googleMapLocation = Fiche.GOOGLE_MAP_LOCATION <~~ json
        //let type: ItemType = (ItemEntity.TYPE <~~ json)!

        self.shortDescription = Fiche.SHORT_DESCRIPTION <~~ json
        self.longDescription = Fiche.LONG_DESCRIPTION <~~ json
        
        super.init(json: json)
    }

    func isIdsOnly() -> Bool {
        return self.relatedFiches == nil && self.relatedFichesIds != nil
    }

    func getRelatedFiches() -> [Fiche]? {
        if !self.isIdsOnly() {
            let fiches: [Fiche] = (self.relatedFiches?.map({ (factory: FicheFactory) -> Fiche in
                factory.getFiche()!
            }))!
            return fiches
        } else {
            return nil
        }
    }

    func toJSON() -> JSON? {
        switch type {
        case .events:
            return (self as! FicheEvent).toJSON()
        case .media:
            return (self as! FicheMedia).toJSON()
        case .objects:
            return (self as! FicheObject).toJSON()
        case .places:
            return (self as! FichePlace).toJSON()
        case .people:
            return (self as! FichePeople).toJSON()
        default:
            return [:]
        }
    }
}

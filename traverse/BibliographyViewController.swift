//
//  BibliographyViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 22.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

private let BibliographyViewControllerNib: String = "BibliographyViewController"

class BibliographyViewController: UIViewController {
    @IBOutlet weak var biblioTextView: UITextView!
    private let ficheBiblio: String

    init(_ ficheBiblio: String) {
        self.ficheBiblio = ficheBiblio
        super.init(nibName: BibliographyViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func setUpView() {

        do {
            let attrStr = try NSMutableAttributedString(
                data: self.ficheBiblio.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                documentAttributes: nil)
            var attributes: [String: Any] = attrStr.attributes(at: 0, effectiveRange: nil)
            attributes[NSFontAttributeName] = UIFont(name: "Roboto-Regular", size: 14.0)
            attributes[NSForegroundColorAttributeName] = UIColor.textColor
            attrStr.setAttributes(attributes, range: NSRange(location: 0, length: attrStr.length))
            self.biblioTextView.attributedText = attrStr
        } catch {
            self.biblioTextView.text = self.ficheBiblio
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

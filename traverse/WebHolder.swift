//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class WebHolder: Gloss.Decodable {
    static let LINK: String = "link"

    var link: String?

    required init?(json: JSON) {
//        guard let link: String = WebHolder.LINK <~~ json
//            else {
//                return nil
//        }

        self.link = WebHolder.LINK <~~ json
    }
}

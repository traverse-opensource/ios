//
//  FiltersViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 05.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Gloss

class FiltersViewController: UIViewController {

    private static let UnwindSegueIdentifier: String = "unwindToExplore"

    static let SegueToTypeFilter: String = "segueToType"
    static let SegueToThemeFilter: String = "segueToTheme"
    static let SegueToCategoryFilter: String = "segueToCategory"
    static let SegueToTagFilter: String = "segueToTag"

    @IBOutlet weak var typeFilterView: UIView!
    @IBOutlet weak var themeFilterView: UIView!
    @IBOutlet weak var categoryFilterView: UIView!
    @IBOutlet weak var tagViewController: UIView!

    @IBOutlet weak var typeFilterLabel: UILabel!
    @IBOutlet weak var themeFilterLabel: UILabel!
    @IBOutlet weak var categoryFilterLabel: UILabel!
    @IBOutlet weak var tagFilterLabel: UILabel!

    @IBOutlet weak var removeFilterButton: UIButton!
    @IBOutlet weak var showResultsButton: UIButton!

    @IBOutlet weak var typeFilterHolderView: UIView!
    @IBOutlet weak var themeFilterHolderView: UIView!
    @IBOutlet weak var categoryFilterHolderView: UIView!
    @IBOutlet weak var tagFilterHolderView: UIView!

    @IBOutlet weak var filterHistoryStackView: UIStackView!

    var activeFilters: Filter?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic-enr-in"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(FiltersViewController.addCurrentFilterToHistory))

        showResultsButton.backgroundColor = UIColor.accentColor
        removeFilterButton.backgroundColor = UIColor.accentColor

        typeFilterView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FiltersViewController.displayTypeFilter)))
        themeFilterView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FiltersViewController.displayThemeFilter)))
        categoryFilterView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FiltersViewController.displayCategoryFilter)))
        tagViewController.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FiltersViewController.displayTagFilter)))

        self.updateFiltersViews()
        self.updateFilterHistoryView()
    }

    @IBAction func unwindFromFilterView(segue: UIStoryboardSegue) {
        self.updateFiltersViews()
    }

    @IBAction func cleanFilters(_ sender: Any) {
        activeFilters = Filter()
        self.updateFiltersViews()
    }

    func updateFiltersViews() {
        typeFilterLabel.text = String(format: NSLocalizedString("%d type(s)", comment: ""), (activeFilters?.types.count)!)
        themeFilterLabel.text = String(format: NSLocalizedString("%d theme(s)", comment: ""), (activeFilters?.themes.count)!)
        categoryFilterLabel.text = String(format: NSLocalizedString("%d category(ies)", comment: ""), (activeFilters?.categories.count)!)
        tagFilterLabel.text = String(format: NSLocalizedString("%d tag(s)", comment: ""), (activeFilters?.tags.count)!)

        typeFilterHolderView.isHidden = (activeFilters?.types.isEmpty)!
        themeFilterHolderView.isHidden = (activeFilters?.themes.isEmpty)!
        categoryFilterHolderView.isHidden = (activeFilters?.categories.isEmpty)!
        tagFilterHolderView.isHidden = (activeFilters?.tags.isEmpty)!
    }

    func updateFilterHistoryView() {
        let history = UserDefaults.standard.array(forKey: Const.FILTER_HISTORY)
        var index: Int = 0

        history?.forEach({ (filterItem) in
            let filter = Filter(json: filterItem as! JSON)
            if filter != nil {
                addFilterHistoryView(filter!, index: index)
                index += 1
            }
        })
    }

    func applyFilter(_ sender: UITapGestureRecognizer) {
        var history = UserDefaults.standard.array(forKey: Const.FILTER_HISTORY)
        self.activeFilters = Filter(json: history?[(sender.view?.tag)!] as! JSON)

        self.updateFiltersViews()
    }

    func removeFilter(_ sender: UITapGestureRecognizer) {
        var history = UserDefaults.standard.array(forKey: Const.FILTER_HISTORY)
        history?.remove(at: (sender.view?.tag)!)
        UserDefaults.standard.set(history, forKey: Const.FILTER_HISTORY)

        self.filterHistoryStackView.subviews.forEach { (view) in
            self.filterHistoryStackView.removeArrangedSubview(view)
            view.removeFromSuperview()
        }

        self.updateFilterHistoryView()
    }

    func addFilterHistoryView(_ filter: Filter, index: Int) {

        let filterHistoryViewController = FilterHistoryViewController()
        filterHistoryViewController.set(filter, index: index)

        self.addChildViewController(filterHistoryViewController)
        filterHistoryViewController.didMove(toParentViewController: self)

        self.filterHistoryStackView.insertArrangedSubview(filterHistoryViewController.view, at: 0)
    }

    func addCurrentFilterToHistory() {

        if !(activeFilters?.types.isEmpty)! || !(activeFilters?.themes.isEmpty)! || !(activeFilters?.categories.isEmpty)! || !(activeFilters?.tags.isEmpty)! {

            var history = UserDefaults.standard.array(forKey: Const.FILTER_HISTORY)
            if history == nil {
                history = []
            }

            history?.append((self.activeFilters?.toJSON())!)
            UserDefaults.standard.set(history, forKey: Const.FILTER_HISTORY)

            addFilterHistoryView(self.activeFilters!, index: (history?.count)! - 1)
            self.showMessage(NSLocalizedString("Filter saved!", comment: "Filter saved!"))
        }

    }

    @IBAction func onShowResults(_ sender: Any) {
        self.performSegue(withIdentifier: FiltersViewController.UnwindSegueIdentifier, sender: self)
    }

    func displayTypeFilter() {
        self.performSegue(withIdentifier: FiltersViewController.SegueToTypeFilter, sender: self)
    }

    func displayThemeFilter() {
        self.performSegue(withIdentifier: FiltersViewController.SegueToThemeFilter, sender: self)
    }

    func displayCategoryFilter() {
        self.performSegue(withIdentifier: FiltersViewController.SegueToCategoryFilter, sender: self)
    }

    func displayTagFilter() {
        self.performSegue(withIdentifier: FiltersViewController.SegueToTagFilter, sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? FilterViewController {
            destination.set(filter: activeFilters!)
        }
        else if let destination = segue.destination as? ExploreViewController {
            destination.activeFilters = activeFilters!
        }
    }
}

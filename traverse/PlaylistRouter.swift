//
//  PlaylistRouter.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Alamofire

enum PlaylistRouter: URLRequestConvertible {
    case getPlaylists()
    case getPlaylist(id: String)
    case likePlaylist(id: String, userId: String)
    case dislikePlaylist(id: String, userId: String)
    case createPlaylist(name: String,
        description: String,
        createdBy: String,
        photoUrl: String,
        ficheId: String,
        tags: [String],
        categories: [String],
        subCategories: [String],
        status: String)
    case addFiche(playlistId: String, ficheId: String)
    case updatePlaylist(id: String,
        name: String,
        description: String,
        createdBy: String,
        photoUrl: String,
        tags: [String],
        categories: [String],
        subCategories: [String],
        status: String)
    case deletePlaylist(id: String)
    case swapFiches(id: String, ficheIds: [String])

    var method: HTTPMethod {
        switch self {
        case .getPlaylists:
            return .get
        case .getPlaylist:
            return .get
        case .dislikePlaylist:
            return .put
        case .likePlaylist:
            return .put
        case .createPlaylist:
            return .post
        case .addFiche:
            return .post
        case .updatePlaylist:
            return .post
        case .deletePlaylist:
            return .delete
        case .swapFiches:
            return .post
        }
    }

    var path: String {
        switch self {
        case .getPlaylists:
            return "/playlists"
        case .getPlaylist(let id):
            return "/playlists/\(id)"
        case .likePlaylist(let id, _):
            return "/playlists/\(id)/like"
        case .dislikePlaylist(let id, _):
            return "/playlists/\(id)/dislike"
        case .createPlaylist:
            return "/playlists"
        case .addFiche(let id, _):
            return "/playlists/\(id)/addLink"
        case .updatePlaylist(let id, _, _, _, _, _, _, _, _):
            return "/playlists/\(id)/fullUpdate"
        case .deletePlaylist(let id):
            return "/playlists/\(id)"
        case .swapFiches(let id, _):
            return "/playlists/\(id)/swap"
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Const.API_URL.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        // if arguments are needed
        var parameters: Parameters!
        switch self {
        case .likePlaylist(_, let userId):
            parameters = ["objectId": userId]
        case .dislikePlaylist(_, let userId):
            parameters = ["objectId": userId]
        case .createPlaylist(let name,
                             let description,
                             let createdBy,
                             let photoUrl,
                             let ficheId,
                             let tags,
                             let categories,
                             let subCategories,
                             let status):
            let labels: [String: Any] = ["tags": tags,
                                         "categories": categories,
                                         "sousCategories": subCategories]
            parameters = ["name": name,
                          "description": description,
                          "created_by": createdBy,
                          "photoUrl": photoUrl,
                          "ficheId": ficheId,
                          "labels": labels,
                          "status": status]
        case .addFiche(_, let ficheId):
            parameters = ["ficheId": ficheId, "linkValue": ""]
        case .updatePlaylist(_,
                             let name,
                             let description,
                             let createdBy,
                             let photoUrl,
                             let tags,
                             let categories,
                             let subCategories,
                             let status):
            let labels: [String: Any] = ["tags": tags,
                                         "categories": categories,
                                         "sousCategories": subCategories]
            parameters = ["name": name,
                          "description": description,
                          "created_by": createdBy,
                          "photoUrl": photoUrl,
                          "labels": labels,
                          "status": status]
        case .swapFiches(_, let ficheIds):
            parameters = ["ficheIds": ficheIds]
        default:
            break
        }

        if parameters != nil {
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }

        return urlRequest
    }
}

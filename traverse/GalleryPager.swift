//
//  GalleryPager.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 18.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import ESTabBarController_swift

class GalleryPager: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    private let medias: [FicheMedia]
    private var selectedIndex: Int = 0

    private var pageViewControllers: [GalleryPageViewController] = []

    init(medias: [FicheMedia], selectedIndex: Int) {
        self.medias = medias
        self.selectedIndex = selectedIndex
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal)
        var i: Int = 0
        for media in medias {
            let pageController: GalleryPageViewController = GalleryPageViewController()
            pageController.set(media, position: i, pager: self)
            i += 1

            pageViewControllers.append(pageController)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let imageView = UIImageView(image: #imageLiteral(resourceName: "ic_grid"))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        imageView.frame = titleView.bounds
        titleView.addSubview(imageView)
        titleView.isUserInteractionEnabled = true
        self.navigationItem.titleView = titleView

        self.navigationItem.backBarButtonItem?.title = ""

        let openGalleryGrid: UITapGestureRecognizer = UITapGestureRecognizer()
        openGalleryGrid.addTarget(self, action: #selector(GalleryPager.openGalleryGrid))
        titleView.addGestureRecognizer(openGalleryGrid)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "\(selectedIndex + 1)/\(medias.count)", style: .plain, target: self, action: nil)

        self.view.backgroundColor = UIColor.backgroundDefaultColor
        self.dataSource = self
        self.delegate = self

        let firstViewController = pageViewControllers[selectedIndex]
        setViewControllers([firstViewController],
                            direction: .forward,
                            animated: true,
                            completion: nil)
    }

    func openGalleryGrid() {
        let gallery = GalleryGridViewController(medias: medias, pager: self)
        self.navigationController?.pushViewController(gallery, animated: false)
    }

    func gotoPage(index: Int) {
        let selectedViewController = pageViewControllers[index]

        setViewControllers([selectedViewController],
                           direction: .reverse,
                           animated: false,
                           completion: nil)

        self.navigationItem.rightBarButtonItem?.title = "\(selectedViewController.position + 1)/\(medias.count)"

    }

    func previousPage() {
        guard let currentViewController = self.viewControllers?.first else { return }
        if let previous = self.pageViewController(self, viewControllerBefore: currentViewController) as? GalleryPageViewController {
            setViewControllers([previous],
                               direction: .reverse,
                               animated: true,
                               completion: nil)

            self.navigationItem.rightBarButtonItem?.title = "\(previous.position + 1)/\(medias.count)"
        }
    }

    func nextPage() {
        guard let currentViewController = self.viewControllers?.first else { return }
        if let next = self.pageViewController(self, viewControllerAfter: currentViewController) as? GalleryPageViewController {
            setViewControllers([next],
                               direction: .forward,
                               animated: true,
                               completion: nil)

            self.navigationItem.rightBarButtonItem?.title = "\(next.position + 1)/\(medias.count)"
        }
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if pageViewControllers.isEmpty || pageViewControllers.count == 1 {
            return nil
        }
        if let page = viewController as? GalleryPageViewController {
            if page.position - 1 >= 0 {
                return pageViewControllers[page.position - 1]
            }
            else {
                return pageViewControllers[pageViewControllers.count - 1]
            }
        }
        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if pageViewControllers.isEmpty || pageViewControllers.count == 1 {
            return nil
        }
        if let page = viewController as? GalleryPageViewController {
            if page.position + 1 < pageViewControllers.count {
                return pageViewControllers[page.position + 1]
            }
            else {
                return pageViewControllers[0]
            }
        }

        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let page = pendingViewControllers[0] as? GalleryPageViewController {
            self.navigationItem.rightBarButtonItem?.title = "\(page.position + 1)/\(medias.count)"
        }
    }
}

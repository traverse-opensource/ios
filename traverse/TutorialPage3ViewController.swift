//
//  TutorialPage3ViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 20.11.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class TutorialPage3ViewController: UIViewController {

    @IBOutlet weak var titleImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(red: 63/255, green: 188/255, blue: 167/255, alpha: 1)
        titleImage.image = titleImage.image?.withRenderingMode(.alwaysTemplate).tint(with: UIColor.white)
    }
}


//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class Label: Gloss.Decodable {
    static let CATEGORIES: String = "categories"
    static let SUB_CATEGORIES: String = "sousCategories"
    static let TAGS: String = "tags"

    let categories: [Category]
    let subCategories: [String]
    let tags: [Tag]

    required init?(json: JSON) {
        guard let categories: [Category] = Label.CATEGORIES <~~ json,
            let subCategories: [String] = Label.SUB_CATEGORIES <~~ json,
            let tags: [Tag] = Label.TAGS <~~ json
            else {
                return nil
        }

        self.categories = categories
        self.subCategories = subCategories
        self.tags = tags
    }
}

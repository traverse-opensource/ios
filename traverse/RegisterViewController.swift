//
//  RegisterViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 30.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Alamofire
import Material

class RegisterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var fullnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!

    @IBOutlet weak var fullNameValidationImage: UIImageView!
    @IBOutlet weak var passwordValidationImage: UIImageView!
    @IBOutlet weak var emailValidationImage: UIImageView!
    @IBOutlet weak var repeatPasswordValidationImage: UIImageView!
    @IBOutlet weak var termsValidationImage: UIImageView!
    
    @IBOutlet weak var termsCheckbox: UIButton!
    @IBOutlet weak var cancelButton: UIImageView!

    @IBOutlet weak var createAccountButton: UIButton!

    var termsAccepted: Bool = false

    let snackbar = Snackbar()
    var parentNavigationController: UINavigationController?

    override func viewDidLoad() {
        super.viewDidLoad()

        fullNameValidationImage.isHidden = true
        emailValidationImage.isHidden = true
        passwordValidationImage.isHidden = true
        repeatPasswordValidationImage.isHidden = true
        termsValidationImage.isHidden = true

        fullnameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        repeatPasswordTextField.delegate = self

        createAccountButton.backgroundColor = UIColor.accentColor
        cancelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.onCancel)))
    }

    func onCancel() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func termsCheckboxClicked(_ sender: Any) {
        termsValidationImage.isHidden = true

        termsAccepted = !termsAccepted
        if termsAccepted {
            termsCheckbox.setImage(#imageLiteral(resourceName: "checkbox_checked"), for: .normal)
        }
        else {
            termsCheckbox.setImage(#imageLiteral(resourceName: "checkbox_unchecked"), for: .normal)
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case fullnameTextField:
            fullNameValidationImage.isHidden = true
            break
        case emailTextField:
            emailValidationImage.isHidden = true
            break
        case passwordTextField:
            passwordValidationImage.isHidden = true
            break
        case repeatPasswordTextField:
            repeatPasswordValidationImage.isHidden = true
            break
        default:
            break
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case fullnameTextField:
            if !(fullnameTextField.text?.isEmpty)! {
                fullNameValidationImage.image = #imageLiteral(resourceName: "field_validation_valid")
                fullNameValidationImage.isHidden = false
            }
            break
        case emailTextField:
            if (!(emailTextField.text?.isEmpty)!) && isValidEmail(email: emailTextField.text!) {
                emailValidationImage.image = #imageLiteral(resourceName: "field_validation_valid")
                emailValidationImage.isHidden = false
            }
            else if !isValidEmail(email: emailTextField.text!) {
                emailValidationImage.image = #imageLiteral(resourceName: "field_validation_error")
                emailValidationImage.isHidden = false

                self.showError(NSLocalizedString("The email provided is not valid", comment: "The email provided is not valid"), true)
            }
            break
        case passwordTextField:
            if !(passwordTextField.text?.isEmpty)! && (passwordTextField.text?.characters.count)! > 7 {
                passwordValidationImage.image = #imageLiteral(resourceName: "field_validation_valid")
                passwordValidationImage.isHidden = false
            }
            break
        case repeatPasswordTextField:
            if (!(repeatPasswordTextField.text?.isEmpty)!) && repeatPasswordTextField.text == passwordTextField.text {
                repeatPasswordValidationImage.image = #imageLiteral(resourceName: "field_validation_valid")
                repeatPasswordValidationImage.isHidden = false
            }
            else if repeatPasswordTextField.text != passwordTextField.text {
                repeatPasswordValidationImage.image = #imageLiteral(resourceName: "field_validation_error")
                repeatPasswordValidationImage.isHidden = false
            }
            break
        default:
            break
        }
    }

    @IBAction func createAccountButton(_ sender: Any) {
        if (fullnameTextField.text?.isEmpty)! {
            fullnameTextField.becomeFirstResponder()
            fullNameValidationImage.image = #imageLiteral(resourceName: "field_validation_error")
            fullNameValidationImage.isHidden = false
        }
        else if (emailTextField.text?.isEmpty)! || !isValidEmail(email: emailTextField.text!) {
            emailTextField.becomeFirstResponder()
            emailValidationImage.image = #imageLiteral(resourceName: "field_validation_error")
            emailValidationImage.isHidden = false
        }
        else if (passwordTextField.text?.isEmpty)! || (passwordTextField.text?.characters.count)! < 8 {
            passwordTextField.becomeFirstResponder()
            passwordValidationImage.image = #imageLiteral(resourceName: "field_validation_error")
            passwordValidationImage.isHidden = false
        }
        else if (repeatPasswordTextField.text?.isEmpty)! || repeatPasswordTextField.text != passwordTextField.text {
            repeatPasswordTextField.becomeFirstResponder()
            repeatPasswordValidationImage.image = #imageLiteral(resourceName: "field_validation_error")
            repeatPasswordValidationImage.isHidden = false
        }
        else if !termsAccepted {
            termsCheckbox.becomeFirstResponder()
            termsValidationImage.image = #imageLiteral(resourceName: "field_validation_error")
            termsValidationImage.isHidden = false
        }
        else {
            //ALL GOOD LET'S CREATE AN ACCOUNT
            UserAPI.customSignUp(email: emailTextField.text!, password: passwordTextField.text!, name: fullnameTextField.text!).then({ (user) in
                UserDefaults.standard.set(true, forKey: Const.IS_USER_LOGGED_IN_KEY)
                UserDefaults.standard.set(Const.CUSTOM_AUTH, forKey: Const.LOGIN_METHOD)
                UserDefaults.standard.set(user.id, forKey: Const.TRAVERSE_USER_ID_KEY)
                UserDefaults.standard.set(user.profile?.name, forKey: Const.TRAVERSE_USER_NAME_KEY)
                UserDefaults.standard.set(user.email, forKey: Const.TRAVERSE_USER_EMAIL_KEY)

                UserAPI.syncFavorites(user).then({ (updatedUser) in
                    UserDefaults.standard.set(updatedUser.toJSON(), forKey: Const.TRAVERSE_AUTH_USER_KEY)
                }).catch({ (_) -> (Void) in
                    UserDefaults.standard.set(user.toJSON(), forKey: Const.TRAVERSE_AUTH_USER_KEY)
                }).always(body: {
                    self.dismiss(animated: true, completion: {
                        self.parentNavigationController?.showMessage(NSLocalizedString("Login succeed!", comment: "Login succeed!"))
                    })
                })

            }).catch({ (error) -> (Void) in
                print(error.localizedDescription)

                if let afError = error as? Alamofire.AFError {
                    if afError.isResponseSerializationError {
                        let nsError = afError.underlyingError! as NSError
                        if let responseData = nsError.userInfo.valueForKeyPath(keyPath: "responseDataDump") as? Data {
                            do {
                                let errorJson = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments)
                                print(errorJson)
                                self.showError(NSLocalizedString("Error! An unknown error occurred!", comment: "Error! An unknown error occurred!"), true)
                            }
                            catch _ {
                                let errorMessage = String(data: responseData, encoding: .utf8)
                                if errorMessage != nil {
                                    self.showError(errorMessage!, true)
                                }
                                else {
                                    self.showGenericError(true)
                                }
                            }
                        }

                    }
                }
            })
        }
    }

    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}

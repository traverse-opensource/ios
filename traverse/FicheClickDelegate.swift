//
//  FicheClickDelegate.swift
//  traverse
//
//  Created by Mattia Gustarini on 08.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Foundation

protocol FicheClickDelegate: class {
    func onClick(fiche: Fiche)
}

//
//  SmallFicheCell.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class SmallFicheCell: UICollectionViewCell {

    @IBOutlet weak var internalCircleView: UIView!
    @IBOutlet weak var ficheIconImage: UIImageView!
    @IBOutlet weak var counterView: UIView!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var hereLabel: UILabel!

    private var fiche: Fiche!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(fiche: Fiche, index: Int, selected: Bool) {
        self.fiche = fiche
        FicheCellUtils.setFicheView(fiche: self.fiche,
                                    index: index,
                                    ficheIconImage: self.ficheIconImage,
                                    internalCircleView: self.internalCircleView,
                                    counterView: self.counterView,
                                    counterLabel: self.counterLabel,
                                    useUserDefault: false)
        if selected {
            setSelected()
        } else {
            self.hereLabel.isHidden = true
        }
    }

    private func setSelected() {
        FicheCellUtils.setAsSeen(fiche: self.fiche,
                                 ficheIconImage: self.ficheIconImage,
                                 internalCircleView: self.internalCircleView)
        self.hereLabel.isHidden = false
    }
}

//
//  MediaCell.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 16.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Haneke

class MediaCell: UICollectionViewCell {

    private var item: ItemEntity!
    @IBOutlet weak var image: UIImageView!

    @IBOutlet weak var selectedView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func set(item: ItemEntity) {
        self.item = item

        if let cover: Cover = item.cover {
            self.image.hnk_setImageFromURL(cover.getImageUrl())
        }
    }

    func getItem() -> ItemEntity {
        return self.item
    }

    func select() {
        self.selectedView.isHidden = false
    }

    func deselect() {
        self.selectedView.isHidden = true
    }
}

//
//  PlaylistSettingsViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Material

private let NibName: String = "PlaylistSettingsViewController"

class PlaylistSettingsViewController: UIViewController {

    @IBOutlet weak var cancelButton: UIImageView!
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var publicSwitch: UISwitch!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var labelsLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!

    private var playlist: Playlist!
    private var fiche: Fiche!
    private var mediasController: SelectMediaViewController!
    private var labelsController: SelectLabelsViewController!
    private var tagsController: SelectTagsViewController!

    private var container: UIViewController?

    // To call when adding to new playlist
    init(fiche: Fiche, container: UIViewController) {
        self.fiche = fiche
        self.container = container
        super.init(nibName: NibName, bundle: nil)
        setUpControllers()
    }

    // To call when operating on the existing playlist
    init(playlist: Playlist, container: UIViewController) {
        self.playlist = playlist
        self.container = container
        super.init(nibName: NibName, bundle: nil)
        setUpControllers()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.addSnackbar()

        // Do any additional setup after loading the view.
        self.publicSwitch.onTintColor = UIColor.accentColor

        self.cancelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PlaylistSettingsViewController.onCancel)))

        // set up the extra views
        self.contentStackView.insertArrangedSubview(self.mediasController.view, at: 6)

        var tagsIndex: Int = 10
        if self.labelsController != nil {
            self.contentStackView.insertArrangedSubview(self.labelsController.view, at: 8)
        } else {
            self.contentStackView.removeArrangedSubview(self.labelsLabel)
            self.labelsLabel.removeFromSuperview()
            tagsIndex = 8
        }

        if self.tagsController != nil {
            self.contentStackView.insertArrangedSubview(self.tagsController.view, at: tagsIndex)
        } else {
            self.contentStackView.removeArrangedSubview(self.tagsLabel)
            self.tagsLabel.removeFromSuperview()
        }

        if self.playlist != nil {
            self.titleTextField.text = self.playlist.name
            self.descriptionTextField.text = self.playlist.description
            self.publicSwitch.setOn(self.playlist.status == 1, animated: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func onCancel() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func saveAction(_ sender: UIButton) {
        if !(titleTextField.text?.isEmpty)! {
            if !(descriptionTextField.text?.isEmpty)! {
                if let selectedCover: Cover = self.mediasController.getSelectedCover() {
                    if self.fiche != nil && self.playlist == nil {
                        if let userId: String = UserAPI.getCurrentUser()?.id {
                            saveButton.isEnabled = false
                            PlaylistAPI.createPlaylist(name: self.titleTextField.text!,
                                                       description: self.descriptionTextField.text!,
                                                       createdBy: userId,
                                                       photoUrl: selectedCover.path!,
                                                       ficheId: self.fiche.id,
                                                       tags: self.tagsController.getSelectedTagIds(),
                                                       categories: self.labelsController.getSelectedLabelIds(),
                                                       subCategories: [],
                                                       status: self.publicSwitch.isOn).then(in: .main, { (playlist: Playlist) in
                                                        Playlist.addToMyPlaylistLocally(playlist)
                                                        self.dismiss(animated: true, completion: {
                                                            if let container = self.container as? AddPlaylistViewController {
                                                                container.dismiss(animated: true, completion: nil)
                                                            }
                                                        })

                                                       }).catch(in: .main, { (_) -> Void in
                                                        self.showGenericError(true)
                                                        self.saveButton.isEnabled = true
                                                       })
                        }
                    } else {
                        if let userId: String = UserAPI.getCurrentUser()?.id {
                            saveButton.isEnabled = false
                            PlaylistAPI.updatePlaylist(id: self.playlist.id, name: self.titleTextField.text!,
                                                   description: self.descriptionTextField.text!,
                                                   createdBy: userId,
                                                   photoUrl: selectedCover.path!,
                                                   tags: self.tagsController.getSelectedTagIds(),
                                                   categories: self.labelsController.getSelectedLabelIds(),
                                                   subCategories: [],
                                                   status: self.publicSwitch.isOn).then(in: .main, { (newPlaylist: Playlist) in
                                                    Playlist.updateMyPlaylistLocally(newPlaylist)
                                                    self.dismiss(animated: true, completion: {
                                                        if let container = self.container as? DiscoveryPlaylistTableViewController {
                                                            container.updatePlaylist(newPlaylist)
                                                        }
                                                    })
                                                   }).catch(in: .main, { (_) -> Void in
                                                    self.showGenericError(true)
                                                    self.saveButton.isEnabled = true
                                                   })
                        }
                    }
                } else {
                    self.showError(NSLocalizedString("You must select a cover image", comment: "You must select a cover image"), true)
                }
            } else {
                self.showError(NSLocalizedString("You must add a description", comment: "You must add a description"), true)
            }
        } else {
            self.showError(NSLocalizedString("You must add a title", comment: "You must add a title"), true)
        }
    }

    private func setUpControllers() {
        if fiche != nil {
            self.mediasController = SelectMediaViewController(fiches: [self.fiche])
            if let categories: [Category] = self.fiche.categories {
                if !categories.isEmpty {
                    self.labelsController = SelectLabelsViewController(labels: categories)
                    self.addViewController(self.labelsController)
                }
            }
            if let tags: [Tag] = self.fiche.tags {
                if !tags.isEmpty {
                    self.tagsController = SelectTagsViewController(tags: tags)
                    self.addViewController(self.tagsController)
                }
            }
        } else {

            let fiches = (self.playlist.linkedFiches?.map({ (linkedFiche) -> Fiche in
                return linkedFiche.fiche
            }))!

            self.mediasController = SelectMediaViewController(fiches: fiches, selected: self.playlist.cover!)

            var allCategories: [Category] = []
            fiches.forEach({ (fiche) in
                if let categories: [Category] = fiche.categories {
                    categories.forEach({ (categoryToAdd) in
                        if !allCategories.contains(where: { (category) -> Bool in
                            return category.id == categoryToAdd.id
                        }) {
                            allCategories.append(categoryToAdd)
                        }
                    })
                }
            })

            if !allCategories.isEmpty {
                self.labelsController = SelectLabelsViewController(labels: allCategories, selected: self.playlist.categories)
                self.addViewController(self.labelsController)
            }

            var allTags: [Tag] = []
            fiches.forEach({ (fiche) in
                if let tags: [Tag] = fiche.tags {
                    tags.forEach({ (tagToAdd) in
                        if !allTags.contains(where: { (tag) -> Bool in
                            return tag.id == tagToAdd.id
                        }) {
                            allTags.append(tagToAdd)
                        }
                    })
                }
            })

            if !allTags.isEmpty {
                self.tagsController = SelectTagsViewController(tags: allTags, selected: self.playlist.tags)
                self.addViewController(self.tagsController)
            }
        }

        self.addViewController(self.mediasController)
    }

    let snackbar = Snackbar()

    func addSnackbar() {
        self.view.addSubview(snackbar)
    }

    private func layoutSnackbar(status: SnackbarStatus) {
        snackbar.y = .visible == status ? view.height - snackbar.height : view.height
    }

    open func animate(snackbar status: SnackbarStatus, delay: TimeInterval = 0, animations: ((Snackbar) -> Void)? = nil, completion: ((Snackbar) -> Void)? = nil) -> MotionDelayCancelBlock? {
        return Motion.delay(delay) { [weak self, status = status, animations = animations, completion = completion] in

            UIView.animate(withDuration: 0.25, animations: { [weak self, status = status, animations = animations] in
                guard let s = self else {
                    return
                }

                s.layoutSnackbar(status: status)

                animations?(s.snackbar)
            }) { [weak self, status = status, completion = completion] _ in
                guard let s = self else {
                    return
                }
                completion?(s.snackbar)
            }
        }
    }
}

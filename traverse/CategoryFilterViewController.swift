//
//  CategoryFilterViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 06.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class CategoryFilterViewController: FilterViewController, FilterViewTableDelegate {

    private static let CategoryFilterNib: String = "CategoryFilterCell"
    var filterData: [Category] = []
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: CategoryFilterViewController.CategoryFilterNib,
                                      bundle: nil), forCellReuseIdentifier: CategoryFilterViewController.CategoryFilterNib)

        CategoryAPI.getCategories().then { (categories) in
            self.filterData = categories
            self.tableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    func getFilterCount() -> Int {
        return filterData.count
    }

    func getFilterViewAt(_ index: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoryFilterViewController.CategoryFilterNib, for: IndexPath(row: index, section: 0)) as! CategoryFilterCell
        cell.set(filterData[index])

        if (self.filter?.categories.contains(where: { (cat) -> Bool in
            return cat.id == filterData[index].id
        }))! {
            tableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.none)
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.filter?.categories.append(filterData[indexPath.row])
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.filter?.categories = (self.filter?.categories.filter({ (cat) -> Bool in
            return cat.id != filterData[indexPath.row].id
        }))!
    }
}

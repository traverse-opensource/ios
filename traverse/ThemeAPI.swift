//
//  ThemeAPI.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Hydra

class ThemeAPI {
    static func getThemes() -> Promise<[Theme]> {
        return ApiRequest.request(array: ThemeRouter.getThemes(), type: Theme.self)
    }
}

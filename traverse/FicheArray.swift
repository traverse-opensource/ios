//
//  FicheArray.swift
//  traverse
//
//  Created by Mattia Gustarini on 03.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class FicheArray: Gloss.Decodable {
    static let FICHES: String = "fiches"

    let fiches: [FicheFactory]

    required init?(json: JSON) {
        guard let fiches: [FicheFactory] = FicheArray.FICHES <~~ json
            else {
                return nil
        }
        self.fiches = fiches
    }
}

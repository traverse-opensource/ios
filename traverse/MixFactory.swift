//
//  MixFactory.swift
//  traverse
//
//  Created by Mattia Gustarini on 18.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class MixFactory: Gloss.Decodable {
    let itemJson: JSON
    let type: ItemType

    required init?(json: JSON) {
        guard let type: ItemType = ItemEntity.TYPE <~~ json
            else {
                return nil
        }

        self.itemJson = json
        self.type = type
    }

    func getItem() -> ItemEntity? {
        switch type {
        case .playlist:
            return Playlist(json: itemJson)
        default:
            return FicheFactory(json: itemJson)?.getFiche()
        }
    }

    func isPlaylist() -> Bool {
        return type == .playlist
    }
}

//
//  EventDateViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 28.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

private let EventDateViewControllerNib: String = "EventDateViewController"

class EventDateViewController: UIViewController {

    @IBOutlet weak var dateLabel: UILabel!
    private let event: FicheEvent

    init(_ event: FicheEvent) {
        self.event = event
        super.init(nibName: EventDateViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func setUpView() {
        var dateText = ""

        var startDateStr: String?
        var endDateStr: String?

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"

        if self.event.startDate != nil {
            let startDate = MongoEntity.getDateFormatter().date(from: self.event.startDate!)
            startDateStr = dateFormatter.string(from: startDate!)
        }
        if self.event.endDate != nil {
            let endDate = MongoEntity.getDateFormatter().date(from: self.event.endDate!)
            endDateStr = dateFormatter.string(from: endDate!)
        }

        if startDateStr != nil {
            dateText  = startDateStr! + ((endDateStr != nil) ? " - " + endDateStr! : "")
        }
        else {
            if endDateStr != nil {
                dateText  = endDateStr!
            }
        }
        self.dateLabel.text = dateText
        self.dateLabel.sizeToFit()
    }
}

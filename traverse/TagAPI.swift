//
//  TagAPI.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Hydra

class TagAPI {
    static func getTags() -> Promise<[Tag]> {
        return ApiRequest.request(array: TagRouter.getTags(), type: Tag.self)
    }
}

//
//  MyPlaylistsViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 28.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Gloss
import Hydra

class MyPlaylistsViewController: UITableViewController {

    private static let MixinHorizontalTableViewCellIdentifier: String = "MixinHorizontalTableViewCell"
    static let SegueToPlaylist: String = "segueToPlaylist"

    private static let EmptyCellIdentifier: String = "EmptyCell"
    private static let LoginCellIdentifier: String = "LoginCell"
    private static let SectionFont: UIFont = UIFont(name: "Roboto-Black", size: 12)!
    private static let SectionHeight: CGFloat = 48
    private static let LoginCellHeight: CGFloat = 130
    private static let EmptyCellHeight: CGFloat = 130

    private static let MixinHorizontalCellHeight: CGFloat = 266

    @IBOutlet var myPlaylistTableView: UITableView!
    
    private var displayLoginButton = false

    var myFavoritesPlaylists: [Playlist] = []
    var myPlaylists: [Playlist] = []

    var selectedItem: Playlist?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up navigation bar for all controllers under this ones
        self.navigationController?.hidesNavigationBarHairline = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.textColor]
        self.navigationController?.navigationBar.tintColor = UIColor.textColor
        self.navigationController?.navigationBar.isTranslucent = false

        self.tableView.register(
            UINib(nibName: MyPlaylistsViewController.EmptyCellIdentifier, bundle: nil),
            forCellReuseIdentifier: MyPlaylistsViewController.EmptyCellIdentifier)

        self.tableView.register(
            UINib(nibName: MyPlaylistsViewController.LoginCellIdentifier, bundle: nil),
            forCellReuseIdentifier: MyPlaylistsViewController.LoginCellIdentifier)

        self.tableView.register(
            UINib(nibName: MyPlaylistsViewController.MixinHorizontalTableViewCellIdentifier, bundle: nil),
            forCellReuseIdentifier: MyPlaylistsViewController.MixinHorizontalTableViewCellIdentifier)

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 300

    }

    override func viewWillAppear(_ animated: Bool) {
        if let userId: String = UserAPI.getCurrentUser()?.id {
            self.displayLoginButton = false
            loadMyPlaylists(userId: userId)
        }
        else {
            self.displayLoginButton = true
            self.myPlaylists = []
            self.myPlaylistTableView.reloadData()
        }
        loadMyFavorites()
    }
    func loadMyPlaylists(userId: String) {
        UserAPI.getMyPlaylists(userId: userId).then(in: .main) { (playlists: [Playlist]) in

            let promises: [Promise<Playlist>] = playlists.map { return PlaylistAPI.getPlaylist(with: $0.id) }
            all(promises)
                .then(in: .main) { (playlists: [Playlist]) in
                    self.myPlaylists = playlists
                    Playlist.saveMyPlaylistsLocally(playlists)
                    self.myPlaylistTableView.reloadData()
                }
                .catch(in: .main) { (_) -> Void in
                    self.showError(NSLocalizedString("Error! Impossible to load the related medias of this fiche!", comment: "Error! Impossible to load the related medias of this fiche!"))
                }
            }.catch { (error) -> Void in
                self.myPlaylists.removeAll()
                let myPlaylistDataArray = UserDefaults.standard.array(forKey: Const.MY_PLAYLISTS_DATA)
                myPlaylistDataArray?.forEach({ (data) in
                    self.myPlaylists.append(Playlist(json: data as! JSON)!)
                })

                self.myPlaylistTableView.reloadData()
        }
    }

    func loadMyFavorites() {
        PlaylistAPI.getMyFavoritePlaylists().then(in: .main) { (playlists: [Playlist]) in
            self.myFavoritesPlaylists = playlists
            self.myPlaylistTableView.reloadData()
            }
            .catch(in: .main) { (_: Error) in
                //Check from local preferences
                self.myFavoritesPlaylists.removeAll()
                
                let favDataArray = UserDefaults.standard.array(forKey: Const.FAVORITES_PLAYLIST_DATA)
                favDataArray?.forEach({ (data) in
                    self.myFavoritesPlaylists.append(Playlist(json: data as! JSON)!)
                })

                self.myPlaylistTableView.reloadData()
                //self.showError(NSLocalizedString("Error! Impossible to load data!", comment: "Error! Impossible to load data!"))
        }

    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return NSLocalizedString("My playlists", comment: "My playlists")
        } else {
            return NSLocalizedString("Favorites playlists", comment: "Favorites playlists")
        }
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.textLabel?.textColor = UIColor.textColor
            headerTitle.textLabel?.font = MyPlaylistsViewController.SectionFont
            //headerTitle.tintColor = UIColor.backgroundDefaultColor

            headerTitle.backgroundView = UIView(frame: headerTitle.bounds)
            headerTitle.backgroundView?.backgroundColor = UIColor.backgroundDefaultColor
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return MyPlaylistsViewController.SectionHeight
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = UITableViewAutomaticDimension
        if indexPath.section == 0 && displayLoginButton {
            height = MyPlaylistsViewController.LoginCellHeight
        }

        else {
            if (indexPath.section == 0 && !myPlaylists.isEmpty)  || (indexPath.section == 1 && !myFavoritesPlaylists.isEmpty) {
                height = MyPlaylistsViewController.MixinHorizontalCellHeight
            }
            else {
                height = MyPlaylistsViewController.EmptyCellHeight
            }

        }

        return height
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell()
        if indexPath.section == 0 && displayLoginButton {
            if let loginCell: LoginCell =
                tableView.dequeueReusableCell(
                    withIdentifier: MyPlaylistsViewController.LoginCellIdentifier,
                    for: indexPath) as? LoginCell {
                loginCell.delegate = self
                cell = loginCell
            }
        } else {
            if (indexPath.section == 0 && !myPlaylists.isEmpty)  || (indexPath.section == 1 && !myFavoritesPlaylists.isEmpty) {
                if let horizontalCell: MixinHorizontalTableViewCell =
                    tableView.dequeueReusableCell(
                        withIdentifier: MyPlaylistsViewController.MixinHorizontalTableViewCellIdentifier,
                        for: indexPath) as? MixinHorizontalTableViewCell {
                    cell = horizontalCell
                }
            }
            else {
                cell = tableView.dequeueReusableCell(withIdentifier: MyPlaylistsViewController.EmptyCellIdentifier, for: indexPath)
            }
        }

        return cell
    }

    override func tableView(_ tableView: UITableView,
                            willDisplay cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
        if indexPath.section != 0 || !self.displayLoginButton {
            // set the delegates for the horizontal UICollectionView
            guard let tableViewCell = cell as? MixinHorizontalTableViewCell else {
                return
            }
            tableViewCell.setCollectionViewDelegate(delegate: self, forRow: indexPath.section)
            // set the offset to go back to the horizontal cell position we left
            //tableViewCell.collectionViewOffset = self.storedOffsetOfHeartstrokes
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == MyPlaylistsViewController.SegueToPlaylist {
            if let destination: DiscoveryPlaylistTableViewController =
                segue.destination as? DiscoveryPlaylistTableViewController {

                destination.set(playlist: self.selectedItem!)

            }
        }
    }

}

extension MyPlaylistsViewController: UICollectionViewDelegate,
    UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    private static let HorizontalSectionInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 8.0, bottom: 0, right: 0)

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return myPlaylists.count
        }
        return myFavoritesPlaylists.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell = UICollectionViewCell()
        if let smallCard: SmallCard = collectionView.dequeueReusableCell(
            withReuseIdentifier: MixinHorizontalTableViewCell.SmallCardNib,
            for: indexPath as IndexPath) as? SmallCard {

            var item: ItemEntity?

            if collectionView.tag == 0 {
                item = myPlaylists[indexPath.row]
            }
            else {
                item = myFavoritesPlaylists[indexPath.row]
            }
            smallCard.set(item: item!)
            cell = smallCard
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        var item: ItemEntity?

        if collectionView.tag == 0 {
            item = myPlaylists[indexPath.row]
        }
        else {
            item = myFavoritesPlaylists[indexPath.row]
        }

        if let playlist = item as? Playlist {
            self.selectedItem = playlist
            performSegue(withIdentifier: MyPlaylistsViewController.SegueToPlaylist, sender: self)
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return MyPlaylistsViewController.HorizontalSectionInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let availableWidth: CGFloat = collectionView.frame.width - MyPlaylistsViewController.HorizontalSectionInsets.left

        //let width: GLfloat = GLfloat((availableWidth / 2.0) + (availableWidth / 4.0))
        let width: GLfloat = GLfloat((availableWidth / 1.8))

        return CGSize(width: Int(width), height: Int(collectionView.frame.height))
    }

//    func collectionView(_ collectionView: UICollectionView,
//                        didSelectItemAt indexPath: IndexPath) {
//        self.performSegueTo(item: self.heartstrokes[indexPath.row])
//    }
}

//
//  Filter.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 06.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//
import Gloss

class Filter: Gloss.Decodable {

    static let THEMES: String = "themes"
    static let TAGS: String = "tags"
    static let CATEGORIES: String = "categories"
    static let TYPES: String = "types"

    static let DATE: String = "date"

    var themes: [Theme] = []
    var tags: [Tag] = []
    var categories: [Category] = []
    var types: [ItemType] = []

    var date: Date?

    required init() {
        self.themes = []
        self.tags = []
        self.categories = []
        self.types = []
    }

    required init?(json: JSON) {

        guard let themes: [Theme] = Filter.THEMES <~~ json,
        let tags: [Tag] = Filter.TAGS <~~ json,
        let categories: [Category] = Filter.CATEGORIES <~~ json,
        let types: [ItemType] = Filter.TYPES <~~ json
            else {
                return nil
        }

        self.themes = themes
        self.tags = tags
        self.categories = categories
        self.types = types
        self.date = Filter.DATE <~~ json
    }

    func toJSON() -> JSON? {

        var jsonThemes: [JSON] = []
        self.themes.forEach { (theme) in
            jsonThemes.append(theme.toJSON()!)
        }

        var jsonTags: [JSON] = []
        self.tags.forEach { (tag) in
            jsonTags.append(tag.toJSON()!)
        }

        var jsonCats: [JSON] = []
        self.categories.forEach { (cat) in
            jsonCats.append(cat.toJSON()!)
        }

        if self.date == nil {
            self.date = Date()
        }

        return jsonify([
            ["themes" : jsonThemes],
            ["tags" : jsonTags],
            ["categories" : jsonCats],
            "date" ~~> self.date,
            "types" ~~> self.types
        ])
    }

    func removeItemAt(_ index: Int) {
        var filterIndex = index
        if filterIndex < self.types.count {
            self.types.remove(at: filterIndex)
        }
        else {
            filterIndex -= self.types.count
            if filterIndex < self.themes.count {
                self.themes.remove(at: filterIndex)
            }
            else {
                filterIndex -= self.themes.count
                if filterIndex < self.categories.count {
                    self.categories.remove(at: filterIndex)
                }
                else {
                    filterIndex -= self.categories.count
                    if filterIndex < self.tags.count {
                        self.tags.remove(at: filterIndex)
                    }
                }
            }
        }

    }

    static func getFilterForIndex(_ filter: Filter, index: Int) -> Any {
        var filterIndex = index
        if filterIndex < filter.types.count {
            return filter.types[filterIndex]
        }
        else {
            filterIndex -= filter.types.count
            if filterIndex < filter.themes.count {
                return filter.themes[filterIndex]
            }
            else {
                filterIndex -= filter.themes.count
                if filterIndex < filter.categories.count {
                    return filter.categories[filterIndex]
                }
                else {
                    filterIndex -= filter.categories.count
                    if filterIndex < filter.tags.count {
                        return filter.tags[filterIndex]
                    }
                    else {
                        //Impossible..
                        return ""
                    }
                }
            }
        }
    }

    static func getFilterLabelForIndex(_ filter: Filter, index: Int) -> String {
        var filterIndex = index
        if filterIndex < filter.types.count {
            return NSLocalizedString(filter.types[filterIndex].rawValue, comment: "")
        }
        else {
            filterIndex -= filter.types.count
            if filterIndex < filter.themes.count {
                return filter.themes[filterIndex].name
            }
            else {
                filterIndex -= filter.themes.count
                if filterIndex < filter.categories.count {
                    return filter.categories[filterIndex].name
                }
                else {
                    filterIndex -= filter.categories.count
                    if filterIndex < filter.tags.count {
                        return filter.tags[filterIndex].name
                    }
                    else {
                        //Impossible..
                        return ""
                    }
                }
            }
        }
    }


}

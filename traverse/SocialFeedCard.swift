//
//  SocialFeedCard.swift
//  traverse
//
//  Created by Mattia Gustarini on 04.10.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class SocialFeedCard: UITableViewCell {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var socialImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!

    private var feed: Feed!
    private var parentController: UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(feed: Feed, parentController: UIViewController) {
        self.feed = feed
        self.parentController = parentController
        switch feed.kind {
            case .facebook:
                self.userImageView.image = #imageLiteral(resourceName: "ic_facebook")
            case .instagram:
                self.userImageView.image = #imageLiteral(resourceName: "ic_instagram")
            case .twitter:
                self.userImageView.image = #imageLiteral(resourceName: "ic_twitter")
            case .traverse:
                self.userImageView.image = #imageLiteral(resourceName: "ic_traverse")
        }
        self.moreButton.isHidden = feed.kind != .traverse
        self.usernameLabel.text = feed.createdBy?.name
        self.socialImageView.contentMode = .center

        if let cover: Cover = feed.cover {
            self.socialImageView.hnk_setImageFromURL(cover.getBigImageUrl(), placeholder: nil, format: nil, failure: nil, success: {(image) in
                self.socialImageView.image = image
                self.socialImageView.contentMode = .scaleAspectFill
            })
        }

        self.descriptionLabel.text = feed.description
    }
    @IBAction func moreAction(_ sender: UIButton) {
        let optionMenu: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        optionMenu.view.tintColor = UIColor.textColor
        let backView = optionMenu.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 24.0
        backView?.backgroundColor = UIColor.backgroundDefaultColor

        let reportAbuseAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Report abuse", comment: "Report abuse"), style: .default, handler: {_ in

            let alertController: UIAlertController =
                UIAlertController(title: NSLocalizedString("Would you like to report this content as inapropriate?", comment: "Would you like to report this content as inapropriate?"), message: nil, preferredStyle: .alert)

            alertController.view.tintColor = UIColor.textColor
            let backView = alertController.view.subviews.last?.subviews.last
            backView?.layer.cornerRadius = 24.0
            backView?.backgroundColor = UIColor.backgroundDefaultColor

            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel) { _ in
                // ...
            }
            alertController.addAction(cancelAction)

            let okAction = UIAlertAction(title: "OK", style: .default) { _ in
                FeedAPI.flagFeed(id: self.feed.id).always(in: .main, body: {
                    self.parentController.showMessage(NSLocalizedString("Thank you for your report!", comment: "Thank you for your report"))
                })
            }
            alertController.addAction(okAction)

            self.parentController.present(alertController, animated: true) {
                // ...
            }
        })

        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel, handler: nil)

        optionMenu.addAction(reportAbuseAction)
        optionMenu.addAction(cancelAction)

        self.parentController.present(optionMenu, animated: true, completion: nil)
    }
}

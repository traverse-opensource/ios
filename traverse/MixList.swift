//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

class MixList {
    let fiches: [Fiche]
    let playlists: [Playlist]

    init?(mixItems: [MixFactory]) {
        var fiches: [Fiche] = []
        var playlists: [Playlist] = []
        for mixItem: MixFactory in mixItems {
            if mixItem.isPlaylist() {
                if let playlist: Playlist = mixItem.getItem() as? Playlist {
                    playlists.append(playlist)
                }
            } else {
                if let fiche: Fiche = mixItem.getItem() as? Fiche {
                    fiches.append(fiche)
                }
            }
        }

        self.fiches = fiches
        self.playlists = playlists
    }
}

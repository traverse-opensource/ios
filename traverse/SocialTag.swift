//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class SocialTag: Gloss.Decodable {
    static let VALUE: String = "value"
    static let LABEL: String = "label"
    static let CLASS_NAME: String = "className"

    let value: String
    let label: String
    let className: String

    required init?(json: JSON) {
        guard let value: String = SocialTag.VALUE <~~ json,
            let label: String = SocialTag.VALUE <~~ json,
            let className: String = SocialTag.VALUE <~~ json
            else {
                return nil
        }

        self.value = value
        self.label = label
        self.className = className
    }
}

//
//  AddFeedDelegate.swift
//  traverse
//
//  Created by Mattia Gustarini on 17.10.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Foundation

protocol AddFeedDelegate: class {
    func feedAdded(feed: Feed)
}

//
//  BigCard.swift
//  traverse
//
//  Created by Mattia Gustarini on 26.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Haneke
import ChameleonFramework

class BigCard: UITableViewCell {
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var authorView: UIView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var ficheCountView: UIView!
    @IBOutlet weak var ficheTypeView: UIView!
    @IBOutlet weak var nFichesLabel: UILabel!
    @IBOutlet weak var ficheImage: UIImageView!
    @IBOutlet weak var themeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentStack: UIStackView!

    private var item: ItemEntity!
    private var showLinked: Bool!
    private var authorController: FicheAuthorViewController!
    private var labelsViewController: LabelsViewController!
    private var linkedFicheViewController: BigLinkedFichesViewController!
    private var parentController: UIViewController!
    private weak var linkedFichesDelegate: FicheClickDelegate!
    private weak var actionBarDelegate: ItemQuickActionBarDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(withTopMargin item: ItemEntity,
             parentController: UIViewController,
             actionBarDelegate: ItemQuickActionBarDelegate) {
        self.set(item: item,
                 parentController: parentController,
                 actionBarDelegate: actionBarDelegate)
        self.topConstraint.constant = 8
    }

    func set(withoutTopMargin item: ItemEntity,
             parentController: UIViewController,
             actionBarDelegate: ItemQuickActionBarDelegate) {
        self.set(item: item,
                 parentController: parentController,
                 actionBarDelegate: actionBarDelegate)
        self.topConstraint.constant = 0
    }

    func set(item: ItemEntity,
             showLinked: Bool = false,
             parentController: UIViewController,
             actionBarDelegate: ItemQuickActionBarDelegate,
             linkedFichesDelegate: FicheClickDelegate! = nil) {
        self.item = item
        self.showLinked = showLinked

        self.parentController = parentController
        self.linkedFichesDelegate = linkedFichesDelegate
        self.actionBarDelegate = actionBarDelegate

        cleanUpContent()

        addAuthor()

        if let cover: Cover = item.cover {
            self.coverImage.contentMode = .center
            
            if let fiche = item as? Fiche {
                self.coverImage.hnk_setImageFromURL(cover.getImageUrl(), placeholder: UIView.getFicheTypeImage(fiche: fiche), format: nil, failure: nil, success: {(image) in
                    self.coverImage.image = image
                    self.coverImage.contentMode = .scaleAspectFill
                })
            }
            else {
                self.coverImage.hnk_setImageFromURL(cover.getImageUrl(), placeholder: #imageLiteral(resourceName: "ic_playlist"), format: nil, failure: nil, success: {(image) in
                    self.coverImage.image = image
                    self.coverImage.contentMode = .scaleAspectFill
                })
            }
        }
        self.titleLabel.text = item.name

        switch item.type {
        case .playlist:
            setPlaylistSpecific()
        default:
            setFicheSpecific()
        }

        self.contentView.layoutIfNeeded()
    }

    func setPlaylistSpecific() {
        if let playlist: Playlist = self.item as? Playlist {
            if playlist.isIdsOnly() {
                self.nFichesLabel.text = "\(playlist.linkedFichesIds!.count) fiches"
            } else {
                self.nFichesLabel.text = "\(playlist.linkedFiches!.count) fiches"
            }

            self.ficheCountView.isHidden = false
            self.ficheTypeView.isHidden = true
            UIView.setTheme(playlist: playlist,
                            separatorView: separatorView,
                            iconView: ficheCountView,
                            themeNameLabel: themeLabel,
                            ficheCounterLabel: nFichesLabel)
            // add extra views to the stack view in a specific order for playlist
            addTags()
            addDescription()
            // add top margin for full playlist view and add linked fiches stack
            if showLinked {
                self.topConstraint.constant = 8
                addLinkedFiches()
            }
            addQuickActionBar()
            addLabels()
        }
    }

    func setFicheSpecific() {
        if let fiche: Fiche = self.item as? Fiche {
            UIView.setFicheTypeImage(self.ficheImage, fiche: fiche)
            self.ficheTypeView.isHidden = false
            self.ficheCountView.isHidden = true
            UIView.setTheme(fiche: fiche,
                            separatorView: self.separatorView,
                            iconView: self.ficheTypeView,
                            themeNameLabel: self.themeLabel)
            // add extra views to the stack view in a specific order for fiche
            addTags()
            addEventDate()
            addDescription()
            addQuickActionBar()
            addLabels()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        let superTarget: CGSize = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
        if self.labelsViewController != nil {
            let labelsSize: CGSize = self.labelsViewController.getSize()
            return CGSize(width: superTarget.width, height: superTarget.height + labelsSize.height - 32) //32 is the basic size set in xib of the labels controller for the collection view size
        }
        return superTarget
    }

    private func addAuthor() {
        if let userProfile: UserProfile = self.item.createdBy?.profile {
            var showMenu: Bool = false
            if let userId: String = UserAPI.getCurrentUser()?.id {
                if let author: User = self.item.createdBy {
                    showMenu = author.id == userId
                }
            }

            if let playlist: Playlist = self.item as? Playlist {
                self.authorController = FicheAuthorViewController(userProfile: userProfile, playlist: playlist, showMenu: showMenu, container: self.parentController)
            }
            else {
                self.authorController = FicheAuthorViewController(userProfile: userProfile, showMenu: showMenu, container: self.parentController)
            }

            self.parentController.addChildViewController(self.authorController)
            self.authorController.didMove(toParentViewController: self.parentController)
            self.authorController.view.translatesAutoresizingMaskIntoConstraints = false
            self.authorView.addSubview(self.authorController.view)
            self.authorView.addConstraint(NSLayoutConstraint(item: self.authorController.view, attribute: .top, relatedBy: .equal, toItem: self.authorView, attribute: .top, multiplier: 1.0, constant: 0.0))
            self.authorView.addConstraint(NSLayoutConstraint(item: self.authorController.view, attribute: .leading, relatedBy: .equal, toItem: self.authorView, attribute: .leading, multiplier: 1.0, constant: 8.0))
            self.authorView.addConstraint(NSLayoutConstraint(item: self.authorView, attribute: .bottom, relatedBy: .equal, toItem: self.authorController.view, attribute: .bottom, multiplier: 1.0, constant: 8.0))
            self.authorView.addConstraint(NSLayoutConstraint(item: self.authorView, attribute: .trailing, relatedBy: .equal, toItem: self.authorController.view, attribute: .trailing, multiplier: 1.0, constant: 0.0))
        }
    }

    private func removeAuthor() {
        if self.authorController != nil {
            self.authorController.view.removeFromSuperview()
        }
    }

    private func addDescription() {
        var description: String = ""
        if let playlist: Playlist = self.item as? Playlist {
            description = playlist.description ?? ""
        }
        else if let fiche: Fiche = self.item as? Fiche {
            description = fiche.shortDescription ?? ""
        }

        let descriptionController: DescriptionViewController = DescriptionViewController(description)
        self.parentController.addChildViewController(descriptionController)
        descriptionController.didMove(toParentViewController: self.parentController)
        self.contentStack.addArrangedSubview(descriptionController.view)
    }

    private func addEventDate() {
        if let ficheEvent: FicheEvent = self.item as? FicheEvent {
            let eventDateController: EventDateViewController = EventDateViewController(ficheEvent)
            self.parentController.addChildViewController(eventDateController)
            eventDateController.didMove(toParentViewController: self.parentController)
            self.contentStack.addArrangedSubview(eventDateController.view)
        }
    }

    private func addQuickActionBar() {
        let quickActionBarController: ItemQuickActionBarViewController = ItemQuickActionBarViewController(item: self.item, actionBarDelegate: self.actionBarDelegate)
            self.parentController.addChildViewController(quickActionBarController)
            quickActionBarController.didMove(toParentViewController: self.parentController)
        self.contentStack.addArrangedSubview(quickActionBarController.view)
    }

    private func addLabels() {
        if self.item.categories != nil {
            let categories: [Category] = self.item.categories!

            var categoryList: [String] = []
            categoryList.append(contentsOf: categories.map({ (cat) -> String in
                return cat.name
            }))

            if self.item.subCategories != nil {
                categoryList.append(contentsOf: self.item.subCategories!)
            }

            if !categoryList.isEmpty {
                self.labelsViewController = LabelsViewController(labels: categoryList, theme: self.item.mainTheme)
                self.parentController.addChildViewController(labelsViewController)
                    labelsViewController.didMove(toParentViewController: self.parentController)
                self.contentStack.addArrangedSubview(labelsViewController.view)
            }
        }
    }

    private func removeLabels() {
        self.labelsViewController = nil
    }

    private func addTags() {
        if self.item.tags != nil {
            let tags: [Tag] = self.item.tags!
            if !tags.isEmpty {
                let tagsViewController: TagsViewController = TagsViewController(tags: tags, theme: self.item.mainTheme)
                self.parentController.addChildViewController(tagsViewController)
                tagsViewController.didMove(toParentViewController: self.parentController)
                self.contentStack.addArrangedSubview(tagsViewController.view)
            }
        }
    }

    private func addLinkedFiches() {
        if let playlist: Playlist = self.item as? Playlist {
            self.linkedFicheViewController = BigLinkedFichesViewController(linkedFiches: playlist.linkedFiches!, delegate: self.linkedFichesDelegate)
            self.parentController.addChildViewController(self.linkedFicheViewController)
            self.linkedFicheViewController.didMove(toParentViewController: self.parentController)
            self.contentStack.addArrangedSubview(self.linkedFicheViewController.view)
        }
    }

    private func removeLinkedFiche() {
        if self.linkedFicheViewController != nil {
            self.contentStack.removeArrangedSubview(self.linkedFicheViewController.view)
            self.linkedFicheViewController.view.removeFromSuperview()
        }
    }

    private func cleanUpContent() {
        removeAuthor()
        removeLabels()
        for view: UIView in self.contentStack.arrangedSubviews {
            self.contentStack.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
}

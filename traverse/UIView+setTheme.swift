//
//  UIView+setTheme.swift
//  traverse
//
//  Created by Mattia Gustarini on 26.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Haneke
import ChameleonFramework

extension UIView {
    static func setTheme(playlist: Playlist,
                         separatorView: UIView,
                         iconView: UIView,
                         themeNameLabel: UILabel,
                         ficheCounterLabel: UILabel) {
        let theme: ItemTheme = playlist.mainTheme
        if let themeColor: UIColor = theme.getThemeColor() {
            setCommonThemeElements(item: playlist,
                                   themeColor: themeColor,
                                   separatorView: separatorView,
                                   iconView: iconView,
                                   themeNameLabel: themeNameLabel)
//            ficheCounterLabel.textColor = UIColor(contrastingBlackOrWhiteColorOn: themeColor, isFlat: true)
            if theme.name?.lowercased() != "default" {
                themeNameLabel.text = "\(theme.name!)"
            }
            else {
                themeNameLabel.text = ""
            }
        }
    }

    static func setTheme(fiche: Fiche,
                         separatorView: UIView,
                         iconView: UIView,
                         themeNameLabel: UILabel) {
        let theme: ItemTheme = fiche.mainTheme
        if let themeColor: UIColor = theme.getThemeColor() {
            setCommonThemeElements(item: fiche,
                                   themeColor: themeColor,
                                   separatorView: separatorView,
                                   iconView: iconView,
                                   themeNameLabel: themeNameLabel)
            if theme.name?.lowercased() != "default" {
                themeNameLabel.text = "\(theme.name!)"
            }
            else {
                themeNameLabel.text = ""
            }
        }
    }

    static func setTheme(fiche: Fiche,
                         iconView: UIView) {
        let theme: ItemTheme = fiche.mainTheme
        if let themeColor: UIColor = theme.getThemeColor() {
            iconView.backgroundColor = themeColor
        }
    }

    static func setTheme(fiche: Fiche,
                         iconView: UIView, separatorView: UIView) {
        let theme: ItemTheme = fiche.mainTheme
        if let themeColor: UIColor = theme.getThemeColor() {
            iconView.backgroundColor = themeColor
            separatorView.backgroundColor = themeColor
        }
    }

    private static func setCommonThemeElements(item: ItemEntity,
                                               themeColor: UIColor,
                                               separatorView: UIView,
                                               iconView: UIView,
                                               themeNameLabel: UILabel) {
        separatorView.backgroundColor = themeColor
        iconView.backgroundColor = themeColor
        themeNameLabel.textColor = themeColor
    }
}

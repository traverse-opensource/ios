//
//  FeedUser.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 28.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class FeedUser: Gloss.Decodable {

    static let NAME: String = "name"
    static let FULLNAME: String = "full_name"
    static let PHOTO_URL: String = "photo_url"

    var name: String?
    private var fullName: String?
    private var photoUrl: String?

    required init?(json: JSON) {
        self.name = FeedUser.NAME <~~ json
        self.fullName = FeedUser.FULLNAME <~~ json
        self.photoUrl = FeedUser.PHOTO_URL <~~ json
    }

}

//
//  EmptyCell.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 22.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class EmptyCell: UITableViewCell {

    @IBOutlet weak var message: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setEmptyMessage(_ emptyMessage: String) {
        self.message.text = emptyMessage
    }
}


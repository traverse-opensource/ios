//
//  SelectTagsViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 13.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import TagCellLayout

private let SelectTagsViewControllerNib: String = "SelectTagsViewController"
private let LabelCellNib: String = "LabelCell"
private let SectionInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 16.0, bottom: 0, right: 16.0)

class SelectTagsViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
TagCellLayoutDelegate {

    @IBOutlet weak var tagsView: UICollectionView!
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!

    private let tags: [Tag]
    private var selectedIndexes: [Int] = []

    private var selectedTags: [Tag]?

    init(tags: [Tag]) {
        self.tags = tags
        super.init(nibName: SelectTagsViewControllerNib, bundle: nil)
    }

    convenience init(tags: [Tag], selected: [Tag]?) {
        self.init(tags: tags)
        self.selectedTags = selected
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        // Create a waterfall layout
        let tagCellLayout: TagCellLayout =
            TagCellLayout(tagAlignmentType: .left, delegate: self)
        // Add the waterfall layout to your collection view
        self.tagsView.collectionViewLayout = tagCellLayout

        // Register cell classes
        self.tagsView.register(UINib(nibName: LabelCellNib, bundle: nil), forCellWithReuseIdentifier: LabelCellNib)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let height: CGFloat = self.tagsView.collectionViewLayout.collectionViewContentSize.height
        self.collectionHeightConstraint.constant = height
    }

    func getSelectedTagIds() -> [String] {
        var selectedTags: [String] = []
        for selectedIndex: Int in self.selectedIndexes {
            selectedTags.append(self.tags[selectedIndex].id)
        }
        return selectedTags
    }

    func tagCellLayoutTagFixHeight(_ layout: TagCellLayout) -> CGFloat {
        return CGFloat(32.0)
    }

    func tagCellLayoutTagWidth(_ layout: TagCellLayout, atIndex index: Int) -> CGFloat {

        let width: CGFloat = TagCellLayout.textWidth("#" + self.tags[index].name,
                                                     font: UIFont(name: "Roboto-Regular", size: 12.0)!)
        return width + 24.0
    }

    // MARK: UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tags.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()

        if let labelCell: LabelCell = collectionView.dequeueReusableCell(withReuseIdentifier: LabelCellNib, for: indexPath) as? LabelCell {
            labelCell.set(label: "#" + self.tags[indexPath.row].name)

            if self.selectedTags != nil && (self.selectedTags?.contains(where: { (tag) -> Bool in
                return tag.id == self.tags[indexPath.row].id
            }))! {
                self.selectedIndexes.append(indexPath.row)
                labelCell.select()
            }
            cell = labelCell
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        if let labelCell: LabelCell = collectionView.cellForItem(at: indexPath) as? LabelCell {
            let selectedIndex: Int = indexPath.row
            if self.selectedIndexes.contains(selectedIndex) {
                let index: Int = self.selectedIndexes.index(of: selectedIndex)!
                self.selectedIndexes.remove(at: index)
                labelCell.deselect()
            } else {
                self.selectedIndexes.append(selectedIndex)
                labelCell.select()
            }
        }
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

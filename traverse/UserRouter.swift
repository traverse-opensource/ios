//
//  UserRouter.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Alamofire

enum UserRouter: URLRequestConvertible {
    case customSignUp(email: String, password: String, name: String)
    case signupFromSN(kind: String, userId: String, userName: String, email: String, picture: String)
    case login(email: String, password: String)
    case loginFromSN(kind: String, userId: String, userName: String)
    case getUsers(page: Int, limit: Int)
    case syncFavorites(id: String, favorites: [String])
    case getSuggestions()
    case getSuggestionsWithParams(maxDistance: Int,
        latitude: Double,
        longitude: Double,
        page: Int,
        limit: Int,
        includePlaylist: Bool)
    case getMyPlaylists(userId: String)

    var method: HTTPMethod {
        switch self {
        case .customSignUp, .signupFromSN, .login, .loginFromSN:
            return .post
        case .getUsers:
            return .get
        case .syncFavorites:
            return .post
        case .getSuggestions:
            return .get
        case .getSuggestionsWithParams:
            return .get
        case .getMyPlaylists:
            return .get
        }
    }

    var path: String {
        switch self {
        case .customSignUp:
            return "users/one/signup"
        case .signupFromSN:
            return "users/one/signup"
        case .login:
            return "users/one/login"
        case .loginFromSN:
            return "users/one/signup"
        case .getUsers:
            return "/users"
        case .syncFavorites(let id, _):
            return "/users/\(id)/syncFavorites"
        case .getSuggestions:
            return "/users/one/suggestions"
        case .getSuggestionsWithParams:
            return "/users/one/suggestions"
        case .getMyPlaylists(let userId):
            return "/users/\(userId)/playlists"
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Const.API_URL.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        // if arguments are needed
        var parameters: Parameters!
        switch self {
        case .customSignUp(let email, let password, let name):
            parameters = ["email": email,
                          "password": password,
                          "name": name]
        case .signupFromSN(let kind, let userId, let userName, let email, let picture):
            parameters = ["kind": kind,
                          "userId": userId,
                          "name": userName,
                          "userName": userName,
                          "email": email,
                          "picture": picture]
        case .login(let email, let password):
            parameters = ["email": email,
                          "password": password]
        case .loginFromSN(let kind, let userId, let userName):
            parameters = ["kind": kind,
                          "userId": userId,
                          "userName": userName]
        case .getSuggestionsWithParams(let maxDistance,
                                       let latitude,
                                       let longitude,
                                       let page,
                                       let limit,
                                        let includePlaylist):
            parameters = ["maxDistance": maxDistance,
                          "lat": latitude,
                          "lng": longitude,
                          "page": page,
                          "limit": limit,
                          "includesPlaylist": includePlaylist]
        case .syncFavorites(_, let favorites):
            parameters = ["favorites": favorites]
        case .getUsers(let page, let limit):
            parameters = ["page": page,
                          "limit": limit]
        default:
            break
        }

        if parameters != nil {
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }

        return urlRequest
    }
}

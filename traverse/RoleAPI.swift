//
//  RoleAPI.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Hydra

class RoleAPI {
    static func getRoles() -> Promise<[Role]> {
        return ApiRequest.request(array: RoleRouter.getRoles(), type: Role.self)
    }
}

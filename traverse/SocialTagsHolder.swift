//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class SocialTagsHolder: WebHolder {
    static let TAGS: String = "tags"

    var tags: [SocialTag]?

    required init?(json: JSON) {
//        guard let tags: [SocialTag] = SocialTagsHolder.TAGS <~~ json
//            else {
//                return nil
//        }

        self.tags = SocialTagsHolder.TAGS <~~ json
        super.init(json: json)
    }
}

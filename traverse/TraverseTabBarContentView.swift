//
//  TraverseTabBarContentView.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 17.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import ESTabBarController_swift

class TraverseTabBarContentView: ESTabBarItemContentView {

    override init(frame: CGRect) {
        super.init(frame: frame)

        let transform = CGAffineTransform.identity
        imageView.transform = transform.scaledBy(x: 1, y: 1)
        insets = UIEdgeInsets(top: 0, left: 0, bottom: 4, right: 0)
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func highlightAnimation(animated: Bool, completion: (() -> Void)?) {
        UIView.beginAnimations("small", context: nil)
        UIView.setAnimationDuration(0.2)
        let transform = imageView.transform.scaledBy(x: 0.7, y: 0.7)
        imageView.transform = transform
        UIView.commitAnimations()
        completion?()
    }

    override func dehighlightAnimation(animated: Bool, completion: (() -> Void)?) {
        UIView.beginAnimations("big", context: nil)
        UIView.setAnimationDuration(0.2)
        let transform = CGAffineTransform.identity
        imageView.transform = transform.scaledBy(x: 1, y: 1)
        UIView.commitAnimations()
        completion?()
    }
}

//
//  DiscoveryFicheViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 04.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Haneke
import MXParallaxHeader

class DiscoveryFicheViewController: UIViewController,
    MXParallaxHeaderDelegate,
    PlaylistClickDelegate,
    FicheClickDelegate,
    ItemQuickActionBarDelegate {
    private static let SegueToPlaylist: String = "segueToPlaylist"
    private static let StoryBoardReference: String = "DiscoveryFicheDetails"

    private var scrollView: MXScrollView!
    private var ficheHeaderViewController: FicheHeaderViewController!
    private var ficheTabController: FicheTabController!
    private var ficheViewController: FicheViewController!
    private var ficheNewsViewController: FicheNewsViewController!
    private var playlistViewController: DiscoveryPlaylistTableViewController!

    private var fiche: Fiche!
    private var selectedFiche: Fiche!
    private var playlist: Playlist!
    private var selectedPlaylist: Playlist!

    var isInit: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Parallax Header
        self.scrollView = MXScrollView(frame: self.view.frame)
        self.ficheHeaderViewController = FicheHeaderViewController()
        addChildViewController(self.ficheHeaderViewController)
        self.scrollView.parallaxHeader.view = self.ficheHeaderViewController.view
        self.ficheHeaderViewController.didMove(toParentViewController: self)
        self.scrollView.parallaxHeader.height = 272
        self.scrollView.parallaxHeader.mode = MXParallaxHeaderMode.bottom
        self.scrollView.parallaxHeader.minimumHeight = 0
        self.scrollView.parallaxHeader.delegate = self
        self.scrollView.bounces = false
        self.view.addSubview(scrollView)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic-map"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(DiscoveryFicheViewController.displayMap))

        // Set Tab controller
        setUpTabController()

        // Set the fiche in all controllers
        setFicheInControllers()

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if !isInit {
            var frame: CGRect = view.frame
//
            self.scrollView.frame = frame
            self.scrollView.contentSize = frame.size
//
            frame.size.height -= self.scrollView.parallaxHeader.minimumHeight
            self.ficheTabController.view.frame = frame

            isInit = true
        }
    }

    func set(fiche: Fiche, playlist: Playlist? = nil) {
        self.fiche = fiche
        self.playlist = playlist
        setFicheInControllers()
    }

    func displayMap() {
        let mapView = MapViewController()

        mapView.staticItems = [self.fiche]
        mapView.staticMode = true
        mapView.isPlaylist = false

        //self.navigationController?.popToRootViewController(animated: false)
        self.navigationController?.pushViewController(mapView, animated: false)

    }

    func onClick(playlist: Playlist) {
        self.selectedPlaylist = playlist
        performSegue(withIdentifier: DiscoveryFicheViewController.SegueToPlaylist,
                     sender: self)
    }

    func onClick(fiche: Fiche) {
        self.selectedFiche = fiche
        if let discoveryFicheViewController: DiscoveryFicheViewController =
            UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: DiscoveryFicheViewController.StoryBoardReference)
                as? DiscoveryFicheViewController {
            if let navigationController: UINavigationController = self.navigationController {
                
                navigationController.pushViewController(discoveryFicheViewController, animated: true)
                var playlist: Playlist? = nil
                if self.playlist != nil {
                    if self.playlist.contains(fiche: self.selectedFiche) {
                        playlist = self.playlist
                    }
                }
                discoveryFicheViewController.set(fiche: self.selectedFiche, playlist: playlist)
            }
        }
    }

    func onAddClick(fiche: Fiche) {
        ItemQuickActionBarViewController.addFicheToPlaylistHelper(controller: self, ficheToAdd: fiche)
    }

    private func setFicheInControllers() {
        if self.fiche != nil &&
            self.ficheHeaderViewController != nil &&
            self.ficheViewController != nil &&
            self.ficheNewsViewController != nil &&
            self.playlistViewController != nil {
            self.ficheHeaderViewController.set(fiche: self.fiche)
            self.playlistViewController.setRelatedFrom(fiche: self.fiche,
                                                       playlistDelegate: self)
        }
    }

    private func setUpTabController() {
        if let playlistViewController: DiscoveryPlaylistTableViewController =
            UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: DiscoveryPlaylistTableViewController.StoryBoardReference)
                as? DiscoveryPlaylistTableViewController {
            // Intialize the controllers for the tabs
            self.playlistViewController = playlistViewController
            self.ficheViewController = FicheViewController(fiche: self.fiche,
                                                           playlist: self.playlist,
                                                           actionBarDelegate: self,
                                                           fichesDelegate: self)
            self.ficheNewsViewController = FicheNewsViewController(fiche: self.fiche, navigationController: self.navigationController)

            // Initialize the tab controller
            self.ficheTabController =
                FicheTabController(viewControllers:
                    [ficheViewController, ficheNewsViewController, playlistViewController],
                                   selectedIndex: 0)
            
            ficheViewController.tabItem.backgroundColor = UIColor.backgroundDefaultColor

            // Add tab controller to this controller
            addChildViewController(self.ficheTabController)
            // controller.view.frame = ...
            self.scrollView.addSubview(self.ficheTabController.view)
            self.ficheTabController.didMove(toParentViewController: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - MXParallaxHeaderDelegate

    func parallaxHeaderDidScroll(_ parallaxHeader: MXParallaxHeader) {
        //print(parallaxHeader.progress)
//        self.ficheHeaderViewController.setImageHeightMultiplier(parallaxHeader.progress)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == DiscoveryFicheViewController.SegueToPlaylist {
            if let destination: DiscoveryPlaylistTableViewController =
                segue.destination as? DiscoveryPlaylistTableViewController {
                destination.set(playlist: self.selectedPlaylist)
            }
        }
    }
}

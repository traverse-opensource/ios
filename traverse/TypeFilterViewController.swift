//
//  TypeFilterViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 06.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class TypeFilterViewController: FilterViewController, FilterViewTableDelegate {

    private static let TypeFilterNib: String = "TypeFilterCell"
    var filterData: [ItemType] = []

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: TypeFilterViewController.TypeFilterNib,
                                      bundle: nil), forCellReuseIdentifier: TypeFilterViewController.TypeFilterNib)

        self.filterData = [.events, .media, .objects, .people, .places]
        self.tableView.reloadData()

    }

    func getFilterCount() -> Int {
        return filterData.count
    }

    func getFilterViewAt(_ index: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TypeFilterViewController.TypeFilterNib, for: IndexPath(row: index, section: 0)) as! TypeFilterCell
        cell.set(filterData[index])

        if (self.filter?.types.contains(filterData[index]))! {
            tableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.none)
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.filter?.types.append(filterData[indexPath.row])
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.filter?.types = (self.filter?.types.filter({ (type) -> Bool in
            return type != filterData[indexPath.row]
        }))!
    }

}

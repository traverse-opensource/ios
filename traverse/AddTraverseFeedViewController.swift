//
//  AddTraverseFeedViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 17.10.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

private let AddTraverseFeedViewControllerNib: String = "AddTraverseFeedViewController"
private let MaxCharacter: Int = 200

class AddTraverseFeedViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var cancelImage: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var charCounterLabel: UILabel!
    @IBOutlet weak var publishButton: UIButton!

    private let ficheId: String
    private let tag: String
    private let photo: UIImage
    private let userId: String
    private weak var addFeedDelegate: AddFeedDelegate!

    init(ficheId: String, tag: String,
         photo: UIImage, userId: String,
         addFeedDelegate: AddFeedDelegate) {
        self.ficheId = ficheId
        self.tag = tag
        self.photo = photo
        self.userId = userId
        self.addFeedDelegate = addFeedDelegate
        super.init(nibName: AddTraverseFeedViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.commentTextView.delegate = self
        self.commentTextView.text = "\(self.tag) "
        self.charCounterLabel.text = "\(self.commentTextView.text?.utf16.count ?? 0)/\(MaxCharacter)"
        self.imageView.image = self.photo
        self.publishButton.backgroundColor = UIColor.accentColor
        self.cancelImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AddTraverseFeedViewController.onCancel)))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return (textView.text?.utf16.count ?? 0) + text.utf16.count - range.length <= MaxCharacter
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let currentTextCount: Int = textView.text?.utf16.count ?? 0
        self.charCounterLabel.text = "\(currentTextCount)/\(MaxCharacter)"
        if currentTextCount == 0 {
            self.publishButton.backgroundColor = UIColor.primaryDarkColor
            self.publishButton.isEnabled = false
        } else {
            self.publishButton.backgroundColor = UIColor.accentColor
            self.publishButton.isEnabled = true
        }
    }

    @IBAction func publishAction(_ sender: UIButton) {
        self.publishButton.backgroundColor = UIColor.primaryDarkColor
        self.publishButton.isEnabled = false
        FicheAPI.postToFicheFeed(with: self.ficheId, description: self.commentTextView.text, image: self.imageView.image!, userId: self.userId).then(in: .main) { (feed: Feed) in
                self.addFeedDelegate.feedAdded(feed: feed)
            }.catch(in: .main) { (_) -> Void in
                self.showError(NSLocalizedString("An error occured while uploading your comment, retry later!", comment: "An error occured while uploading your comment, retry later!"), true)
            }.always {
                self.dismiss(animated: true, completion: nil)
            }
    }

    func onCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

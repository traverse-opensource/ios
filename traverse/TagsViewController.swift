//
//  TagsViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 27.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import TagCellLayout

private let TagsViewControllerNib: String = "TagsViewController"
private let TagCellNib: String = "TagCell"

class TagsViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var tagsCollectionView: UICollectionView!

    private let tags: [Tag]
    private let theme: ItemTheme

    init(tags: [Tag], theme: ItemTheme) {
        self.tags = tags
        self.theme = theme
        super.init(nibName: TagsViewControllerNib, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Attach datasource and delegate
        self.tagsCollectionView.dataSource  = self
        self.tagsCollectionView.delegate = self

        // Set up the nib for the cell
        let viewNib: UINib = UINib(nibName: TagCellNib, bundle: nil)
        self.tagsCollectionView.register(viewNib, forCellWithReuseIdentifier: TagCellNib)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//    func set(tags: [Tag], theme: ItemTheme) {
//        self.tags = tags
//        self.theme = theme
//        self.tagsCollectionView.reloadData()
//    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return self.tags.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        var cell: UICollectionViewCell = UICollectionViewCell()
        if let tagCell: TagCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: TagCellNib,
                                               for: indexPath) as? TagCell {
            tagCell.set(tag: self.tags[indexPath.row], theme: self.theme)
            cell = tagCell
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let textWidth = TagCellLayout.textWidth("#" + self.tags[indexPath.row].name,
                                                font: UIFont(name: "Roboto-Regular", size: 14.0)!)
        return CGSize(width: textWidth, height: 16.0)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

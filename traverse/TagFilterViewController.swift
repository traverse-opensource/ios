//
//  TagFilterViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 06.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import SearchTextField
import TagCellLayout

class TagFilterViewController: FilterViewController, UICollectionViewDelegate,
    UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {

    private static let TagCell: String = "TagCell"

    @IBOutlet weak var keywordField: SearchTextField!
    @IBOutlet weak var tagCollectionView: UICollectionView!

    private var tags: [Tag] = []
    private var suggestedTags: [Tag] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tagCollectionView.dataSource  = self
        self.tagCollectionView.delegate = self

        // Set up the nib for the cell
        let viewNib: UINib = UINib(nibName: TagFilterViewController.TagCell,
                                   bundle: nil)
        self.tagCollectionView.register(viewNib,
                                         forCellWithReuseIdentifier: TagFilterViewController.TagCell)

        keywordField.theme.cellHeight = 60
        keywordField.theme.bgColor = UIColor.white
        keywordField.theme.font = keywordField.theme.font.withSize(14)
        keywordField.theme.fontColor = UIColor.black
        keywordField.highlightAttributes = [NSForegroundColorAttributeName: UIColor.accentColor, NSFontAttributeName: keywordField.theme.font]

        keywordField?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                               for: .editingChanged)

        keywordField.itemSelectionHandler = {items, itemPosition in

            if self.suggestedTags.count > itemPosition {
                let selectedTag = self.suggestedTags[itemPosition]
                self.filter?.tags.append(selectedTag)

                self.keywordField.text = ""
                self.keywordField.resignFirstResponder()
                self.tagCollectionView.reloadData()
            }
        }

        TagAPI.getTags().then { (tags) in
            self.tags = tags
        }
    }

    func textFieldDidChange(textField: UITextField) {
        self.suggestedTags.removeAll()

        var results: [SearchTextFieldItem] = []

        if !(textField.text?.isEmpty)! {
            self.suggestedTags.append(contentsOf: self.tags.filter({ (tag) -> Bool in

                return tag.name.lowercased().hasPrefix((textField.text?.lowercased())!)
            }))

            for tag in self.suggestedTags {
                let item = SearchTextFieldItem(title: tag.name)
                results.append(item)
            }
        }
        keywordField.filterItems(results)
    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return (self.filter?.tags.count)!
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        var cell: UICollectionViewCell = UICollectionViewCell()
        if let tagCell: TagCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: TagFilterViewController.TagCell,
                                               for: indexPath) as? TagCell {
            tagCell.set(tagWithBorder: (self.filter?.tags[indexPath.row])!)
            cell = tagCell
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let textWidth = TagCellLayout.textWidth("#" + self.filter!.tags[indexPath.row].name,
                                                font: UIFont(name: "Roboto-Regular", size: 14.0)!)
        return CGSize(width: textWidth + 8, height: 24.0)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.filter?.tags.remove(at: indexPath.row)
        collectionView.reloadData()
    }
}

//
//  PlaylistFicheOrderingViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 25.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import SwiftReorder

private let NibName: String = "PlaylistFicheOrderingViewController"

class PlaylistFicheOrderingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, TableViewReorderDelegate {

    private static let RelatedFicheOrderingCell: String = "RelatedFicheOrderingCell"
    @IBOutlet weak var playlistTitleLabel: UILabel!
    @IBOutlet weak var coloredView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelButton: UIImageView!
    
    var playlist: Playlist
    var container: UIViewController

    init(playlist: Playlist, container: UIViewController) {
        self.playlist = playlist
        self.container = container
        super.init(nibName: NibName, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.playlistTitleLabel.text = self.playlist.name

        let theme: ItemTheme = playlist.mainTheme
        if let themeColor: UIColor = theme.getThemeColor() {
            self.coloredView.backgroundColor = themeColor
        }

        self.cancelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PlaylistFicheOrderingViewController.onCancel)))
        tableView.reorder.delegate = self
        tableView.delegate = self
        tableView.dataSource = self

        tableView.register(UINib.init(nibName: PlaylistFicheOrderingViewController.RelatedFicheOrderingCell, bundle: nil), forCellReuseIdentifier: PlaylistFicheOrderingViewController.RelatedFicheOrderingCell)
        tableView.isEditing = true

        //tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView()

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let spacer = tableView.reorder.spacerCell(for: indexPath) {
            return spacer
        }
        if let cell: RelatedFicheOrderingCell =
            tableView.dequeueReusableCell(
                withIdentifier: PlaylistFicheOrderingViewController.RelatedFicheOrderingCell,
                for: indexPath) as? RelatedFicheOrderingCell {
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero

            cell.set(fiche: playlist.linkedFiches![indexPath.row].fiche, index: indexPath.row)

            return cell
        }
        else {
            return UITableViewCell()
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = playlist.linkedFiches?.count ?? 0

        if count == 1 {
            tableView.isEditing = false
        }

        return count
    }

    func tableViewDidFinishReordering(_ tableView: UITableView, from initialSourceIndexPath: IndexPath, to finalDestinationIndexPath: IndexPath) {
        let linkedFiche = playlist.linkedFiches?.remove(at: initialSourceIndexPath.row)
        playlist.linkedFiches?.insert(linkedFiche!, at: finalDestinationIndexPath.row)

        PlaylistAPI.swapFiches(id: playlist.id, ficheIds: (playlist.linkedFiches?.map({ (linkedFiche) -> String in
            return linkedFiche.fiche.id
        }))!).then { (playlist) in
            self.playlist = playlist
            tableView.reloadData()
            }
            .catch { (error) -> Void in
                print(error)
        }
    }

    // this method handles row deletion
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {

            playlist.linkedFiches!.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            PlaylistAPI.swapFiches(id: playlist.id, ficheIds: (playlist.linkedFiches?.map({ (linkedFiche) -> String in
                return linkedFiche.fiche.id
            }))!).then { (playlist) in
                self.playlist = playlist
                tableView.reloadData()
                }
                .catch { (error) -> Void in
                    print(error)
            }

        } else if editingStyle == .insert {
            // Not used in our example, but if you were adding a new row, this is where you would do it.
        }
    }

    func tableView(_ tableView: UITableView, reorderRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
    }

    func onCancel() {
        self.dismiss(animated: true) {
            if let container = self.container as? DiscoveryPlaylistTableViewController {
                container.updatePlaylist(self.playlist)
            }
        }
    }

}

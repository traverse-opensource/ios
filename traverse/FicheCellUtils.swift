//
//  FicheCellUtils.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Foundation
import UIKit

class FicheCellUtils {
    static func setFicheView(fiche: Fiche,
                             index: Int,
                             ficheIconImage: UIImageView,
                             internalCircleView: UIView,
                             counterView: UIView,
                             counterLabel: UILabel,
                             useUserDefault: Bool = true) {
        counterLabel.text = String(index + 1)
        // set the theme
        UIView.setFicheTypeImage(ficheIconImage, fiche: fiche)
        let theme: ItemTheme = fiche.mainTheme
        if let themeColor: UIColor = theme.getThemeColor() {
            // check if fiche has been seen or not
            if useUserDefault {
                if let seen: Bool = UserDefaults.standard.object(forKey: fiche.id) as? Bool {
                    if seen {
                        internalCircleView.backgroundColor = themeColor
                    }
                } else {
                    defaultColorIcon(themeColor,
                                     ficheIconImage: ficheIconImage,
                                     internalCircleView: internalCircleView)
                }
            } else {
                defaultColorIcon(themeColor,
                                 ficheIconImage: ficheIconImage,
                                 internalCircleView: internalCircleView)
            }

            internalCircleView.borderColor = themeColor
            counterView.borderColor = themeColor
            counterLabel.textColor = themeColor
        }
    }

    static func setAsSeen(fiche: Fiche,
                          ficheIconImage: UIImageView,
                          internalCircleView: UIView) {
        let theme: ItemTheme = fiche.mainTheme
        if let themeColor: UIColor = theme.getThemeColor() {
            internalCircleView.backgroundColor = themeColor
            colorIcon(UIColor.white, ficheIconImage: ficheIconImage)
        }
    }

    private static func defaultColorIcon(_ color: UIColor, ficheIconImage: UIImageView, internalCircleView: UIView) {
        internalCircleView.backgroundColor = UIColor.white
        colorIcon(color, ficheIconImage: ficheIconImage)
    }

    private static func colorIcon(_ color: UIColor, ficheIconImage: UIImageView) {
        let tempImage: UIImage = (ficheIconImage.image?.withRenderingMode(.alwaysTemplate))!
        ficheIconImage.image = tempImage
        ficheIconImage.tintColor = color
    }
}

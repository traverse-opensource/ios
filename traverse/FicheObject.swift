//
//  FicheEvent.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class FicheObject: Fiche {
    static let DATE: String = "date"
    static let DELTA_START: String = "delta_start"
    static let HISTORY: String = "history"
    static let TECHNICAL_INFO: String = "technical_information"
    static let MORE_INFO: String = "more_information"

    var history: String?
    var date: FicheDate?
    var deltaStart: Int?
    var technicalInformation: String?
    var moreInformation: String?

    required init?(json: JSON) {
        self.history = FicheObject.HISTORY <~~ json
        self.date = FicheObject.DATE <~~ json
        self.technicalInformation = FicheObject.TECHNICAL_INFO <~~ json
        self.deltaStart = FicheObject.DELTA_START <~~ json
        self.moreInformation = FicheObject.MORE_INFO <~~ json

        super.init(json: json)
    }

    override func toJSON() -> JSON? {

        var jsonThemes: [JSON] = []
        themes.forEach { (theme) in
            jsonThemes.append(theme.toJSON()!)
        }

        var jsonCategories: [JSON] = []
        categories?.forEach { (cat) in
            jsonCategories.append(cat.toJSON()!)
        }

        var jsonTags: [JSON] = []
        tags?.forEach { (tag) in
            jsonTags.append(tag.toJSON()!)
        }

        return jsonify([
            MongoEntity.ID ~~> self.id,
            ItemEntity.TYPE ~~> self.type,
            ItemEntity.NAME ~~> self.name,
            Gloss.Encoder.encode(dateForKey: ItemEntity.CREATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.createdAt),
            Gloss.Encoder.encode(dateForKey: ItemEntity.LAST_UPDATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.lastUpdatedAt),
            ItemEntity.STATUS ~~> self.status,
            ItemEntity.COVER ~~> self.cover?.toJSON(),
            ItemEntity.CREATED_BY ~~> self.createdBy?.toJSON(),
            ItemEntity.LAST_UPDATED_BY ~~> self.lastUpdatedBy?.toJSON(),
            ItemEntity.THEME ~~> self.mainTheme.toJSON(),
            ItemEntity.SUB_CATEGORIES ~~> self.subCategories,
            ItemEntity.LIKES ~~> self.likes,
            Fiche.REFERENCES ~~> self.references,
            Fiche.LIKES ~~> self.city,
            Fiche.LATITUDE ~~> self.latitude,
            Fiche.LONGITUDE ~~> self.longitude,
            Fiche.MONGO_LOCATION ~~> self.mongoLocation?.toJSON(),
            Fiche.GOOGLE_MAP_LOCATION ~~> self.googleMapLocation?.toJSON(),
            Fiche.SHORT_DESCRIPTION ~~> self.shortDescription,
            Fiche.LONG_DESCRIPTION ~~> self.longDescription,
            FicheObject.HISTORY ~~> self.history,
            FicheObject.DATE ~~> self.date?.toJSON(),
            FicheObject.DELTA_START ~~> self.deltaStart,
            FicheObject.TECHNICAL_INFO ~~> self.technicalInformation,
            FicheObject.MORE_INFO ~~> self.moreInformation,
            [ItemEntity.CATEGORIES: jsonCategories],
            [ItemEntity.TAGS: jsonTags],
            [Fiche.THEMES: jsonThemes]
            ])
    }
}

//
//  FicheEvent.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class FicheMedia: Fiche {
    static let PATH: String = "path"
    static let DATE: String = "date"
    static let DELTA_START: String = "delta_start"
    static let ATTACHMENTS: String = "attachments"

    var path: String?
    var date: FicheDate?
    var deltaStart: Int?
    let attachments: String

    required init?(json: JSON) {
        guard let attachments: String = FicheMedia.ATTACHMENTS <~~ json
            else {
                return nil
        }

        self.path = FicheMedia.PATH <~~ json
        self.date = FicheMedia.DATE <~~ json
        self.attachments = attachments
        self.deltaStart = FicheMedia.DELTA_START <~~ json

        super.init(json: json)
    }

    override func toJSON() -> JSON? {

        var jsonThemes: [JSON] = []
        themes.forEach { (theme) in
            jsonThemes.append(theme.toJSON()!)
        }

        var jsonCategories: [JSON] = []
        categories?.forEach { (cat) in
            jsonCategories.append(cat.toJSON()!)
        }

        var jsonTags: [JSON] = []
        tags?.forEach { (tag) in
            jsonTags.append(tag.toJSON()!)
        }

        return jsonify([
            MongoEntity.ID ~~> self.id,
            ItemEntity.TYPE ~~> self.type,
            ItemEntity.NAME ~~> self.name,
            Gloss.Encoder.encode(dateForKey: ItemEntity.CREATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.createdAt),
            Gloss.Encoder.encode(dateForKey: ItemEntity.LAST_UPDATED_AT, dateFormatter: MongoEntity.getDateFormatter())(self.lastUpdatedAt),
            ItemEntity.STATUS ~~> self.status,
            ItemEntity.COVER ~~> self.cover?.toJSON(),
            ItemEntity.CREATED_BY ~~> self.createdBy?.toJSON(),
            ItemEntity.LAST_UPDATED_BY ~~> self.lastUpdatedBy?.toJSON(),
            ItemEntity.THEME ~~> self.mainTheme.toJSON(),
            ItemEntity.SUB_CATEGORIES ~~> self.subCategories,
            ItemEntity.LIKES ~~> self.likes,
            Fiche.REFERENCES ~~> self.references,
            Fiche.LIKES ~~> self.city,
            Fiche.LATITUDE ~~> self.latitude,
            Fiche.LONGITUDE ~~> self.longitude,
            Fiche.MONGO_LOCATION ~~> self.mongoLocation?.toJSON(),
            Fiche.GOOGLE_MAP_LOCATION ~~> self.googleMapLocation?.toJSON(),
            Fiche.SHORT_DESCRIPTION ~~> self.shortDescription,
            Fiche.LONG_DESCRIPTION ~~> self.longDescription,
            FicheMedia.PATH ~~> self.path,
            FicheMedia.DATE ~~> self.date?.toJSON(),
            FicheMedia.DELTA_START ~~> self.deltaStart,
            FicheMedia.ATTACHMENTS ~~> self.attachments,
            [ItemEntity.CATEGORIES: jsonCategories],
            [ItemEntity.TAGS: jsonTags],
            [Fiche.THEMES: jsonThemes]
            ])
    }
}

//
//  MixAPI.swift
//  traverse
//
//  Created by Mattia Gustarini on 18.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Hydra

class MixAPI {
    static func getHeartstrokes() -> Promise<[ItemEntity]> {
        return ApiRequest.request(array: MixRouter.getHeartstrokes(), type: MixFactory.self)
            .then(in: .background, { (factories: [MixFactory]) -> (Promise<[ItemEntity]>) in
                return Promise<[ItemEntity]>(in: .background, { resolve, reject, _ in
                    var items: [ItemEntity] = []
                    for factory: MixFactory in factories {
                        if let item: ItemEntity = factory.getItem() {
                            items.append(item)
                        } else {
                            reject(APIError.decode(what: "Mixin"))
                        }
                    }
                    resolve(items)
                })
            })
    }

    static func getMixins() -> Promise<[ItemEntity]> {
        return ApiRequest.request(array: MixRouter.getMixins(), type: MixFactory.self)
        .then(in: .background, { (factories: [MixFactory]) -> (Promise<[ItemEntity]>) in
            return Promise<[ItemEntity]>(in: .background, { resolve, reject, _ in
                var items: [ItemEntity] = []
                for factory: MixFactory in factories {
                    if let item: ItemEntity = factory.getItem() {
                        items.append(item)
                    } else {
                        reject(APIError.decode(what: "Mixin"))
                    }
                }
                resolve(items)
            })
        })
    }

    static func getMixins(with maxDistance: Int,
                               latitude: Double,
                               longitude: Double,
                               page: Int,
                               limit: Int,
                               includePlaylists: Bool = false, filter: Filter?) -> Promise<[ItemEntity]> {
        return ApiRequest.request(array: MixRouter.getMixinsWithParams(
            maxDistance: maxDistance,
            latitude: latitude,
            longitude: longitude,
            page: page,
            limit: limit,
            includePlaylists: includePlaylists,
            filter: filter), type: MixFactory.self)
            .then(in: .background, { (factories: [MixFactory]) -> (Promise<[ItemEntity]>) in
                return Promise<[ItemEntity]>(in: .background, { resolve, reject, _ in
                    var items: [ItemEntity] = []
                    for factory: MixFactory in factories {
                        if let item: ItemEntity = factory.getItem() {
                            items.append(item)
                        } else {
                            reject(APIError.decode(what: "Mixin"))
                        }
                    }
                    resolve(items)
                })
            })
    }

    static func getFichesOnly() -> Promise<[Fiche]> {
        return ApiRequest.request(array: MixRouter.getMixinsWithoutPlaylists(), type: MixFactory.self)
        .then(in: .background, { (factories: [MixFactory]) -> (Promise<[Fiche]>) in
            return Promise<[Fiche]>(in: .background, { resolve, reject, _ in
                if let mixList: MixList = MixList(mixItems: factories) {
                    resolve(mixList.fiches)
                } else {
                    reject(APIError.decode(what: "Fiches"))
                }
            })
        })
    }

    static func getPlaylistsOnly() -> Promise<[Playlist]> {
        return ApiRequest.request(array: MixRouter.getMixins(), type: MixFactory.self)
            .then(in: .background, { (factories: [MixFactory]) -> (Promise<[Playlist]>) in
                return Promise<[Playlist]>(in: .background, { resolve, reject, _ in
                    if let mixList: MixList = MixList(mixItems: factories) {
                        resolve(mixList.playlists)
                    } else {
                        reject(APIError.decode(what: "Playlists"))
                    }
                })
            })
    }
}

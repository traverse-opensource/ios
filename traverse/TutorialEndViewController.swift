//
//  TutorialEndViewController.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 20.11.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class TutorialEndViewController: UIViewController {

    @IBOutlet weak var endButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(red: 63/255, green: 188/255, blue: 167/255, alpha: 1)
    }

    @IBAction func onClickedEnd(_ sender: Any) {

        self.dismiss(animated: true, completion: nil)
    }
}



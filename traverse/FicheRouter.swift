//
//  FicheRouter.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Alamofire

enum FicheRouter: URLRequestConvertible {
    case getFiches()
    case getFiche(id: String)
    case getRelatedPlaylists(id: String)
    case likeFiche(id: String, userId: String)
    case dislikeFiche(id: String, userId: String)
    case postToFicheFeed(id: String, description: String, userId: String)
    var method: HTTPMethod {
        switch self {
        case .getFiches:
            return .get
        case .getFiche:
            return .get
        case .getRelatedPlaylists:
            return .get
        case .dislikeFiche:
            return .put
        case .likeFiche:
            return .put
        case .postToFicheFeed:
            return .post
        }
    }

    var path: String {
        switch self {
        case .getFiches:
            return "/fiches"
        case .getFiche(let id):
            return "/fiches/\(id)"
        case .getRelatedPlaylists(let id):
            return "/fiches/\(id)/playlists"
        case .likeFiche(let id, _):
            return "/fiches/\(id)/like"
        case .dislikeFiche(let id, _):
            return "/fiches/\(id)/dislike"
        case .postToFicheFeed(let id, _, _):
            return "/fiches/\(id)/feeds"
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Const.API_URL.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        // if arguments are needed
        var parameters: Parameters!
        switch self {
        case .likeFiche(_, let userId):
            parameters = ["objectId": userId]
        case .dislikeFiche(_, let userId):
            parameters = ["objectId": userId]
        default:
            break
        }

        if parameters != nil {
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }

        return urlRequest
    }
}

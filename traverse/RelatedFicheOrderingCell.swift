//
//  RelatedFicheOrderingCell.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 25.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class RelatedFicheOrderingCell: UITableViewCell {

    @IBOutlet weak var ficheTitleLabel: UILabel!
    @IBOutlet weak var internalCircleView: UIView!
    @IBOutlet weak var ficheIconImage: UIImageView!
    @IBOutlet weak var counterView: UIView!
    @IBOutlet weak var counterLabel: UILabel!
    
    private var fiche: Fiche!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(fiche: Fiche, index: Int) {
        self.fiche = fiche

        self.ficheTitleLabel.text = self.fiche.name

        FicheCellUtils.setFicheView(fiche: self.fiche,
                                    index: index,
                                    ficheIconImage: self.ficheIconImage,
                                    internalCircleView: self.internalCircleView,
                                    counterView: self.counterView,
                                    counterLabel: self.counterLabel)
    }
    
}

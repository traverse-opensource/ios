//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class Category: MongoEntity {
    static let NAME: String = "name"
    static let SUB_CATEGORIES: String = "sousCategories"

    let name: String
    let subCategories: [String]

    required init?(json: JSON) {
        guard let name: String = Category.NAME <~~ json,
            let subCategories: [String] = Category.SUB_CATEGORIES <~~ json
            else {
                return nil
        }

        self.name = name
        self.subCategories = subCategories

        super.init(json: json)
    }

    func toJSON() -> JSON? {

        return jsonify([
            "_id" ~~> self.id,
            "name" ~~> self.name,
            "sousCategories" ~~> self.subCategories
            ])
    }
}

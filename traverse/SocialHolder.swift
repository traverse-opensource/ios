//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class SocialHolder: Gloss.Decodable {
    static let WEB: String = "web"
    static let FACEBOOK: String = "facebook"
    static let TWITTER: String = "twitter"
    static let INSTAGRAM: String = "instagram"

    var web: WebHolder?
    var facebook: SocialTagsHolder?
    var twitter: SocialTagsHolder?
    var instagram: SocialTagsHolder?

    required init?(json: JSON) {
//        guard let web: WebHolder = SocialHolder.WEB <~~ json
////            let facebook: SocialTagsHolder = SocialHolder.FACEBOOK <~~ json,
////            let twitter: SocialTagsHolder = SocialHolder.TWITTER <~~ json,
////            let instagram: SocialTagsHolder = SocialHolder.INSTAGRAM <~~ json
//            else {
//                return nil
//        }

        self.web = SocialHolder.WEB <~~ json
        self.facebook = SocialHolder.FACEBOOK <~~ json
        self.twitter = SocialHolder.TWITTER <~~ json
        self.instagram = SocialHolder.INSTAGRAM <~~ json
    }
}

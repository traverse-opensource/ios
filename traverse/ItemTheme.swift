//
//  ItemTheme.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class ItemTheme: Gloss.Decodable {
    static let HEX_COLOR: String = "color"
    static let PONDERATION: String = "ponderation"
    static let DESCRIPTION: String = "description"
    static let NAME: String = "name"

    var hexColor: String?
    var ponderation: Int?
    var description: String?
    var name: String?

    required init?(json: JSON) {
        
        self.hexColor = ItemTheme.HEX_COLOR <~~ json
        self.ponderation = ItemTheme.PONDERATION <~~ json
        self.description = ItemTheme.DESCRIPTION <~~ json
        self.name = ItemTheme.NAME <~~ json
    }

    func getThemeColor() -> UIColor? {
        if let name: String = self.name {
            if let hexColor: String = self.hexColor {
                return UIColor(hexString: hexColor)!
            }
            return nil
        }
        return nil
    }

    func toJSON() -> JSON? {

        return jsonify([
            ItemTheme.HEX_COLOR ~~> self.hexColor,
            ItemTheme.PONDERATION ~~> self.ponderation,
            ItemTheme.DESCRIPTION ~~> self.description,
            ItemTheme.NAME ~~> self.name
        ])
    }
}

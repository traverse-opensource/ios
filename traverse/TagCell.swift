//
//  TagCell.swift
//  traverse
//
//  Created by Mattia Gustarini on 27.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class TagCell: UICollectionViewCell {

    @IBOutlet weak var tagLabel: UILabel!
    private var tagItem: Tag!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(tag: Tag, theme: ItemTheme) {
        self.tagItem = tag
        self.tagLabel.text = "#" + tag.name
        if let themeColor: UIColor = theme.getThemeColor() {
            self.tagLabel.textColor = themeColor
        }
    }

    func set(tagWithBorder: Tag) {
        self.tagItem = tagWithBorder
        self.tagLabel.text = "#" + tagWithBorder.name

        self.tagLabel.textColor = UIColor.accentColor
        self.tagLabel.backgroundColor = UIColor.white
        self.tagLabel.borderWidth = 1
        self.tagLabel.borderColor = UIColor.accentColor
        self.tagLabel.cornerRadius = 4
    }
}

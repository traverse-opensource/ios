//
//  Role.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class UserProfile: Gloss.Decodable {
    static let NAME: String = "name"
    static let GENDER: String = "gender"
    static let LOCATION: String = "location"
    static let WEBSITE: String = "website"
    static let PICTURE: String = "picture"

    var name: String?
    var gender: String?
    var location: String?
    var website: String?
    var picture: String?

    required init?(json: JSON) {
//        guard let name: String = UserProfile.NAME <~~ json,
//            let gender: String = UserProfile.GENDER <~~ json,
//            let location: String = UserProfile.LOCATION <~~ json,
//            let website: String = UserProfile.WEBSITE <~~ json,
//            let picture: String = UserProfile.PICTURE <~~ json
//        else {
//            return nil
//        }

        self.name = UserProfile.NAME <~~ json
        self.gender = UserProfile.GENDER <~~ json
        self.location = UserProfile.LOCATION <~~ json
        self.website = UserProfile.WEBSITE <~~ json
        self.picture = UserProfile.PICTURE <~~ json
    }

    func getPictureUrl() -> URL? {
        if let picture: String = self.picture {
            if !picture.isEmpty {
                if picture.hasPrefix("http") || picture.hasPrefix("https") {
                    return URL(string: picture)!
                } else {
                    return URL(string: Const.BASE_URL + picture)!
                }
            } else {
                return nil
            }
        }
        return nil
    }

    func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> self.name,
            "gender" ~~> self.gender,
            "location" ~~> self.location,
            "website" ~~> self.location,
            "picture" ~~> self.picture
        ])
    }
}

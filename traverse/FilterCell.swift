//
//  FilterCell.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 12.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit

class FilterCell: UICollectionViewCell {

    @IBOutlet weak var labelLabel: UILabel!
    @IBOutlet weak var closeImage: UIImageView!
    @IBOutlet weak var coloredView: UIView!

    private var label: String!
    var filter: Any?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        coloredView.borderWidth = 1
    }

    func set(label: String, filter: Any) {
        self.filter = filter
        self.label = label
        self.labelLabel.text = label

        switch filter {
        case is ItemType:
            coloredView.backgroundColor = UIColor.accentColor
            coloredView.borderColor = UIColor.accentColor
            labelLabel.textColor = UIColor.white
            closeImage.image = #imageLiteral(resourceName: "ic_cancel").withRenderingMode(.alwaysTemplate).tint(with: UIColor.white)
            break
        case is Theme:
            coloredView.backgroundColor = UIColor(hexString: (filter as! Theme).hexColor)
            coloredView.borderColor = UIColor(hexString: (filter as! Theme).hexColor)
            labelLabel.textColor = UIColor.white
            closeImage.image = #imageLiteral(resourceName: "ic_cancel").withRenderingMode(.alwaysTemplate).tint(with: UIColor.white)
            break
        case is Tag:
            coloredView.backgroundColor = UIColor.white
            coloredView.borderColor = UIColor.accentColor
            labelLabel.textColor = UIColor.accentColor
            closeImage.image = #imageLiteral(resourceName: "ic_cancel").withRenderingMode(.alwaysTemplate).tint(with: UIColor.accentColor)
            break
        case is Category:
            coloredView.backgroundColor = UIColor.accentColor
            coloredView.borderColor = UIColor.accentColor
            labelLabel.textColor = UIColor.white
            closeImage.image = #imageLiteral(resourceName: "ic_cancel").withRenderingMode(.alwaysTemplate).tint(with: UIColor.white)
            break
        default:
            coloredView.backgroundColor = UIColor.accentColor
            coloredView.borderColor = UIColor.accentColor
            labelLabel.textColor = UIColor.white
            closeImage.image = #imageLiteral(resourceName: "ic_cancel").withRenderingMode(.alwaysTemplate).tint(with: UIColor.white)
            break
        }
        //        self.labelLabel.textColor = UIColor(contrastingBlackOrWhiteColorOn: themeColor, isFlat: true)
    }
}

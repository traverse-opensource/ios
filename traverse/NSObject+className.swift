//
//  NSObject+className.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 25.08.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

extension NSObject {
    var className: String {
        return NSStringFromClass(type(of: self))
    }
}

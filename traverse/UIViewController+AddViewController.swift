//
//  UIViewController+AddViewController.swift
//  traverse
//
//  Created by Mattia Gustarini on 13.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Material

extension UIViewController {
    func addViewController(_ controller: UIViewController) {
        self.addChildViewController(controller)
        controller.didMove(toParentViewController: self)
    }
}

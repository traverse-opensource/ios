//
//  CategoryAPI.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Hydra

class CategoryAPI {
    static func getCategories() -> Promise<[Category]> {
        return ApiRequest.request(array: CategoryRouter.getCategories(), type: Category.self)
    }
}

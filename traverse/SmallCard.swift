//
//  PlaylistSmallCard.swift
//  traverse
//
//  Created by Mattia Gustarini on 19.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import UIKit
import Haneke
import ChameleonFramework

class SmallCard: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var ficheCounterView: UIView!
    @IBOutlet weak var ficheTypeView: UIView!
    @IBOutlet weak var ficheCounterLabel: UILabel!
    @IBOutlet weak var ficheIcon: UIImageView!
    @IBOutlet weak var themeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    private var item: ItemEntity!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(item: ItemEntity) {
        self.item = item

        if let cover: Cover = item.cover {
            if let fiche = item as? Fiche {
                self.image.hnk_setImageFromURL(cover.getThumbImageUrl(), placeholder: UIView.getFicheTypeImage(fiche: fiche), format: nil, failure: nil, success: {(img) in
                    self.image.image = img
                    self.image.contentMode = .scaleAspectFill
                })
            }
            else {
                self.image.hnk_setImageFromURL(cover.getThumbImageUrl(), placeholder: #imageLiteral(resourceName: "ic_playlist"), format: nil, failure: nil, success: {(img) in
                    self.image.image = img
                    self.image.contentMode = .scaleAspectFill
                })
            }
        }


        self.titleLabel.text = item.name

        switch item.type {
        case .playlist:
            setPlaylistSpecific()
        default:
            setFicheSpecific()
        }
    }

    private func setPlaylistSpecific() {
        if let playlist: Playlist = self.item as? Playlist {
            if playlist.isIdsOnly() {
                self.ficheCounterLabel.text = "\(playlist.linkedFichesIds!.count) fiches"
            } else {
                self.ficheCounterLabel.text = "\(playlist.linkedFiches!.count) fiches"
            }

            self.ficheCounterView.isHidden = false
            self.ficheTypeView.isHidden = true
            UIView.setTheme(playlist: playlist,
                            separatorView: self.separatorView,
                            iconView: self.ficheCounterView,
                            themeNameLabel: self.themeLabel,
                            ficheCounterLabel: self.ficheCounterLabel)
        }
    }

    private func setFicheSpecific() {
        if let fiche: Fiche = self.item as? Fiche {
            UIView.setFicheTypeImage(self.ficheIcon, fiche: fiche)
            self.ficheTypeView.isHidden = false
            self.ficheCounterView.isHidden = true
            UIView.setTheme(fiche: fiche,
                            separatorView: self.separatorView,
                            iconView: self.ficheTypeView,
                            themeNameLabel: self.themeLabel)
        }
    }
}

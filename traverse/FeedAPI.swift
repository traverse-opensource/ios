//
//  FeedAPI.swift
//  traverse
//
//  Created by Jérôme Marchanoff on 28.09.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Hydra

class FeedAPI {
    static func getFeedForFiche(id: String) -> Promise<[Feed]> {
        return ApiRequest.request(array: FeedRouter.getFeedForFiche(id: id), type: Feed.self)
    }

    static func flagFeed(id: String) -> Promise<Any> {
        return ApiRequest.request(object: FeedRouter.flagFeed(id: id))
    }
}

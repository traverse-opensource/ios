//
//  RoleRouter.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Alamofire

enum RoleRouter: URLRequestConvertible {
    case getRoles()

    var method: HTTPMethod {
        switch self {
        case .getRoles:
            return .get
        }
    }

    var path: String {
        switch self {
        case .getRoles:
            return "/roles"
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Const.API_URL.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        // if arguments are needed
        //        switch self {
        //        case .createUser(let parameters):
        //            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        //        case .updateUser(_, let parameters):
        //            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        //        default:
        //            break
        //        }

        return urlRequest
    }
}

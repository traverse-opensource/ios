//
//  FicheEvent.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class FicheDate: Gloss.Decodable {
    static let DATE: String = "date"
    static let DESCRIPTION: String = "description"

    let date: Date
    let description: String

    required init?(json: JSON) {
        guard let _: String = FicheDate.DATE <~~ json,
            let description: String = FicheDate.DESCRIPTION <~~ json
            else {
                return nil
        }

        let dateFormatter: DateFormatter = MongoEntity.getDateFormatter()
        self.date =
            Decoder.decode(dateForKey: FicheDate.DATE, dateFormatter: dateFormatter)(json)!
        self.description = description
    }

    func toJSON() -> JSON? {
        return jsonify([
            FicheDate.DESCRIPTION ~~> self.description,
            Gloss.Encoder.encode(dateForKey: FicheDate.DATE, dateFormatter: MongoEntity.getDateFormatter())(self.date)
            ])
    }
}

//
//  FicheFactory.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import Gloss

class FicheFactory: Gloss.Decodable {
    let ficheJson: JSON
    let type: ItemType

    required init?(json: JSON) {
        guard let type: ItemType = ItemEntity.TYPE <~~ json
            else {
                return nil
        }

        self.ficheJson = json
        self.type = type
    }

    func getFiche() -> Fiche? {
        switch type {
        case .events:
            return FicheEvent(json: self.ficheJson)
        case .media:
            return FicheMedia(json: self.ficheJson)
        case .objects:
            return FicheObject(json: self.ficheJson)
        case .people:
            return FichePeople(json: self.ficheJson)
        case .places:
            return FichePlace(json: self.ficheJson)
        default:
            return nil
        }
    }
}

//
//  roleTest.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import XCTest
import Hydra
@testable import traverse

class UserModelRemoteTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        continueAfterFailure = false
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.

        super.tearDown()
    }

    func testRoleRemote() {
        // given
        var role: Role!
        let promise = expectation(description: "Wait for a tole from server")
        // when
        RoleAPI.getRoles().then { (roles: [Role]) in
            role = roles[roles.count - 1]
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(role, "Role must not be nil")
        XCTAssertNotNil(role.id, "Role must have an id")
//        XCTAssertNotNil(role.title, "Role must have a title")
//        XCTAssertNotNil(role.description, "Role must have a description")
    }

    func testGroupRemote() {
        // given
        var group: Group!
        let promise = expectation(description: "Wait for a group from server")
        // when
        GroupAPI.getGroups().then { (groups: [Group]) in
            group = groups[groups.count - 1]
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(group, "Group must not be nil")
        XCTAssertNotNil(group.id, "Group must have an id")
//        XCTAssertNotNil(group.name, "Group must have a name")
//        XCTAssertNotNil(group.description, "Group must have a description")
    }

    func testUserProfileRemote() {
        // given
        var userProfile: UserProfile!
        let promise = expectation(description: "Wait for a user from server")
        // when
        UserAPI.getUsers(page: 1, limit: 1).then { (users: [User]) in
            userProfile = users[0].profile
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(userProfile, "User profile must not be nil")
//        XCTAssertNotNil(userProfile.name, "User profile must have a name")
//        XCTAssertNotNil(userProfile.gender, "User profile must have a gender")
//        XCTAssertNotNil(userProfile.location, "User profile must have a location")
//        XCTAssertNotNil(userProfile.website, "User profile must have a website")
//        XCTAssertNotNil(userProfile.picture, "User profile must have a picture")
        // checking URL
        if userProfile.picture != nil &&
            !(userProfile.picture?.isEmpty)! {
            XCTAssertNotNil(userProfile.getPictureUrl(), "If picture is not nil or empty a valid URL must be provided")
        } else {
            XCTAssertNil(userProfile.getPictureUrl(), "If picture is nil the URL must be nil")
        }
    }

    func testUserRemote() {
        // given
        var user: User!
        let promise = expectation(description: "Wait for a user from server")
        // when
        UserAPI.getUsers(page: 1, limit: 1).then { (users: [User]) in
            user = users[0]
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(user, "User must not be nil")
        XCTAssertNotNil(user.id, "User must have an id")
//        XCTAssertNotNil(user.email, "User must have an email")
//        XCTAssertNotNil(user.role, "User must have a role")
        XCTAssertNotNil(user.groups, "User must have groups")
//        XCTAssertNotNil(user.profile, "User must have a profile")
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
}

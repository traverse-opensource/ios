//
//  ItemModelTests.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import XCTest
import Hydra
@testable import traverse

class ItemModelRemoteTests: XCTestCase {
    private static let ficheId: String = "591c434afda54c285ef9a0b4"

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        continueAfterFailure = false
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testThemeRemote() {
        // given
        var theme: Theme!
        let promise = expectation(description: "Wait for a theme from server")
        // when
        ThemeAPI.getThemes().then { (themes: [Theme]) in
            theme = themes[themes.count - 1]
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(theme, "Theme must not be nil")
        XCTAssertNotNil(theme.id, "Theme must have an id")
        XCTAssertNotNil(theme.hexColor, "Theme must have a color")
        XCTAssertNotNil(theme.name, "Theme must have a name")
        XCTAssertNotNil(theme.description, "Theme must have a description")
    }

    func testItemThemeRemote() {
        // given
        var theme: ItemTheme!
        let promise = expectation(description: "Wait for a theme from server")
        // when
        FicheAPI.getFiche(with: ItemModelRemoteTests.ficheId).then { (newfiche: Fiche) in
            theme = newfiche.mainTheme
            }.always {
                promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(theme, "Theme must not be nil")
        //        XCTAssertNotNil(theme.hexColor, "Theme must have a color")
        //        XCTAssertNotNil(theme.name, "Theme must have a name")
        //        XCTAssertNotNil(theme.description, "Theme must have a description")
        if theme.name != nil && theme.hexColor != nil {
            XCTAssertNotNil(theme.getThemeColor(), "If theme has a name and a hex color we must return a valid UIColor")
        } else {
            XCTAssertNil(theme.getThemeColor(), "If theme has a no name and/or a hex color we must return nil")
        }
    }

    func testCoverRemote() {
        // given
        var cover: Cover!
        let promise = expectation(description: "Wait for a cover from server")
        // when
        FicheAPI.getFiche(with: ItemModelRemoteTests.ficheId).then { (newfiche: Fiche) in
            cover = newfiche.cover
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(cover, "Cover must not be nil")
        XCTAssertNotNil(cover.getImageUrl(), "Cover image URL must not be nil")
//        XCTAssertNotNil(cover.getFieldName(), "Cover must have a field name")
//        XCTAssertNotNil(cover.getOriginalName(), "Cover must have an original name")
//        XCTAssertNotNil(cover.getEncoding(), "Cover must have an encoding")
//        XCTAssertNotNil(cover.getMimeType(), "Cover must have a mimetype")
//        XCTAssertNotNil(cover.getDestination(), "Cover must have a destination")
//        XCTAssertNotNil(cover.getPath(), "Cover must have a path")
//        XCTAssertNotNil(cover.getSize(), "Cover must have a size")
//        XCTAssertNotNil(cover.getCoverCredit(), "Cover must have a cover credit")
    }

    func testCategoryRemote() {
        // given
        var category: traverse.Category!
        let promise = expectation(description: "Wait for a category from server")
        // when
        CategoryAPI.getCategories().then { (categories: [traverse.Category]) in
            category = categories[categories.count - 1]
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(category, "Category must not be nil")
        XCTAssertNotNil(category.id, "Category must have an id")
        XCTAssertNotNil(category.name, "Category must have a name")
        XCTAssertNotNil(category.subCategories, "Category must have subcategories")
    }

    func testTagRemote() {
        // given
        var tag: Tag!
        let promise = expectation(description: "Wait for a tag from server")
        // when
        TagAPI.getTags().then { (tags: [Tag]) in
            tag = tags[tags.count - 1]
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(tag, "Tag must not be nil")
        XCTAssertNotNil(tag.id, "Tag must have an id")
        XCTAssertNotNil(tag.name, "Tag must have a name")
        XCTAssertNotNil(tag.count, "Tag must have a count")
    }

    func testMongoLocationRemote() {
        // given
        var location: MongoLocation!
        let promise = expectation(description: "Wait for a mongo location from server")
        // when
        FicheAPI.getFiche(with: ItemModelRemoteTests.ficheId).then { (newfiche: Fiche) in
            location = newfiche.mongoLocation
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(location, "Mongo Location must not be nil")
//        XCTAssertNotNil(location.type, "Mongo Location must have a type")
//        XCTAssertNotNil(location.coordinates, "Mongo Location must have coordinates")
    }

    func testGoogleMapLocationRemote() {
        // given
        var location: GoogleMapLocation!
        let promise = expectation(description: "Wait for a Google Map location from server")
        // when
        FicheAPI.getFiche(with: ItemModelRemoteTests.ficheId).then { (newfiche: Fiche) in
            location = newfiche.googleMapLocation
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(location.placeId, "Google Map Location must have a placeId")
        XCTAssertNotNil(location.fullAddrStr, "Google Map Location must have an address")
//        XCTAssertNotNil(location.getLatitude(), "Google Map Location must have a latitude")
//        XCTAssertNotNil(location.getLongitude(), "Google Map Location must have a longitude")
    }

    func testWebHolderRemote() {
        // given
        var webHolder: WebHolder!
        let promise = expectation(description: "Wait for a web holder from server")
        // when
        FicheAPI.getFiche(with: ItemModelRemoteTests.ficheId).then { (newfiche: Fiche) in
            webHolder = newfiche.socialHolder?.web
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(webHolder, "Web holder must not be nil")
//        XCTAssertNotNil(webHolder.getLink(), "Web holder must have a link")
    }

//    func testSocialTagRemote() {
//        // given
//        var socialTag: SocialTag!
//        let promise = expectation(description: "Wait for a social tag from server")
//        // when
//        FicheAPI.getFiche(with: ItemModelRemoteTests.ficheId).then { (newfiche: Fiche) in
//            let tags: [SocialTag] = newfiche.socialHolder.getFacebook().getTags()
//            if !tags.isEmpty {
//                socialTag = tags[tags.count - 1]
//            }
//        }.always {
//            promise.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        // then
//        //XCTAssertNotNil(socialTag, "Social must not be nil")
//        if socialTag !=  nil {
//            XCTAssertNotNil(socialTag.value, "Social tag must have a value")
//        }
//    }

//    func testSocialTagsHolderRemote() {
//        // given
//        var socialTagsHolder: SocialTagsHolder!
//        let promise = expectation(description: "Wait for a social tags holder from server")
//        // when
//        FicheAPI.getFiche(with: ItemModelRemoteTests.ficheId).then { (newfiche: Fiche) in
//            socialTagsHolder = newfiche.socialHolder?.facebook
//        }.always {
//            promise.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        // then
//        XCTAssertNotNil(socialTagsHolder, "Social tag holder must not be nil")
////        XCTAssertNotNil(socialTagsHolder.getTags(), "Social tags holder must have tags")
//    }

    func testSocialHolderRemote() {
        // given
        var socialHolder: SocialHolder!
        let promise = expectation(description: "Wait for a social holder from server")
        // when
        FicheAPI.getFiche(with: ItemModelRemoteTests.ficheId).then { (newfiche: Fiche) in
            socialHolder = newfiche.socialHolder
            }.always {
                promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(socialHolder, "Social holder must not be nil")
//        XCTAssertNotNil(socialHolder.getWeb(), "Social tags holder must have a web holder")
//        XCTAssertNotNil(socialHolder.getFacebook(), "Social tags holder must have facebook")
//        XCTAssertNotNil(socialHolder.getTwitter(), "Social tags holder must have twitter")
//        XCTAssertNotNil(socialHolder.getInstagram(), "Social tags holder must have instagram")
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
}

//
//  PlaylistModelTests.swift
//  traverse
//
//  Created by Mattia Gustarini on 12.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import XCTest
@testable import traverse

class PlaylistModelRemoteTests: XCTestCase {
    private static let playlistId: String = "5963f3b4239bd644b0ec3115"

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        continueAfterFailure = false
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testLinkedFicheRemote() {
        // given
        var playlist: Playlist!
        let promise = expectation(description: "Wait for a linked fiche from server")
        // when
        PlaylistAPI.getPlaylist(with: PlaylistModelRemoteTests.playlistId).then { (newPlaylist: Playlist) in
            playlist = newPlaylist
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 10, handler: nil)

        //then
        XCTAssertNotNil(playlist, "Playlist must not be nil")

        if playlist.isIdsOnly() {
            let linkedFicheId: String = playlist.linkedFichesIds![0]
            XCTAssertNotNil(linkedFicheId, "Linked Fiche Id must not be nil")
        } else {
            let linkedFiche: LinkedFiche = playlist.linkedFiches![0]
            XCTAssertNotNil(linkedFiche, "Linked Fiche must not be nil")
            XCTAssertNotNil(linkedFiche.fiche, "Linked Fiche must have a fiche")
        }
    }

//    func testLabelRemote() {
//        // given
//        var label: Label!
//        let promise = expectation(description: "Wait for a label from server")
//        // when
//        PlaylistAPI.getPlaylist(with: PlaylistModelRemoteTests.playlistId).then { (playlist: Playlist) in
//            label = playlist.label
//        }.always {
//                promise.fulfill()
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        //then
//        XCTAssertNotNil(label, "Label must not be nil")
//        XCTAssertNotNil(label.categories, "Label must have categories")
//        XCTAssertNotNil(label.subCategories, "Label must have sub categories")
//        XCTAssertNotNil(label.tags, "Label must have tags")
//    }

    func testPlaylistRemote() {
        // given
        var playlist: Playlist!
        let promise = expectation(description: "Wait for a playlist from server")
        // when
        PlaylistAPI.getPlaylist(with: PlaylistModelRemoteTests.playlistId).then { (newPlaylist: Playlist) in
            playlist = newPlaylist
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        //then
        XCTAssertNotNil(playlist, "Playlist must not be nil")
        XCTAssertNotNil(playlist.type, "Playlist must have a type")
        XCTAssertEqual(playlist.type, ItemType.playlist, "Playlist must have a type of playlist")
        XCTAssertNotNil(playlist.name, "Playlist must have a name")
        XCTAssertNotNil(playlist.createdAt, "Playlist must have a create at date")
        XCTAssertNotNil(playlist.status, "Playlist must have a status")
        XCTAssertNotNil(playlist.cover, "Playlist must have a cover")
        XCTAssertNotNil(playlist.mainTheme, "Playlist must have a theme")
        XCTAssertNotNil(playlist.description, "Playlist must have a description")
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
}

//
//  FicheModelTests.swift
//  traverse
//
//  Created by Mattia Gustarini on 11.07.17.
//  Copyright © 2017 Mattia Gustarini. All rights reserved.
//

import XCTest
import Hydra
@testable import traverse

class FicheModelRemoteTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        continueAfterFailure = false
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAllFichesRemote() {
        // given
        var fiches: [Fiche]!
        let promise = expectation(description: "Wait for a fiches from server")
        // when
        MixAPI.getFichesOnly().then { (newFiches: [Fiche]) in
            fiches = newFiches
        }.always {
                promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        // then
        XCTAssertNotNil(fiches, "Fiches must not be nil")
        checkFicheAssertions(fiches: fiches)
    }

    func checkFicheAssertions(fiches: [Fiche]) {
        for fiche: Fiche in fiches {
            baseFicheAssertions(fiche: fiche)
            if let fichePeople: FichePeople = fiche as? FichePeople {
                fichePeopleAssertions(fiche: fichePeople)
            } else if let ficheMedia: FicheMedia = fiche as? FicheMedia {
                ficheMediaAssertions(fiche: ficheMedia)
            } else if let ficheObject: FicheObject = fiche as? FicheObject {
                ficheObjectAssertions(fiche: ficheObject)
            } else if let fichePlace: FichePlace = fiche as? FichePlace {
                fichePlaceAssertions(fiche: fichePlace)
            } else if let ficheEvent: FicheEvent = fiche as? FicheEvent {
                ficheEventAssertions(fiche: ficheEvent)
            } else {
                XCTFail("Not a supported fiche type")
            }
        }
    }

    func testFicheEventRemote() {
        // given
        var fiche: FicheEvent!
        let promise = expectation(description: "Wait for a fiche event from server")
        // when
        FicheAPI.getFiche(with: "595caf51239bd644b0ec30f2").then { (newfiche: Fiche) in
            if let tmpFiche: FicheEvent = newfiche as? FicheEvent {
                fiche = tmpFiche
            }
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(fiche, "Fiche must not be nil")
        baseFicheAssertions(fiche: fiche)
        ficheEventAssertions(fiche: fiche)
    }

    func testFicheMediaRemote() {
        // given
        var fiche: FicheMedia!
        let promise = expectation(description: "Wait for a fiche event from server")
        // when
        FicheAPI.getFiche(with: "5964e242239bd644b0ec3127").then { (newfiche: Fiche) in
            if let tmpFiche: FicheMedia = newfiche as? FicheMedia {
                fiche = tmpFiche
            }
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(fiche, "Fiche must not be nil")
        baseFicheAssertions(fiche: fiche)
        ficheMediaAssertions(fiche: fiche)
    }

    func testFicheObjectRemote() {
        // given
        var fiche: FicheObject!
        let promise = expectation(description: "Wait for a fiche object from server")
        // when
        FicheAPI.getFiche(with: "593e44014afbdc5cfef47d74").then { (newfiche: Fiche) in
            if let tmpFiche: FicheObject = newfiche as? FicheObject {
                fiche = tmpFiche
            }
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(fiche, "Fiche must not be nil")
        baseFicheAssertions(fiche: fiche)
        ficheObjectAssertions(fiche: fiche)
    }

    func testFichePeopleRemote() {
        // given
        var fiche: FichePeople!
        let promise = expectation(description: "Wait for a fiche people from server")
        // when
        FicheAPI.getFiche(with: "591d6f7ffda54c285ef9a0c2").then { (newfiche: Fiche) in
            if let tmpFiche: FichePeople = newfiche as? FichePeople {
                fiche = tmpFiche
            }
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(fiche, "Fiche must not be nil")
        baseFicheAssertions(fiche: fiche)
        fichePeopleAssertions(fiche: fiche)
    }

    func testFichePlaceRemote() {
        // given
        var fiche: FichePlace!
        let promise = expectation(description: "Wait for a fiche place from server")
        // when
        FicheAPI.getFiche(with: "595bc241239bd644b0ec30d9").then { (newfiche: Fiche) in
            if let tmpFiche: FichePlace = newfiche as? FichePlace {
                fiche = tmpFiche
            }
        }.always {
            promise.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        // then
        XCTAssertNotNil(fiche, "Fiche must not be nil")
        baseFicheAssertions(fiche: fiche)
        fichePlaceAssertions(fiche: fiche)
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    private func baseFicheAssertions(fiche: Fiche) {
        // fiche fields
//        XCTAssertNotNil(fiche, "Fiche must not be nil")
        XCTAssertNotNil(fiche.type, "Fiche must have a type")
        XCTAssertNotNil(fiche.id, "Fiche must have an id")
        XCTAssertNotNil(fiche.name, "Fiche must have a name")
        XCTAssertNotNil(fiche.createdAt, "Fiche must have a create at date")
        XCTAssertNotNil(fiche.status, "Fiche must have a status")
        XCTAssertNotNil(fiche.cover, "Fiche must have a cover")
        XCTAssertNotNil(fiche.mainTheme, "Fiche must have a theme")
        XCTAssertNotNil(fiche.description, "Fiche must have a description")
//        XCTAssertNotNil(fiche.getName(), "Fiche must have a name")
        if fiche.isIdsOnly() {
            XCTAssertNotNil(fiche.relatedFichesIds, "Fiche must have related only ids of related Fiches")
            XCTAssertNil(fiche.getRelatedFiches(), "Fiche must not have related Fiches")
        } else {
            XCTAssertNotNil(fiche.getRelatedFiches(), "Fiche must have related Fiches")
        }
//        XCTAssertNotNil(fiche.categories, "Fiche must have categories")
        XCTAssertNotNil(fiche.themes, "Fiche must have themes")
//        XCTAssertNotNil(fiche.subCategories, "Fiche must have sub categories")
//        XCTAssertNotNil(fiche.tags, "Fiche must have tags")
//        XCTAssertNotNil(fiche.references, "Fiche must have references")
//        XCTAssertNotNil(fiche.socialHolder, "Fiche must have a social holder")
//        XCTAssertNotNil(fiche.getCity(), "Fiche must have a city")
//        XCTAssertNotNil(fiche.getCountry(), "Fiche must have a country")
        //        XCTAssertNotNil(fiche.latitude, "Fiche must have a latitude")
        //        XCTAssertNotNil(fiche.longitude, "Fiche must have a longitude")
        //        XCTAssertNotNil(fiche.mongoLocation, "Fiche must have a mongo location")
        //        XCTAssertNotNil(fiche.googleMapLocation, "Fiche must have a Google Map location")
    }

    private func ficheEventAssertions(fiche: FicheEvent) {
        // fiche event fields
        XCTAssertEqual(fiche.type, ItemType.events, "Fiche Event must have a type of events")
        //XCTAssertNotNil(fiche.getStartDate(), "Fiche Event must have a start date")
        //XCTAssertNotNil(fiche.getEndDate(), "Fiche Event must have an end date")
//        XCTAssertNotNil(fiche.getPresentation(), "Fiche Event must have a presentation")
//        XCTAssertNotNil(fiche.getDescription(), "Fiche Event must have a description")
//        XCTAssertNotNil(fiche.getDeltaStart(), "Fiche Event must have a delta start")
//        XCTAssertNotNil(fiche.getDeltaEnd(), "Fiche Event must have a delta end")
    }

    private func ficheMediaAssertions(fiche: FicheMedia) {
        // fiche media fields
        XCTAssertEqual(fiche.type, ItemType.media, "Fiche Media must have a type of media")
//        XCTAssertNotNil(fiche.getPath(), "Fiche Media must have a path")
//        XCTAssertNotNil(fiche.presentation, "Fiche Media must have a presentation")
//        XCTAssertNotNil(fiche.getDescription, "Fiche Media must have a description")
//        XCTAssertNotNil(fiche.attachments, "Fiche Media must have attachements")
//        //        XCTAssertNotNil(fiche.date, "Fiche Media must have a date")
//        XCTAssertNotNil(fiche.getDeltaStart(), "Fiche Media must have a delta start")
    }

    private func ficheObjectAssertions(fiche: FicheObject) {
        // fiche object fields
        XCTAssertEqual(fiche.type, ItemType.objects, "Fiche Object must have a type of objects")
//        XCTAssertNotNil(fiche.getHistory(), "Fiche Object must have a history")
//        XCTAssertNotNil(fiche.getPresentation(), "Fiche Object must have a presentation")
////        XCTAssertNotNil(fiche.shortDescription, "Fiche Object must have a short description")
//        XCTAssertNotNil(fiche.getTechnicalInformation(), "Fiche Object must have technical information")
//        XCTAssertNotNil(fiche.getMoreInformation(), "Fiche Object must have more information")
//        //XCTAssertNotNil(fiche.getDate(), "Fiche Object must have a date")
//        XCTAssertNotNil(fiche.getDeltaStart(), "Fiche Object must have a delta start")
    }

    private func fichePeopleAssertions(fiche: FichePeople) {
        // fiche people fields
        XCTAssertEqual(fiche.type, ItemType.people, "Fiche People must have a type of people")
        //XCTAssertNotNil(fiche.getSurname(), "Fiche People must have a surname")
//        XCTAssertNotNil(fiche.getPresentation(), "Fiche People must have a presentation")
        //        XCTAssertNotNil(fiche.birthday, "Fiche People must have a birthday")
        //        XCTAssertNotNil(fiche.deathday, "Fiche People must have deathday")
        //XCTAssertNotNil(fiche.getShortBio(), "Fiche People must have a short bio")
        //XCTAssertNotNil(fiche.getPlaceOfBirth(), "Fiche People must have a place of birth")
        //XCTAssertNotNil(fiche.getDate(), "Fiche People must have a date")
        //XCTAssertNotNil(fiche.getDeltaStart(), "Fiche People must have a delta start")
        //XCTAssertNotNil(fiche.getDeltaEnd(), "Fiche People must have a delta end")
    }

    private func fichePlaceAssertions(fiche: FichePlace) {
        // these assertions should be common to all fishes but model
        // is inconsistent so they are only on the place one ...
//        XCTAssertNotNil(fiche.getLatitude(), "Fiche Place must have a latitude")
//        XCTAssertNotNil(fiche.getLongitude(), "Fiche Place must have a longitude")
//        XCTAssertNotNil(fiche.getMongoLocation(), "Fiche Place must have a mongo location")
//        XCTAssertNotNil(fiche.getGoogleMapLocation(), "Fiche Place must have a Google Map location")
        // fiche place fields
        XCTAssertEqual(fiche.type, ItemType.places, "Fiche Place must have a type of places")
//        XCTAssertNotNil(fiche.getHistory(), "Fiche Place must have a history")
//        XCTAssertNotNil(fiche.getPresentation(), "Fiche Place must have a presentation")
//        XCTAssertNotNil(fiche.getShortDescription(), "Fiche Place must have a short description")
//        XCTAssertNotNil(fiche.getTechnicalInformation(), "Fiche Place must have technical information")
//        XCTAssertNotNil(fiche.getMoreInformation(), "Fiche Place must have more information")
//        XCTAssertNotNil(fiche.getSchedule(), "Fiche Place must have a schedule")
//        XCTAssertNotNil(fiche.getAccessibility(), "Fiche Place must have an accessibility")
//        XCTAssertNotNil(fiche.getContact(), "Fiche Place must have a contact")
    }
}
